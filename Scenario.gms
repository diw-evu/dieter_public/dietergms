
********************************************************************************
$ontext
The Dispatch and Investment Evaluation Tool with Endogenous Renewables (DIETER).
Version 1.5.0, April 2021.
Written by Alexander Zerrahn, Wolf-Peter Schill, and Fabian St�ckl.
This work is licensed under the MIT License (MIT).
For more information on this license, visit http://opensource.org/licenses/mit-license.php.
Whenever you use this code, please refer to http://www.diw.de/dieter.
We are happy to receive feedback under wschill@diw.de.
$offtext
********************************************************************************




*****************************************
**** Scenario file **********************
*****************************************

*** SOME EXEMPLARY SCENARIO CHOICES

phi_min_res = 0.9 ;

* EV
%EV%$ontext
ev_quant = 1e6 ;
$ontext
$offtext

* Prosumage
%prosumage%$ontext
phi_pro_load(n) = 0.1 ;
phi_pro_self = 0.2 ;
$ontext
$offtext

* Germany only, also adjust Excel, no infeasibility
NTC.fx(l) = 0 ;
F.fx(l,h) = 0 ;

G_INFES.fx(n,h) = 0 ;


%heat%$ontext
* Heating
Parameter
security_margin_n_heat_out /1.0/
;

eta_heat_dyn(n,bu,ch) = eta_heat_dyn('DE',bu,ch) ;

* Parameterization of water-based heat storage
n_heat_p_out(n,bu,ch) = security_margin_n_heat_out * smax( h , dh(n,bu,ch,h) + d_dhw(n,bu,ch,h) ) ;
n_heat_e(n,bu,ch) = 3 * n_heat_p_out(n,bu,ch) ;
n_heat_p_in(n,bu,ch) = n_heat_p_out(n,bu,ch) ;
n_heat_p_in(n,bu,'hp_gs') = n_heat_p_out(n,bu,'hp_gs') / ( eta_heat_dyn(n,bu,'hp_gs') * (temp_sink(n,bu,'hp_gs')+273.15) / (temp_sink(n,bu,'hp_gs') - 10) ) ;
* at least -5�C; applied to 98% of hours; minimum: -13.4
n_heat_p_in(n,bu,'hp_as') = n_heat_p_out(n,bu,'hp_as') / ( eta_heat_dyn(n,bu,'hp_as') * (temp_sink(n,bu,'hp_as')+273.15) / (temp_sink(n,bu,'hp_as') + 5) ) ;

* Parameterization of SETS
n_sets_p_out(n,bu,ch) = security_margin_n_heat_out * smax( h , dh(n,bu,ch,h) ) ;
n_sets_p_in(n,bu,ch) = 2 * n_sets_p_out(n,bu,ch) ;
n_sets_e(n,bu,ch) = 16 * n_sets_p_out(n,bu,ch) ;

* Parameterization of DHW SETS
n_sets_dhw_p_out(n,bu,ch) = security_margin_n_heat_out * smax( h , d_dhw(n,bu,ch,h) ) ;
n_sets_dhw_p_in(n,bu,ch) = n_sets_dhw_p_out(n,bu,ch) ;
n_sets_dhw_e(n,bu,ch) = 2.2 * n_sets_dhw_p_out(n,bu,ch) ;
$ontext
$offtext
