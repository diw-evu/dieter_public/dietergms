# Changelog

## 1.5.0

### Changed

- Major: 
  - Added a P2X sector to produce synthetic gases and liquid fuels. P2X gases/fuels are distributed to ﬁlling stations in the same way as hydrogen, yet each gas/fuel only features one distribution channel and must meet a respective demand. Hydrogen used to produce P2X products must be taken from the centralized production site storage. For production and centralized storage, infrastructure is shared with production & supply capacities used for hydrogen for mobility.
  - The possibility to use hydrogen for reconversion to electricity, i.e., as a means of electricity storage, has now matured.
  - We have added techno-economic parameters for the reconversion module and for the Power-to-Liquid process. Finding the right cost/effciency/... parameters for Power-to-Gas is, however, up to the researcher.
  - Fueling pressure in the hydrogen module has been lowered from 700 bar (passenger cars) to 350 bar (trucks). Accordingly, we now only consider hydrogen to be a plausible fuel for freight traffic, and not for passenger vehicles.

- Minor:
  - Minor error corrections.
  - Updates of input data.

## 1.5.0-pre

### Changed

- Unifies Hydrogen branch (1.4.0) and main branch (1.3.2)

## 1.4.0

### Added

- New Hydrogen module implemented

## 1.3.2

### Added & Changed

- Electricity generation
  - New parameter: availability
    - Time-constant *avail*, *avail_sto*, *avail_rsvr* in [0,1]
  - Load change costs parameterized according to flexmex
    - Substantially lower
    - Reflect wear and tear
- Storage
  - New parameter: availability, for power and energy
  - Investments, investment costs, fixed costs, variable costs, efficiency differentiated for sto in and sto out
  - Self-discharge
    - Not included in minres
- Reservoir (not validated anyway)
  - Investments, investment costs, fixed costs differentiated according to in, out, energy
  - Variable costs introduced
  - New parameter: availability 
- Transmission
  - New: losses (Not included in minres; should cause all kinds of reporting issues anyway (imports, exports))
  - Variable OM costs, set to 0
  - New distances according to flexmex
  - New nomenclature for lines: origin_destination, e.g.., DE_AT
- Data_input and time_series
  - In part, data from flexmex
- *report_to_excel.gms* removed
- EV
  - Separate marginal costs for charging and discharging
- Hours now denoted as t0001, t0002, ...
- Prosumage storage not differentiated according to in and out
- Heat and DHW infeasibility introduced

### Removed

- GUSS-Tool removed
- All loops removed; reporting aligned accordingly (not completely validated, especially concerning all the shares)

### Open issues

- Prosumagers currently have no variable OM (and curtailment cost) for their RES generation
- Reporting
  - Reserve activation shares for EV in case of exogenous reserve requirements missing; note in code
  - Cost breakdown for report_cost does not perfectly add up with EV and reserves, and total costs in report_cost not equal to objective value with EV and reserves
- MinRes shares to be checked
- Scenario file includes some random examples
  - Heat parameterized according to heat paper
  - Maybe better placed somewhere else
