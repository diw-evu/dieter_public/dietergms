
********************************************************************************
$ontext
The Dispatch and Investment Evaluation Tool with Endogenous Renewables (DIETER).
Version 1.5, February 2021.
Written by Alexander Zerrahn, Wolf-Peter Schill, and Fabian St�ckl.
This work is licensed under the MIT License (MIT).
For more information on this license, visit http://opensource.org/licenses/mit-license.php.
Whenever you use this code, please refer to http://www.diw.de/dieter.
We are happy to receive feedback under wschill@diw.de.

DISCLAIMER:
This reporting file has evolved over time. As the model increased in size and
scope, e.g. related to the representation of additional sector coupling
technologies, some parts/parameters of the reporting may now be unsystematic
and/or incomplete. We encourage the user to double-check and potentially
adjust this reporting file, especially for model features that are rarely
used, such as the reserves or prosumage modules. If you find errors or want
to contribute corrections and improvements, we are happy to receive feedback.
$offtext
********************************************************************************


$setenv gdxcompress 1


********************************************************************************
**** Parameters for report file  ***********************************************
********************************************************************************

Parameter
corr_fac_dis             Balancing correction factor - dispatchable technologies
corr_fac_nondis          Balancing correction factor - nondispatchable technologies
corr_fac_sto             Balancing correction factor - storage technologies
corr_fac_dsm_cu          Balancing correction factor - DSM curtailment technologies
corr_fac_dsm_shift       Balancing correction factor - DSM shifting technologies
corr_fac_ev              Balancing correction factor - electric vehicles
corr_fac_rsvr            Balancing correction factor - reservoir
corr_fac_sets            Balancing correction factor - SETS
corr_fac_sets_aux        Balancing correction factor - SETS auxiliary DHW
corr_fac_hp              Balancing correction factor - heat pumps
corr_fac_h_elec          Balancing correction factor - hybrid electric storage heating

corr_fac_ev_sep

gross_energy_demand                      Gross energy demand
gross_energy_demand_market               Gross energy demand - market (non-prosumage segment)
gross_energy_demand_prosumers            Gross energy demand - prosumagers
gross_energy_demand_prosumers_selfgen    Gross energy demand - prosumagers self-generated
gross_energy_demand_prosumers_market     Gross energy demand - prosumagers from market

reserves_activated

calc_maxprice
calc_minprice

report
report_tech
report_tech_hours
report_hours
report_node
report_line
report_cost
report_hours_P2H2

report_reserves
report_reserves_hours
report_reserves_tech
report_reserves_tech_hours

report_prosumage
report_prosumage_tech
report_prosumage_tech_hours
report_market
report_market_tech
report_market_tech_hours

report_heat_tech_hours
report_heat_tech

h2_report_electrolysis_cap
h2_report_recon_cap
h2_report_aux_cap_tech2channel
h2_report_aux_cap_channel2tech
h2_report_infrastructure_cap_channel
h2_report_h2mobiliy_shares
h2_report_electrolysis_flows
h2_report_bypass_1_flow
h2_report_bypass_2_flow
h2_report_prod_sto_level
h2_report_LP_sto_level
h2_report_MP_sto_level
h2_report_HP_sto_level
h2_other_flows
h2_report_filling_flow
h2_report_recon_flows

h2_fixed_costs_electrolysis_aux
h2_fixed_costs_reconversion
h2_fixed_costs
h2_fixed_costs_oa_by_channel

h2_elec_dem_electrolysis_aux_per_h
h2_elec_dem_reconversion_per_h
h2_elec_dem_per_h
h2_elec_dem_oa_by_channel_per_h

h2_elec_dem_electrolysis_aux
h2_elec_dem_reconversion
h2_elec_dem
h2_elec_dem_oa_by_channel

h2_elec_dem_costs_electrolysis_aux_per_h
h2_elec_dem_costs_reconversion_per_h
h2_elec_dem_costs_per_h
h2_elec_dem_costs_oa_by_channel_per_h

h2_elec_dem_costs_oa_by_channel

h2_add_costs_by_channel

h2_trans_on_road


;




********************************************************************************
**** Initialize reporting paremetrs  *******************************************
********************************************************************************

* Set reporting sensitivity. All results below will be reported as zero
Scalar eps_rep_rel Sensitivity for shares defined between 0 and 1        / 1e-4 / ;
Scalar eps_rep_abs Sensitivity for absolute values - e.g. hourly         / 1e-2 / ;
Scalar eps_rep_ins Sensitivity for absolute values - e.g. installed MW   / 1 /    ;
Scalar eps_rep_pos Report zeros in time series                           / 1e-9 / ;


* ----------------------------------------------------------------------------

* Min and max for prices
calc_maxprice = 0 ;
calc_minprice = 1000 ;


* ----------------------------------------------------------------------------

* Default values for correction factors
corr_fac_dis(n,dis,h) = 0 ;
corr_fac_nondis(n,nondis,h) = 0 ;
corr_fac_sto(n,sto,h) = 0 ;
corr_fac_dsm_cu(n,dsm_curt,h) = 0 ;
corr_fac_dsm_shift(n,dsm_shift,h) = 0 ;
corr_fac_ev(n,h) = 0 ;
corr_fac_rsvr(n,rsvr,h) = 0 ;
corr_fac_sets(n,bu,ch,h) = 0 ;
corr_fac_sets_aux(n,bu,ch,h) = 0 ;
corr_fac_hp(n,bu,ch,h) = 0 ;
corr_fac_h_elec(n,bu,ch,h) = 0 ;

corr_fac_ev_sep(n,ev,h) = 0 ;


* ----------------------------------------------------------------------------

* Parameter for hourly nodal reserves activated
reserves_activated(n,h) = 0 ;
%prosumage%N_RES_PRO.l(n,nondis) = 0 ;

%reserves_endogenous%$ontext
reserves_activated(n,h)$(ord(h) > 1) =
feat_node('reserves',n) * (
sum( reserves_nonprim_up , phi_reserves_call(n,reserves_nonprim_up,h) * 1000 * phi_reserves_share(n,reserves_nonprim_up) * ( reserves_intercept(n,reserves_nonprim_up) + sum( nondis , reserves_slope(n,reserves_nonprim_up,nondis) * (N_TECH.l(n,nondis) + N_RES_PRO.l(n,nondis)))/1000))
- sum( reserves_nonprim_do , phi_reserves_call(n,reserves_nonprim_do,h) * 1000 * phi_reserves_share(n,reserves_nonprim_do) * ( reserves_intercept(n,reserves_nonprim_do) + sum( nondis , reserves_slope(n,reserves_nonprim_do,nondis) * (N_TECH.l(n,nondis)+ N_RES_PRO.l(n,nondis)))/1000))
) ;
$ontext
$offtext

%reserves_exogenous%$ontext
reserves_activated(n,h)$(ord(h) > 1) =
feat_node('reserves',n) * (
sum( reserves_nonprim_up , phi_reserves_call(n,reserves_nonprim_up,h) * reserves_exogenous(n,reserves_nonprim_up,h))
- sum( reserves_nonprim_do , phi_reserves_call(n,reserves_nonprim_do,h) * reserves_exogenous(n,reserves_nonprim_do,h))
) ;
$ontext
$offtext


* ----------------------------------------------------------------------------

* Prepare prosumage reporting parameters
%prosumage%$ontext
gross_energy_demand_prosumers(n) = sum( h , phi_pro_load(n) * d(n,h));
gross_energy_demand_prosumers_selfgen(n) = sum( (h,res) , G_RES_PRO.l(n,res,h)) + sum( (sto,h) , STO_OUT_PRO2PRO.l(n,sto,h) ) ;
gross_energy_demand_prosumers_market(n) = sum( h , G_MARKET_M2PRO.l(n,h)) + sum( (sto,h) , STO_OUT_M2PRO.l(n,sto,h) ) ;
$ontext
$offtext


* ----------------------------------------------------------------------------

*Determine balancing correction factors
%reserves%$ontext
        corr_fac_dis(n,tech,h) =
          sum( reserves_do ,  RP_DIS.l(n,reserves_do,tech,h) * phi_reserves_call(n,reserves_do,h))
        - sum( reserves_up ,  RP_DIS.l(n,reserves_up,tech,h) * phi_reserves_call(n,reserves_up,h))
;
        corr_fac_nondis(n,tech,h) =
          sum( reserves_do ,  RP_NONDIS.l(n,reserves_do,tech,h) * phi_reserves_call(n,reserves_do,h))
        - sum( reserves_up ,  RP_NONDIS.l(n,reserves_up,tech,h) * phi_reserves_call(n,reserves_up,h))
;
        corr_fac_sto(n,sto,h) =
           sum( reserves_do , phi_reserves_call(n,reserves_do,h) * ( RP_STO_IN.l(n,reserves_do,sto,h) + RP_STO_OUT.l(n,reserves_do,sto,h)) )
         - sum( reserves_up , phi_reserves_call(n,reserves_up,h) * ( RP_STO_IN.l(n,reserves_up,sto,h) + RP_STO_OUT.l(n,reserves_up,sto,h)) )
;
        corr_fac_rsvr(n,rsvr,h) =
          sum( reserves_do ,  RP_RSVR.l(n,reserves_do,rsvr,h) * phi_reserves_call(n,reserves_do,h))
        - sum( reserves_up ,  RP_RSVR.l(n,reserves_up,rsvr,h) * phi_reserves_call(n,reserves_up,h))
;

%DSM%$ontext
        corr_fac_dsm_cu(n,dsm_curt,h) =
       - sum( reserves_up , RP_DSM_CU.l(n,reserves_up,dsm_curt,h) * phi_reserves_call(n,reserves_up,h) )
;
        corr_fac_dsm_shift(n,dsm_shift,h) =
          sum( reserves_do , RP_DSM_SHIFT.l(n,reserves_do,dsm_shift,h) * phi_reserves_call(n,reserves_do,h))
        - sum( reserves_up , RP_DSM_SHIFT.l(n,reserves_up,dsm_shift,h) * phi_reserves_call(n,reserves_up,h))
;
$ontext
$offtext
%EV%$ontext
%reserves%$ontext
         corr_fac_ev(n,h) = sum( ev ,
         + sum( reserves_do , phi_reserves_call(n,reserves_do,h) * (RP_EV_G2V.l(n,reserves_do,ev,h) + RP_EV_V2G.l(n,reserves_do,ev,h)))
         - sum( reserves_up , phi_reserves_call(n,reserves_up,h) * (RP_EV_G2V.l(n,reserves_up,ev,h) + RP_EV_V2G.l(n,reserves_up,ev,h))) )
;

         corr_fac_ev_sep(n,ev,h) =
         + sum( reserves_do , phi_reserves_call(n,reserves_do,h) * (RP_EV_G2V.l(n,reserves_do,ev,h) + RP_EV_V2G.l(n,reserves_do,ev,h)))
         - sum( reserves_up , phi_reserves_call(n,reserves_up,h) * (RP_EV_G2V.l(n,reserves_up,ev,h) + RP_EV_V2G.l(n,reserves_up,ev,h)))
;



$ontext
$offtext

%heat%$ontext
%reserves%$ontext
         corr_fac_sets(n,bu,ch,h) =
        + sum( reserves_do , phi_reserves_call(n,reserves_do,h) * ( theta_sets(n,bu,ch) * RP_SETS.l(n,reserves_do,bu,ch,h) ))
        - sum( reserves_up , phi_reserves_call(n,reserves_up,h) * ( theta_sets(n,bu,ch) * RP_SETS.l(n,reserves_up,bu,ch,h) ))
;
         corr_fac_sets_aux(n,bu,ch,h) =
        + sum( reserves_do , phi_reserves_call(n,reserves_do,h) * ( theta_sets(n,bu,ch) * RP_SETS_AUX.l(n,reserves_do,bu,ch,h) ))
        - sum( reserves_up , phi_reserves_call(n,reserves_up,h) * ( theta_sets(n,bu,ch) * RP_SETS_AUX.l(n,reserves_up,bu,ch,h) ))
;
         corr_fac_hp(n,bu,ch,h) =
        + sum( reserves_do , phi_reserves_call(n,reserves_do,h) * ( theta_hp(n,bu,ch) * RP_HP.l(n,reserves_do,bu,ch,h) ))
        - sum( reserves_up , phi_reserves_call(n,reserves_up,h) * ( theta_hp(n,bu,ch) * RP_HP.l(n,reserves_up,bu,ch,h) ))
;
         corr_fac_h_elec(n,bu,ch,h) =
        + sum( reserves_do , phi_reserves_call(n,reserves_do,h) * ( theta_storage(n,bu,ch) * RP_H_ELEC.l(n,reserves_do,bu,ch,h) ))
        - sum( reserves_up , phi_reserves_call(n,reserves_up,h) * ( theta_storage(n,bu,ch) * RP_H_ELEC.l(n,reserves_up,bu,ch,h) ))
;
$ontext
$offtext


* ----------------------------------------------------------------------------

* Note: "gross" includes storage losses and sector coupling energy demand

gross_energy_demand(n) = sum( h , d(n,h) + sum( sto , STO_IN.l(n,sto,h) - STO_OUT.l(n,sto,h) )
%prosumage%$ontext
         + sum( sto , sum( res , STO_IN_PRO2PRO.l(n,res,sto,h) + STO_IN_PRO2M.l(n,res,sto,h) ) + STO_IN_M2PRO.l(n,sto,h) + STO_IN_M2M.l(n,sto,h) - STO_OUT_PRO2PRO.l(n,sto,h) - STO_OUT_PRO2M.l(n,sto,h) - STO_OUT_M2PRO.l(n,sto,h) - STO_OUT_M2M.l(n,sto,h) )
$ontext
$offtext
%DSM%$ontext
         - sum( dsm_curt , DSM_CU.l(n,dsm_curt,h) )
         + sum( dsm_shift , DSM_UP.l(n,dsm_shift,h) - sum( hh$( ord(hh) >= ord(h) - t_dur_dsm_shift(n,dsm_shift) AND ord(hh) <= ord(h) + t_dur_dsm_shift(n,dsm_shift) ) , DSM_DO.l(n,dsm_shift,h,hh)) )
$ontext
$offtext
%EV%$ontext
         + sum( ev , EV_CHARGE.l(n,ev,h) - EV_DISCHARGE.l(n,ev,h) )
$ontext
$offtext
%reserves%$ontext
         + reserves_activated(n,h)
         + sum( sto , corr_fac_sto(n,sto,h) )
$ontext
$offtext
%DSM%$ontext
%reserves%$ontext
       - sum( (dsm,reserves_up) , RP_DSM_CU.l(n,reserves_up,dsm,h) * phi_reserves_call(n,reserves_up,h) )
$ontext
$offtext
%reserves%$ontext
%EV%$ontext
       + corr_fac_ev(n,h)
$ontext
$offtext
%heat%$ontext
        + sum( (bu,ch) , theta_dir(n,bu,ch) * H_DIR.l(n,bu,ch,h) + theta_sets(n,bu,ch) * H_SETS_IN.l(n,bu,ch,h) + theta_hp(n,bu,ch) * H_HP_IN.l(n,bu,ch,h) + theta_elec(n,bu,ch) * H_ELECTRIC_IN.l(n,bu,ch,h) )
        + sum( (bu,ch) , theta_dir(n,bu,ch) * H_DHW_DIR.l(n,bu,ch,h) + theta_sets(n,bu,ch) * H_DHW_AUX_ELEC_IN.l(n,bu,ch,h) )
$ontext
$offtext
%heat%$ontext
%reserves%$ontext
        + sum( (bu,ch) , corr_fac_sets(n,bu,ch,h) + corr_fac_sets_aux(n,bu,ch,h) + corr_fac_hp(n,bu,ch,h) + corr_fac_h_elec(n,bu,ch,h) )
$ontext
$offtext
)

%P2H2%$ontext
* 0.001 to convert kWh electricity demand into MWh demand.
      + 0.001 * (
  sum( h,
* prod
                 + sum( ( h2_tech,h2_channel ) , H2_E_H2_IN.l(n,h2_tech,h2_channel,h) )
* aux_prod_site
                 + sum( ( h2_tech,h2_channel ) , h2_prod_aux_sw(n,h2_tech,h2_channel) * h2_prod_aux_ed(n,h2_tech,h2_channel) * H2_PROD_AUX_IN.l(n,h2_tech,h2_channel,h) )
* hydration_lohc
                 + sum ( h2_channel , h2_hyd_liq_sw(n,h2_channel) * h2_hyd_liq_ed(n,h2_channel) * H2_PROD_AUX_OUT.l(n,h2_channel,h) )
* prod_site_storage
                 + sum ( h2_channel , h2_sto_p_sw(n,h2_channel) * H2_STO_P_L.l(n,h2_channel,h) * h2_sto_p_ed(n,h2_channel) )
* aux_bftrans
                 + sum ( h2_channel , h2_aux_pretrans_sw(n,h2_channel) * h2_aux_pretrans_ed(n,h2_channel) * (sum(h2_tech,h2_bypass_1_sw(n,h2_tech,h2_channel) * H2_BYPASS_1.l(n,h2_tech,h2_channel,h)) + H2_STO_P_OUT.l(n,h2_channel,h)) )
* aux_bflohc_filling_storage
                 + sum ( h2_channel , h2_aux_bflp_sto_sw(n,h2_channel) * h2_aux_bflp_sto_ed(n,h2_channel) * H2_AUX_BFLP_STO_IN.l(n,h2_channel,h) )
* LP_storage
                 + sum ( h2_channel , h2_lp_sto_sw(n,h2_channel) * H2_LP_STO_L.l(n,h2_channel,h) * h2_lp_sto_ed(n,h2_channel) )
* dehydration_lohc
                 + sum ( h2_channel , h2_dehyd_evap_sw(n,h2_channel) * ( 1 - h2_dehyd_evap_gas_sw(n,h2_channel) ) * h2_dehyd_evap_ed(n,h2_channel) * H2_LP_STO_OUT.l(n,h2_channel,h) )
* aux_bfMP_storage
                 + sum ( h2_channel , h2_aux_bfMP_sto_sw(n,h2_channel) * h2_aux_bfMP_sto_ed(n,h2_channel) * H2_DEHYD_EVAP_OUT.l(n,h2_channel,h) )
* MP_storage
                 + sum ( h2_channel , h2_MP_sto_sw(n,h2_channel) * H2_MP_STO_L.l(n,h2_channel,h) * h2_MP_sto_ed(n,h2_channel) )
* aux_bffilling_storage
                 + sum ( h2_channel , h2_aux_bfhp_sto_sw(n,h2_channel) * h2_aux_bfhp_sto_ed(n,h2_channel) * H2_AUX_BFHP_STO_IN.l(n,h2_channel,h) )
* HP_storage
                 + sum ( h2_channel , h2_hp_sto_sw(n,h2_channel) * H2_HP_STO_L.l(n,h2_channel,h) * h2_hp_sto_ed(n,h2_channel) )
* aux_bffuel
                 + sum ( h2_channel , h2_aux_bffuel_sw(n,h2_channel) * h2_aux_bffuel_ed(n,h2_channel) * H2_HP_STO_OUT.l(n,h2_channel,h) )
* aux_recon_site
                 + sum ( (h2_tech_recon , h2_channel) , h2_recon_aux_sw(n,h2_channel, h2_tech_recon) * h2_recon_aux_ed(n,h2_channel, h2_tech_recon) * H2_RECON_AUX_OUT.l(n,h2_channel,h2_tech_recon,h)/(1-H2_eta_recon_aux(n,h2_channel,h2_tech_recon)) )


                                 - sum ( (h2_channel,h2_tech_recon) , h2_recon_sw(n,h2_channel) * H2_E_RECON_OUT.l(n,h2_channel,h2_tech_recon,h) )

          )
                )



$ontext
$offtext
;

********************************************************************************
**** Report  *******************************************************************
********************************************************************************

* REPORT model statistics
        report('model status') = DIETER.modelstat ;
        report('solve time') = DIETER.resusd ;
        report('obj value') = Z.l * %sec_hour% ;


* ----------------------------------------------------------------------------

* REPORT HOURS
        report_hours('demand consumers',h,n) = d(n,h) ;
        report_hours('energy generated',h,n) = sum( dis , G_L.l(n,dis,h) ) + sum( nondis , G_RES.l(n,nondis,h) - corr_fac_nondis(n,nondis,h)) + sum( sto , STO_OUT.l(n,sto,h) - corr_fac_sto(n,sto,h)) + sum( rsvr , RSVR_OUT.l(n,rsvr,h) - corr_fac_rsvr(n,rsvr,h))
%DSM%$ontext
         + sum( dsm_shift , DSM_DO_DEMAND.l(n,dsm_shift,h) - corr_fac_dsm_shift(n,dsm_shift,h))
$ontext
$offtext
%EV%$ontext
         + sum( ev , EV_DISCHARGE.l(n,ev,h)) - corr_fac_ev(n,h)
$ontext
$offtext
%prosumage%$ontext


***************
*    /\_/\    *
*   ( o.o )   *    >+++> Wolf: Diesen Term kann ich ehrlich gesagt nicht mehr nachvollziehen
*    > ^ <    *
***************
         + sum( res , G_RES_PRO.l(n,res,h) + G_MARKET_PRO2M.l(n,res,h) + sum( sto , STO_IN_PRO2PRO.l(n,res,sto,h) + STO_IN_PRO2M.l(n,res,sto,h))) + sum( sto , STO_OUT_PRO2PRO.l(n,sto,h) + STO_OUT_PRO2M.l(n,sto,h) + STO_OUT_M2PRO.l(n,sto,h) + STO_OUT_M2M.l(n,sto,h))
$ontext
$offtext
%P2H2%$ontext
                 + 0.001 * sum ( (h2_channel,h2_tech_recon) , h2_recon_sw(n,h2_channel) * H2_E_RECON_OUT.l(n,h2_channel,h2_tech_recon,h) )
$ontext
$offtext

;

        report_hours('infeasibility',h,n) =  G_INFES.l(n,h) ;
        report_hours('gross exports',h,n) = sum( l , max( inc(l,n) * F.l(l,h) , 0 ) ) ;
        report_hours('gross imports',h,n) = sum( l , max( -inc(l,n) * eta_ntc(l) * F.l(l,h) , 0 ) ) ;
        report_hours('net exports',h,n) = report_hours('gross exports',h,n) - report_hours('gross imports',h,n) ;
        report_hours('price',h,n) = - con1a_bal.m(n,h) ;

                 report_hours('energy generated',h,n)$(report_hours('energy generated',h,n) < eps_rep_abs ) = 0 ;
                 report_hours('gross exports',h,n)$(report_hours('gross exports',h,n) < eps_rep_abs ) = 0 ;
                 report_hours('gross imports',h,n)$(report_hours('gross imports',h,n) < eps_rep_abs ) = 0 ;
                 report_hours('net exports',h,n)$(abs(report_hours('net exports',h,n)) < eps_rep_abs ) = 0 ;
                 report_hours('demand consumers',h,n)$(report_hours('demand consumers',h,n) < eps_rep_abs) = 0 ;
                 report_hours('price',h,n)$(report_hours('price',h,n) < eps_rep_abs AND report_hours('price',h,n) > -eps_rep_abs) = eps ;
                 report_hours('infeasibility',h,n)$(report_hours('infeasibility',h,n) < eps_rep_abs) = 0 ;


* ----------------------------------------------------------------------------

* REPORT TECH HOURS

        report_tech_hours('generation conventional',con,h,n) =  G_L.l(n,con,h) + corr_fac_dis(n,con,h) ;
        report_tech_hours('infeasibility','',h,n) = G_INFES.l(n,h) ;
        report_tech_hours('generation renewable',res,h,n) = G_L.l(n,res,h) + corr_fac_dis(n,res,h) + G_RES.l(n,res,h)
%prosumage%$ontext
         + G_RES_PRO.l(n,res,h) + G_MARKET_PRO2M.l(n,res,h) + sum( sto , STO_IN_PRO2PRO.l(n,res,sto,h) + STO_IN_PRO2M.l(n,res,sto,h))
$ontext
$offtext
;

        report_tech_hours('generation renewable',rsvr,h,n) = RSVR_OUT.l(n,rsvr,h)  ;
        report_tech_hours('reservoir inflow',rsvr,h,n) = rsvr_in(n,rsvr,h)/1000 * N_RSVR_E.l(n,rsvr) ;
        report_tech_hours('reservoir level',rsvr,h,n) = RSVR_L.l(n,rsvr,h) ;
        report_tech_hours('curtailment of fluct res',res,h,n) = CU.l(n,res,h)
%prosumage%$ontext
         + CU_PRO.l(n,res,h)
$ontext
$offtext
;
        report_tech_hours('generation storage',sto,h,n) = STO_OUT.l(n,sto,h)
%prosumage%$ontext
         + STO_OUT_PRO2M.l(n,sto,h) + STO_OUT_PRO2PRO.l(n,sto,h) + STO_OUT_M2PRO.l(n,sto,h) + STO_OUT_M2M.l(n,sto,h)
$ontext
$offtext
;

%P2H2%$ontext
                 report_tech_hours('generation storage','H2-recon',h,n) = 0.001 * sum ( (h2_channel,h2_tech_recon) , h2_recon_sw(n,h2_channel) * H2_E_RECON_OUT.l(n,h2_channel,h2_tech_recon,h) )
$ontext
$offtext
;



%EV%$ontext
        report_tech_hours('generation EV','v2g',h,n) = sum( ev , EV_DISCHARGE.l(n,ev,h) )
$ontext
$offtext
        report_tech_hours('storage loading',sto,h,n) = STO_IN.l(n,sto,h)
%prosumage%$ontext
         + sum( res , STO_IN_PRO2PRO.l(n,res,sto,h) + STO_IN_PRO2M.l(n,res,sto,h)) + STO_IN_M2PRO.l(n,sto,h) + STO_IN_M2M.l(n,sto,h)
$ontext
$offtext
;
        report_tech_hours('storage level',sto,h,n) = STO_L.l(n,sto,h)
%prosumage%$ontext
         + STO_L_PRO.l(n,sto,h)
$ontext
$offtext
;


        report_tech_hours('gross exports','',h,n) = sum( l , max( inc(l,n) * F.l(l,h) , 0 ) ) ;
        report_tech_hours('gross imports','',h,n) = sum( l , max( -inc(l,n) * eta_ntc(l) * F.l(l,h) , 0 ) ) ;
        report_tech_hours('net exports','',h,n) = report_tech_hours('gross exports','',h,n) - report_tech_hours('gross imports','',h,n) ;

                 report_tech_hours('generation conventional',con,h,n)$(report_tech_hours('generation conventional',con,h,n) < eps_rep_abs) = 0 ;
                 report_tech_hours('generation renewable',res,h,n)$(report_tech_hours('generation renewable',res,h,n) < eps_rep_abs) = 0 ;
                 report_tech_hours('generation renewable',rsvr,h,n)$(report_tech_hours('generation renewable',rsvr,h,n) < eps_rep_abs) = 0 ;
                 report_tech_hours('curtailment of fluct res',res,h,n)$(report_tech_hours('curtailment of fluct res',res,h,n) < eps_rep_abs) = 0 ;
                 report_tech_hours('generation storage',sto,h,n)$(report_tech_hours('generation storage',sto,h,n) < eps_rep_abs) = 0 ;
                                 report_tech_hours('generation storage','H2-recon',h,n)$(report_tech_hours('generation storage','H2-recon',h,n) < eps_rep_abs) = 0 ;
                 report_tech_hours('generation EV','v2g',h,n)$(report_tech_hours('generation EV','v2g',h,n) < eps_rep_abs ) = 0 ;
                 report_tech_hours('storage loading',sto,h,n)$(report_tech_hours('storage loading',sto,h,n) < eps_rep_abs) = 0 ;
                 report_tech_hours('storage level',sto,h,n)$(report_tech_hours('storage level',sto,h,n) < eps_rep_abs) = 0 ;
                 report_tech_hours('gross exports','',h,n)$(abs(report_tech_hours('gross exports','',h,n)) < eps_rep_abs ) = 0 ;
                 report_tech_hours('gross imports','',h,n)$(abs(report_tech_hours('gross imports','',h,n)) < eps_rep_abs ) = 0 ;
                 report_tech_hours('net exports','',h,n)$(abs(report_tech_hours('net exports','',h,n)) < eps_rep_abs ) = 0 ;
                 report_tech_hours('infeasibility','',h,n)$(report_tech_hours('infeasibility','',h,n) < eps_rep_abs ) = 0 ;
                 report_tech_hours('reservoir inflow',rsvr,h,n)$(report_tech_hours('reservoir inflow',rsvr,h,n) < eps_rep_abs) = 0 ;
                 report_tech_hours('reservoir level',rsvr,h,n)$(report_tech_hours('reservoir level',rsvr,h,n) < eps_rep_abs) = 0 ;


* ----------------------------------------------------------------------------

* RPEORT LINE
         report_line('NTC',l) = NTC.l(l) ;
         report_line('average line use',l)$report_line('NTC',l) = sum( h, abs(F.l(l,h))) / sum( h , report_line('NTC',l)) ;
         report_line('total exports',l)$report_line('NTC',l) = sum( h, F.l(l,h) ) ;
         report_line('total imports',l)$report_line('NTC',l) = sum( h, eta_ntc(l) * F.l(l,h) ) ;
         report_line('total losses',l)$report_line('NTC',l) = sum( h, ( 1-eta_ntc(l) ) * F.l(l,h) ) ;

         report_line('costs: investment',l)$report_line('NTC',l) = c_i_ntc(l)*NTC.l(l) ;
         report_line('costs: fix',l)$report_line('NTC',l) = c_fix_ntc(l)*NTC.l(l) ;
         report_line('costs: variable O&M',l)$report_line('NTC',l) = sum( h , c_m_ntc(l) * F.l(l,h) ) ;

                         report_line('NTC',l)$(report_line('NTC',l) < eps_rep_ins ) = 0 ;
                         report_line('average line use',l)$(report_line('average line use',l) < eps_rep_rel) = 0 ;
                         report_line('total exports',l)$(abs(report_line('total exports',l)) < eps_rep_abs) = 0 ;
                         report_line('total imports',l)$(abs(report_line('total imports',l)) < eps_rep_abs) = 0 ;
                         report_line('total losses',l)$(abs(report_line('total losses',l)) < eps_rep_abs) = 0 ;

                         report_line('costs: investment',l)$(report_line('costs: investment',l) < eps_rep_abs) = 0 ;
                         report_line('costs: fix',l)$(report_line('costs: fix',l) < eps_rep_abs) = 0 ;
                         report_line('costs: variable O&M',l)$(report_line('costs: variable O&M',l) < eps_rep_abs) = 0 ;


* ----------------------------------------------------------------------------

* REPORT NODE
        report_node('energy demand total',n) = sum( h , d(n,h) ) + sum( (sto,h) , STO_IN.l(n,sto,h)) *%sec_hour%
%prosumage%$ontext
         + sum( (h,sto,res) , STO_IN_PRO2PRO.l(n,res,sto,h) + STO_IN_PRO2M.l(n,res,sto,h)) + sum( (h,sto) , STO_IN_M2PRO.l(n,sto,h) + STO_IN_M2M.l(n,sto,h)) *%sec_hour%
$ontext
$offtext
%DSM%$ontext
        + sum( (dsm_shift,h) , DSM_UP_DEMAND.l(n,dsm_shift,h)) * %sec_hour%
$ontext
$offtext
%EV%$ontext
         + sum( (ev,h) , EV_CHARGE.l(n,ev,h)) * %sec_hour%
$ontext
$offtext
%reserves%$ontext
         + sum( h , reserves_activated(n,h))
$ontext
$offtext
%heat%$ontext
        + sum( (bu,ch,h) , theta_dir(n,bu,ch)*( H_DIR.l(n,bu,ch,h) + H_DHW_DIR.l(n,bu,ch,h) ) + theta_sets(n,bu,ch)*H_SETS_IN.l(n,bu,ch,h) + theta_hp(n,bu,ch)*H_HP_IN.l(n,bu,ch,h) + theta_elec(n,bu,ch)*H_STO_IN_ELECTRIC.l(n,bu,ch,h) )

$ontext
$offtext

%P2H2%$ontext
* 0.001 to convert kWh electricity demand into MWh demand.
      + 0.001 * (
  sum( h,

* prod
                 + sum( (h2_tech,h2_channel) , H2_E_H2_IN.l(n,h2_tech,h2_channel,h) )
* aux_prod_site
                 + sum( (h2_tech,h2_channel) , h2_prod_aux_sw(n,h2_tech,h2_channel) * h2_prod_aux_ed(n,h2_tech,h2_channel) * H2_PROD_AUX_IN.l(n,h2_tech,h2_channel,h) )
* hydration_lohc
                 + sum ( h2_channel , h2_hyd_liq_sw(n,h2_channel) * h2_hyd_liq_ed(n,h2_channel) * H2_PROD_AUX_OUT.l(n,h2_channel,h) )
* prod_site_storage
                 + sum ( h2_channel , h2_sto_p_sw(n,h2_channel) * H2_STO_P_L.l(n,h2_channel,h) * h2_sto_p_ed(n,h2_channel) )
* aux_bftrans
                 + sum ( h2_channel, h2_aux_pretrans_sw(n,h2_channel) * h2_aux_pretrans_ed(n,h2_channel) * (sum(h2_tech,h2_bypass_1_sw(n,h2_tech,h2_channel) * H2_BYPASS_1.l(n,h2_tech,h2_channel,h)) + H2_STO_P_OUT.l(n,h2_channel,h)) )
* aux_bflohc_filling_storage
                 + sum ( h2_channel, h2_aux_bflp_sto_sw(n,h2_channel) * h2_aux_bflp_sto_ed(n,h2_channel) * H2_AUX_BFLP_STO_IN.l(n,h2_channel,h) )
* LP_storage
                 + sum ( h2_channel , h2_lp_sto_sw(n,h2_channel) * H2_LP_STO_L.l(n,h2_channel,h) * h2_lp_sto_ed(n,h2_channel) )
* dehydration_lohc
                 + sum ( h2_channel , h2_dehyd_evap_sw(n,h2_channel) * ( 1 - h2_dehyd_evap_gas_sw(n,h2_channel) ) * h2_dehyd_evap_ed(n,h2_channel) * H2_LP_STO_OUT.l(n,h2_channel,h) )
* aux_bfMP_storage
                 + sum ( h2_channel, h2_aux_bfMP_sto_sw(n,h2_channel) * h2_aux_bfMP_sto_ed(n,h2_channel) * H2_DEHYD_EVAP_OUT.l(n,h2_channel,h) )
* MP_storage
                 + sum ( h2_channel , h2_MP_sto_sw(n,h2_channel) * H2_MP_STO_L.l(n,h2_channel,h) * h2_Mp_sto_ed(n,h2_channel) )
* aux_bffilling_storage
                 + sum ( h2_channel, h2_aux_bfhp_sto_sw(n,h2_channel) * h2_aux_bfhp_sto_ed(n,h2_channel) * H2_AUX_BFHP_STO_IN.l(n,h2_channel,h) )
* HP_storage
                 + sum ( h2_channel, h2_hp_sto_sw(n,h2_channel) * H2_HP_STO_L.l(n,h2_channel,h) * h2_hp_sto_ed(n,h2_channel) )
* aux_bffuel
                 + sum ( h2_channel , h2_aux_bffuel_sw(n,h2_channel) * h2_aux_bffuel_ed(n,h2_channel) * H2_HP_STO_OUT.l(n,h2_channel,h) )
* aux_recon_site
                 + sum ( (h2_tech_recon, h2_channel) , h2_recon_aux_sw(n,h2_channel, h2_tech_recon) * h2_recon_aux_ed(n,h2_channel, h2_tech_recon) * H2_RECON_AUX_OUT.l(n,h2_channel,h2_tech_recon,h)/(1-H2_eta_recon_aux(n,h2_channel,h2_tech_recon)) )

             )
     )

$ontext
$offtext
;

        report_node('energy demand gross',n) = gross_energy_demand(n) ;
* Note: "net" in the sense of primary electricity generation
        report_node('energy generated net',n) = sum( h , sum( dis , G_L.l(n,dis,h)) + sum( nondis , G_RES.l(n,nondis,h) - corr_fac_nondis(n,nondis,h)) + sum( rsvr , RSVR_OUT.l(n,rsvr,h) - corr_fac_rsvr(n,rsvr,h))
%prosumage%$ontext
         + sum( res , phi_res(n,res,h) * N_RES_PRO.l(n,res) - CU_PRO.l(n,res,h))
$ontext
$offtext
         )
;
        report_node('energy generated gross',n) = sum( h , sum( dis , G_L.l(n,dis,h)) + sum( nondis , G_RES.l(n,nondis,h) - corr_fac_nondis(n,nondis,h)) + sum( rsvr , RSVR_OUT.l(n,rsvr,h) - corr_fac_rsvr(n,rsvr,h)) + sum( sto , STO_OUT.l(n,sto,h) - corr_fac_sto(n,sto,h))
%DSM%$ontext
         + sum( dsm_shift , DSM_DO_DEMAND.l(n,dsm_shift,h) - corr_fac_dsm_shift(n,dsm_shift,h)) + sum( dsm_curt , DSM_CU.l(n,dsm_curt,h))
$ontext
$offtext
%P2H2%$ontext
                 + 0.001 * sum ( (h2_channel,h2_tech_recon) , h2_recon_sw(n,h2_channel) * H2_E_RECON_OUT.l(n,h2_channel,h2_tech_recon,h) )
$ontext
$offtext
%EV%$ontext
         + sum( ev , EV_DISCHARGE.l(n,ev,h)) - corr_fac_ev(n,h)
$ontext
$offtext
%prosumage%$ontext
         +  sum( res , phi_res(n,res,h) * N_RES_PRO.l(n,res) - CU_PRO.l(n,res,h)) + sum( sto , STO_OUT_PRO2PRO.l(n,sto,h) + STO_OUT_PRO2M.l(n,sto,h) + STO_OUT_M2PRO.l(n,sto,h) + STO_OUT_M2M.l(n,sto,h))
$ontext
$offtext
         )
;
        report_node('gross exports',n) = sum( (l,h) , max( inc(l,n) * F.l(l,h) , 0 ) ) ;
        report_node('gross imports',n) = sum( (l,h) , max( -inc(l,n) * eta_ntc(l) * F.l(l,h) , 0 ) ) ;
        report_node('net exports',n) = report_node('gross exports',n) - report_node('gross imports',n) ;

        report_node('net import share in gross demand',n) = -min(report_node('net exports',n),0) / report_node('energy demand gross',n) ;
        report_node('net export share in net generation',n) = max(report_node('net exports',n),0) / report_node('energy generated net',n) ;
        report_node('trade capacity',n) = sum( l , abs(inc(l,n) * NTC.l(l)) ) ;
        report_node('gross export share in net generation',n) = sum( h , report_hours('gross exports',h,n)) / sum( h , report_hours('energy generated',h,n) ) ;
        report_node('generation capacity total',n) = sum( tech , N_TECH.l(n,tech)) + sum( sto , N_STO_P_OUT.l(n,sto)) + sum( rsvr , N_RSVR_P.l(n,rsvr))

%P2H2%$ontext
                 + 0.001 * sum ( h2_tech_recon , H2_N_RECON.l(n,h2_tech_recon) )
$ontext
$offtext
%prosumage%$ontext
         + sum( res , N_RES_PRO.l(n,res)) + sum( sto , N_STO_P_PRO.l(n,sto) )
$ontext
$offtext
%DSM%$ontext
        + sum( dsm_curt , N_DSM_CU.l(n,dsm_curt) ) + sum( dsm_shift , N_DSM_SHIFT.l(n,dsm_shift) )
$ontext
$offtext
;
        report_node('curtailment of fluct res absolute',n) = sum( (nondis,h) , CU.l(n,nondis,h) ) * %sec_hour%
%prosumage%$ontext
         + sum( (nondis,h) , CU_PRO.l(n,nondis,h) ) * %sec_hour%
$ontext
$offtext
;
        report_node('curtailment of fluct res relative',n)$(sum((nondis,h), phi_res(n,nondis,h) * (N_TECH.l(n,nondis)))
%prosumage%$ontext
         + sum((h,res),phi_res(n,res,h) * N_RES_PRO.l(n,res))
$ontext
$offtext
         > eps_rep_abs*card(res)*card(h)) = ( sum( (nondis,h), CU.l(n,nondis,h) )
%prosumage%$ontext
          + sum( (res,h), CU_PRO.l(n,res,h) )
$ontext
$offtext
         ) * %sec_hour% / ( sum( (nondis,h), phi_res(n,nondis,h) * N_TECH.l(n,nondis) )
%prosumage%$ontext
         + sum( (res,h) , phi_res(n,res,h) * N_RES_PRO.l(n,res) )
$ontext
$offtext
) ;
        report_node('bio not utilized absolute',n)$(m_e(n,'bio')) = (m_e(n,'bio') - sum(h, G_L.l(n,'bio',h))) * %sec_hour% ;
        report_node('bio not utilized relative',n)$(m_e(n,'bio')) = (m_e(n,'bio') - sum(h, G_L.l(n,'bio',h))) / m_e(n,'bio') ;
        report_node('max price',n)$sum(h,d(n,h)) = max( calc_maxprice , smax( h, -con1a_bal.m(n,h)) ) ;
        report_node('min price',n)$sum(h,d(n,h)) = min( calc_minprice , smin( h, -con1a_bal.m(n,h)) ) ;
        report_node('mean price',n)$sum(h,d(n,h)) = -sum(h, con1a_bal.m(n,h))/card(h) ;
        report_node('CO2 emissions',n) = sum( (h,dis)$eta(n,dis) , carbon_content(n,dis)/eta(n,dis) * G_L.l(n,dis,h) ) ;


                 report_node('energy demand total',n)$(report_node('energy demand total',n) < eps_rep_abs) = 0 ;
                 report_node('curtailment of fluct res absolute',n)$(report_node('curtailment of fluct res absolute',n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_node('curtailment of fluct res relative',n)$(report_node('curtailment of fluct res relative',n) < eps_rep_rel) = 0 ;
                 report_node('bio not utilized absolute',n)$(report_node('bio not utilized absolute',n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_node('bio not utilized relative',n)$(report_node('bio not utilized relative',n) < eps_rep_rel) = 0 ;
                 report_node('min price',n)$(report_node('min price',n) < eps_rep_abs AND report_node('min price',n) > -eps_rep_abs) = eps ;
                 report_node('gross exports',n)$(report_node('gross exports',n) < eps_rep_abs*card(h)) = 0 ;
                 report_node('gross imports',n)$(report_node('gross imports',n) < eps_rep_abs*card(h)) = 0 ;
                 report_node('net exports',n)$(abs(report_node('net exports',n)) < eps_rep_abs*card(h)) = 0 ;
                 report_node('energy generated net',n)$(report_node('energy generated net',n) < eps_rep_abs*card(h)) = 0 ;
                 report_node('energy generated gross',n)$(report_node('energy generated gross',n) < eps_rep_abs) = 0 ;
                 report_node('energy demand gross',n)$(report_node('energy demand gross',n) < eps_rep_abs*card(h)) = 0 ;
                 report_node('net import share in gross demand',n)$(report_node('net import share in gross demand',n) < eps_rep_rel) = 0 ;
                 report_node('net export share in net generation',n)$(report_node('net export share in net generation',n) < eps_rep_rel) = 0 ;
                 report_node('generation capacity total',n)$(report_node('generation capacity total',n) < eps_rep_ins) = 0 ;
                 report_node('gross import share',n)$(report_node('gross import share',n) < eps_rep_rel ) = 0 ;
                 report_node('gross export share in net generation',n)$(report_node('gross export share in net generation',n) < eps_rep_rel ) = 0 ;
                 report_node('CO2 emissions',n)$(report_node('CO2 emissions',n) < eps_rep_abs ) = 0 ;



* ----------------------------------------------------------------------------

* REPORT COST

        report_cost('Nodal cost: dispatch',n) = sum( h , sum( dis , c_m(n,dis)*G_L.l(n,dis,h) + c_up(n,dis)*G_UP.l(n,dis,h)$(ord(h)>1) + c_do(n,dis)*G_DO.l(n,dis,h)$(ord(h)>1)) + sum( nondis , c_cu(n,nondis)*CU.l(n,nondis,h) + c_vom(n,nondis)*G_RES.l(n,nondis,h) ) + sum( sto , c_m_sto_in(n,sto)*STO_IN.l(n,sto,h) + c_m_sto_out(n,sto)*STO_OUT.l(n,sto,h) ) + sum( rsvr , c_m_rsvr(n,rsvr) * RSVR_OUT.l(n,rsvr,h) )
* Note: in these dispatch costs, variable V2G costs are not included
%DSM%$ontext
                 + sum( dsm_curt , c_m_dsm_cu(n,dsm_curt)*DSM_CU.l(n,dsm_curt,h) ) + sum( dsm_shift , c_m_dsm_shift(n,dsm_shift)*(DSM_UP_DEMAND.l(n,dsm_shift,h) + DSM_DO_DEMAND.l(n,dsm_shift,h)))
%reserves%$ontext
                 + sum( (dsm_curt,reserves_up) , RP_DSM_CU.l(n,reserves_up,dsm_curt,h) * phi_reserves_call(n,reserves_up,h) * c_m_dsm_cu(n,dsm_curt) )
                 + sum( (dsm_shift,reserves) , RP_DSM_SHIFT.l(n,reserves,dsm_shift,h) * phi_reserves_call(n,reserves,h) * c_m_dsm_shift(n,dsm_shift) )
$ontext
$offtext
%prosumage%$ontext
                 + sum( map_n_sto_pro(n,sto) , c_m_sto_out(n,sto) * ( STO_OUT_PRO2PRO.l(n,sto,h) + STO_OUT_M2PRO.l(n,sto,h) + STO_OUT_PRO2M.l(n,sto,h) + STO_OUT_M2M.l(n,sto,h)) + c_m_sto_in(n,sto) * ( sum( res , STO_IN_PRO2PRO.l(n,res,sto,h) + STO_IN_PRO2M.l(n,res,sto,h)) + STO_IN_M2PRO.l(n,sto,h) + STO_IN_M2M.l(n,sto,h) ) )
$ontext
$offtext
%reserves%$ontext
                 + sum( (map_n_sto(n,sto),reserves_up) , phi_reserves_call(n,reserves_up,h) * c_m_sto_out(n,sto) * RP_STO_OUT.l(n,reserves_up,sto,h) )
                 - sum( (map_n_sto(n,sto),reserves_up) , phi_reserves_call(n,reserves_up,h) * c_m_sto_in(n,sto) * RP_STO_IN.l(n,reserves_up,sto,h) )
                 - sum( (map_n_sto(n,sto),reserves_do) , phi_reserves_call(n,reserves_do,h) * c_m_sto_out(n,sto) * RP_STO_OUT.l(n,reserves_do,sto,h) )
                 + sum( (map_n_sto(n,sto),reserves_do) , phi_reserves_call(n,reserves_do,h) * c_m_sto_in(n,sto) * RP_STO_IN.l(n,reserves_do,sto,h) )
                 + sum( (map_n_rsvr(n,rsvr),reserves_up) , RP_RSVR.l(n,reserves_up,rsvr,h) * phi_reserves_call(n,reserves_up,h) * c_m_rsvr(n,rsvr) )
                 - sum( (map_n_rsvr(n,rsvr),reserves_do) , RP_RSVR.l(n,reserves_do,rsvr,h) * phi_reserves_call(n,reserves_do,h) * c_m_rsvr(n,rsvr) )
$ontext
$offtext
%reserves%$ontext
%EV%$ontext
%EV_EXOG%        + sum( (reserves_up,ev) , RP_EV_V2G.l(n,reserves_up,ev,h) * phi_reserves_call(n,reserves_up,h) * c_m_ev_dis(n,ev) ) - sum( (reserves_do,ev) , RP_EV_V2G.l(n,reserves_do,ev,h) * phi_reserves_call(n,reserves_do,h) * c_m_ev_dis(n,ev) )
$ontext
$offtext
);

***************
*    /\_/\    *
*   ( o.o )   *    >+++> S�MTLICHE KOSTEN ERG�NZEN UM PROSUMAGE, EV, H2 ...
*    > ^ <    *
***************

         report_cost('Nodal cost: fuel',n) = sum( h , sum( dis , c_fuel(n,dis)*G_L.l(n,dis,h)) ) ;
         report_cost('Nodal cost: CO2',n) = sum( h , sum( dis , c_co2(n,dis)*G_L.l(n,dis,h)) ) ;
         report_cost('Nodal cost: variable O&M and other variable',n) = sum( h , sum( dis , c_vom(n,dis)*G_L.l(n,dis,h)) + sum( nondis , c_vom(n,nondis)*G_RES.l(n,nondis,h)) + sum( sto , c_m_sto_in(n,sto)*STO_IN.l(n,sto,h) + c_m_sto_out(n,sto)*STO_OUT.l(n,sto,h) ) + sum( rsvr , c_m_rsvr(n,rsvr) * RSVR_OUT.l(n,rsvr,h) )
%prosumage%$ontext
         + sum( sto , c_m_sto_out(n,sto) * ( STO_OUT_PRO2PRO.l(n,sto,h) + STO_OUT_M2PRO.l(n,sto,h) + STO_OUT_PRO2M.l(n,sto,h) + STO_OUT_M2M.l(n,sto,h)) + c_m_sto_in(n,sto) * ( sum( res , STO_IN_PRO2PRO.l(n,res,sto,h) + STO_IN_PRO2M.l(n,res,sto,h)) + STO_IN_M2PRO.l(n,sto,h) + STO_IN_M2M.l(n,sto,h) ) )
$ontext
$offtext
%reserves%$ontext
         + sum( (rsvr,reserves_up) , RP_RSVR.l(n,reserves_up,rsvr,h) * phi_reserves_call(n,reserves_up,h) * c_m_rsvr(n,rsvr) )
         - sum( (rsvr,reserves_do) , RP_RSVR.l(n,reserves_do,rsvr,h) * phi_reserves_call(n,reserves_do,h) * c_m_rsvr(n,rsvr) )
         + sum( (sto,reserves_up) , phi_reserves_call(n,reserves_up,h) * c_m_sto_out(n,sto) * RP_STO_OUT.l(n,reserves_up,sto,h) )
         - sum( (sto,reserves_up) , phi_reserves_call(n,reserves_up,h) * c_m_sto_in(n,sto) * RP_STO_IN.l(n,reserves_up,sto,h) )
         - sum( (sto,reserves_do) , phi_reserves_call(n,reserves_do,h) * c_m_sto_out(n,sto) * RP_STO_OUT.l(n,reserves_do,sto,h) )
         + sum( (sto,reserves_do) , phi_reserves_call(n,reserves_do,h) * c_m_sto_in(n,sto) * RP_STO_IN.l(n,reserves_do,sto,h) )
$ontext
$offtext
) ;
         report_cost('Nodal cost: renewable curtailment',n) =  sum( (h,nondis) , c_cu(n,nondis)*CU.l(n,nondis,h) ) ;
         report_cost('Nodal cost: load change',n) = sum( h$(ord(h)>1) , sum( dis , c_up(n,dis)*G_UP.l(n,dis,h) + c_do(n,dis)*G_DO.l(n,dis,h)) ) ;

%DSM%$ontext
         report_cost('Nodal cost: variable DSM',n) = sum( (h,dsm_curt) , c_m_dsm_cu(n,dsm_curt)*DSM_CU.l(n,dsm_curt,h) ) + sum( (h,dsm_shift) , c_m_dsm_shift(n,dsm_shift) * DSM_UP_DEMAND.l(n,dsm_shift,h) ) + sum( (h,dsm_shift) , c_m_dsm_shift(n,dsm_shift) * DSM_DO_DEMAND.l(n,dsm_shift,h) )
%reserves%$ontext
         + sum( (h,dsm_curt,reserves_up) , RP_DSM_CU.l(n,reserves_up,dsm_curt,h) * phi_reserves_call(n,reserves_up,h) * c_m_dsm_cu(n,dsm_curt) )
         + sum( (h,dsm_shift,reserves) , RP_DSM_SHIFT.l(n,reserves,dsm_shift,h) * phi_reserves_call(n,reserves,h) * c_m_dsm_shift(n,dsm_shift) )
$ontext
$offtext
;

         report_cost('Nodal cost: investment',n) = sum( tech , c_i(n,tech) *N_TECH.l(n,tech) ) + sum( sto , c_i_sto_e(n,sto)*N_STO_E.l(n,sto) + c_i_sto_p_in(n,sto)*N_STO_P_IN.l(n,sto)+ c_i_sto_p_out(n,sto)*N_STO_P_OUT.l(n,sto) ) + sum( rsvr , c_i_rsvr_e(n,rsvr)*N_RSVR_E.l(n,rsvr) + c_i_rsvr_p_out(n,rsvr)*N_RSVR_P.l(n,rsvr) )
%DSM%$ontext
                 + sum( dsm_curt , c_i_dsm_cu(n,dsm_curt)*N_DSM_CU.l(n,dsm_curt) ) + sum( dsm_shift , c_i_dsm_shift(n,dsm_shift)*N_DSM_SHIFT.l(n,dsm_shift))
$ontext
$offtext
%prosumage%$ontext
                 + sum( res , c_i(n,res)*N_RES_PRO.l(n,res) ) + sum( sto , c_i_sto_e(n,sto)*N_STO_E_PRO.l(n,sto) + c_i_sto_p_in(n,sto)*N_STO_P_PRO.l(n,sto) )
$ontext
$offtext
;

         report_cost('Nodal cost: fix',n) = sum( tech , c_fix(n,tech)*N_TECH.l(n,tech) ) + sum( sto , c_fix_sto_e(n,sto)*N_STO_E.l(n,sto) + c_fix_sto_p_in(n,sto)*N_STO_P_IN.l(n,sto)+ c_fix_sto_p_out(n,sto)*N_STO_P_OUT.l(n,sto) ) + sum( rsvr , c_fix_rsvr_p_out(n,rsvr)*N_RSVR_P.l(n,rsvr) +  c_fix_rsvr_e(n,rsvr)*N_RSVR_E.l(n,rsvr) )
%DSM%$ontext
                 + sum( dsm_curt , c_fix_dsm_cu(n,dsm_curt)*N_DSM_CU.l(n,dsm_curt) ) + sum( dsm_shift , c_fix_dsm_shift(n,dsm_shift)*N_DSM_SHIFT.l(n,dsm_shift) )
$ontext
$offtext
%prosumage%$ontext
                 + sum( res , c_fix(n,res)*N_RES_PRO.l(n,res) ) + sum( sto , c_fix_sto_e(n,sto)*N_STO_E_PRO.l(n,sto) + c_fix_sto_p_in(n,sto)*N_STO_P_PRO.l(n,sto) )
$ontext
$offtext
;
         report_cost('Nodal cost: investment and fix',n) = report_cost('Nodal cost: investment',n) + report_cost('Nodal cost: fix',n) ;
         report_cost('Nodal cost: infeasibility',n) = sum( h , c_infes * G_INFES.l(n,h) ) ;
%heat%$ontext
         report_cost('Nodal cost: fossil heating',n) = sum( (bu,hst,h) , pen_heat_fuel(n,bu,hst) * H_STO_IN_FOSSIL.l(n,bu,hst,h) ) ;
         report_cost('Nodal cost: infeasibility heating',n) = sum( (bu,ch,h) , c_h_infes * H_INFES.l(n,bu,ch,h) + c_h_dhw_infes * H_DHW_INFES.l(n,bu,ch,h) ) ;
$ontext
$offtext
;
         report_cost('Costs lines',l) = ( c_i_ntc(l) + c_fix_ntc(l) ) * NTC.l(l) ;
%EV%$ontext
         report_cost('Nodal cost: PHEV fuel',n) = sum( (h,ev) , pen_phevfuel(n,ev) * EV_PHEVFUEL.l(n,ev,h) ) ;
         report_cost('Nodal cost: EV charging and discharging',n) = sum( (h,ev) , c_m_ev_cha(n,ev) * EV_CHARGE.l(n,ev,h) + c_m_ev_dis(n,ev) * EV_DISCHARGE.l(n,ev,h) )
%reserves%$ontext
%EV_EXOG% + sum( (h,reserves_up,ev) , RP_EV_V2G.l(n,reserves_up,ev,h) * phi_reserves_call(n,reserves_up,h) * c_m_ev_dis(n,ev) ) - sum( (h,reserves_do,ev) , RP_EV_V2G.l(n,reserves_do,ev,h) * phi_reserves_call(n,reserves_do,h) * c_m_ev_dis(n,ev) )
$ontext
$offtext
;
$ontext
$offtext

***************
*    /\_/\    *
*   ( o.o )   *    >+++> Total costs are now calculated at the end of the document (after the H2 module).
*    > ^ <    *
***************

                 report_cost('Nodal cost: dispatch',n)$(report_cost('Nodal cost: dispatch',n) < card(h) * eps_rep_abs) = 0 ;
                 report_cost('Nodal cost: fuel',n)$(report_cost('Nodal cost: fuel',n) < eps_rep_abs) = 0 ;
                 report_cost('Nodal cost: CO2',n)$(report_cost('Nodal cost: CO2',n) < eps_rep_abs) = 0 ;
                 report_cost('Nodal cost: variable O&M and other variable',n)$(report_cost('Nodal cost: variable O&M and other variable',n) < eps_rep_abs) = 0 ;
                 report_cost('Nodal cost: renewable curtailment',n)$(report_cost('Nodal cost: renewable curtailment',n) < eps_rep_abs) = 0 ;
                 report_cost('Nodal cost: load change',n)$(report_cost('Nodal cost: load change',n) < eps_rep_abs) = 0 ;
                 report_cost('Nodal cost: investment',n)$(report_cost('Nodal cost: investment',n) < eps_rep_ins) = 0 ;
                 report_cost('Nodal cost: fix',n)$(report_cost('Nodal cost: fix',n) < eps_rep_ins) = 0 ;
                 report_cost('Nodal cost: investment and fix',n)$(report_cost('Nodal cost: investment and fix',n) < eps_rep_ins) = 0 ;
                 report_cost('Nodal cost: infeasibility',n)$(report_cost('Nodal cost: infeasibility',n) < card(h) * eps_rep_abs) = 0 ;
                 report_cost('Costs lines',l)$(report_cost('Costs lines',l) < eps_rep_ins) = 0 ;
%EV%$ontext
                 report_cost('Nodal cost: PHEV fuel',n)$(report_cost('Nodal cost: PHEV fuel',n) < eps_rep_abs) = 0 ;
$ontext
$offtext
%heat%$ontext
                 report_cost('Nodal cost: fossil heating',n)$(report_cost('Nodal cost: fossil heating',n) < card(h) * eps_rep_abs) = 0 ;
                 report_cost('Nodal cost: infeasibility heating',n)$(report_cost('Nodal cost: infeasibility heating',n) < card(h) * eps_rep_abs) = 0 ;
$ontext
$offtext
%DSM%$ontext
                 report_cost('Nodal cost: variable DSM',n)$(report_cost('Nodal cost: variable DSM',n) < eps_rep_abs ) = 0 ;
$ontext
$offtext

* ----------------------------------------------------------------------------

* REPORT TECH
        report_tech('capacities conventional',con,n) =  N_TECH.l(n,con) ;
        report_tech('capacities renewable',res,n) = 0 + N_TECH.l(n,res)
%prosumage%$ontext
         + N_RES_PRO.l(n,res)
$ontext
$offtext
;
        report_tech('capacities reservoir MW',rsvr,n) = N_RSVR_P.l(n,rsvr) ;
        report_tech('capacities reservoir MWh',rsvr,n) = N_RSVR_E.l(n,rsvr);
        report_tech('capacities storage MW in',sto,n) =  N_STO_P_IN.l(n,sto)
%prosumage%$ontext
         + N_STO_P_PRO.l(n,sto)
$ontext
$offtext
;
        report_tech('capacities storage MW out',sto,n) =  N_STO_P_OUT.l(n,sto)
%prosumage%$ontext
         + N_STO_P_PRO.l(n,sto)
$ontext
$offtext
;
        report_tech('capacities storage MWh',sto,n) =  N_STO_E.l(n,sto)
%prosumage%$ontext
         + N_STO_E_PRO.l(n,sto)
$ontext
$offtext
;

%P2H2%$ontext
                 report_tech('capacities H2 recon MW out',h2_tech_recon,n) = 0.001 * H2_N_RECON.l(n,h2_tech_recon)
$ontext
$offtext
;


        report_tech('Costs: investment MW',con,n) =  c_i(n,con)*N_TECH.l(n,con) ;
        report_tech('Costs: investment MW',res,n) = 0 + c_i(n,res)*N_TECH.l(n,res)
%prosumage%$ontext
         + c_i(n,res)*N_RES_PRO.l(n,res)
$ontext
$offtext
;
        report_tech('Costs: investment MW',rsvr,n) = c_i_rsvr_p_out(n,rsvr)*N_RSVR_P.l(n,rsvr) + c_i_rsvr_e(n,rsvr)*N_RSVR_E.l(n,rsvr) ;
        report_tech('Costs: investment MWh',rsvr,n) = c_i_rsvr_e(n,rsvr)*N_RSVR_E.l(n,rsvr);
        report_tech('Costs: investment MW',sto,n) =  c_i_sto_p_in(n,sto)*N_STO_P_IN.l(n,sto) + c_i_sto_p_out(n,sto)*N_STO_P_OUT.l(n,sto)
%prosumage%$ontext
         + c_i_sto_p_in(n,sto)*N_STO_P_PRO.l(n,sto)
$ontext
$offtext
;
        report_tech('Costs: investment MWh',sto,n) =  c_i_sto_e(n,sto)*(N_STO_E.l(n,sto)
* Note: H2 reconversion costs not included here
%prosumage%$ontext
         + N_STO_E_PRO.l(n,sto)
$ontext
$offtext
)* %sec_hour%
;

%DSM%$ontext
        report_tech('Costs: investment MW',dsm_curt,n) = c_i_dsm_cu(n,dsm_curt)*N_DSM_CU.l(n,dsm_curt) ;
        report_tech('Costs: investment MW',dsm_shift,n) = c_i_dsm_shift(n,dsm_shift)*N_DSM_SHIFT.l(n,dsm_shift) ;
$ontext
$offtext
        report_tech('Costs: fix MW',con,n) = c_fix(n,con)*N_TECH.l(n,con) ;
        report_tech('Costs: fix MW',res,n) = c_fix(n,res)*N_TECH.l(n,res)
%prosumage%$ontext
        + c_fix(n,res)*N_RES_PRO.l(n,res)
$ontext
$offtext
;
        report_tech('Costs: fix MW',sto,n) = c_fix_sto_p_in(n,sto)*N_STO_P_IN.l(n,sto) + c_fix_sto_p_out(n,sto)*N_STO_P_OUT.l(n,sto)
%prosumage%$ontext
        + c_fix_sto_p_in(n,sto)*N_STO_P_PRO.l(n,sto)
$ontext
$offtext
;
        report_tech('Costs: fix MWh',sto,n) =c_fix_sto_e(n,sto)*N_STO_E.l(n,sto)
%prosumage%$ontext
        + c_fix_sto_e(n,sto)*N_STO_E_PRO.l(n,sto)
$ontext
$offtext
;
        report_tech('Costs: fix MW',rsvr,n) = c_fix_rsvr_p_out(n,rsvr) * N_RSVR_P.l(n,rsvr) ;
        report_tech('Costs: fix MWh',rsvr,n) = c_fix_rsvr_e(n,rsvr)*N_RSVR_E.l(n,rsvr) ;
%DSM%$ontext
        report_tech('Costs: fix MW',dsm_curt,n) = c_fix_dsm_cu(n,dsm_curt)*N_DSM_CU.l(n,dsm_curt) ;
        report_tech('Costs: fix MW',dsm_shift,n) = c_fix_dsm_shift(n,dsm_shift)*N_DSM_SHIFT.l(n,dsm_shift) ;
$ontext
$offtext
        report_tech('Costs: variable curtailment',nondis,n) = c_cu(n,nondis) * sum( h , CU.l(n,nondis,h) ) ;
        report_tech('Costs: variable fuel',dis,n) = c_fuel(n,dis) * sum( h , G_L.l(n,dis,h) ) ;
        report_tech('Costs: variable CO2',con,n) = c_co2(n,con) * sum( h , G_L.l(n,con,h) ) ;
        report_tech('Costs: variable WAT aka load change',dis,n) = c_up(n,dis) * sum( h$(ord(h)>1) , G_UP.l(n,dis,h) ) + c_do(n,dis) * sum( h$(ord(h)>1) , G_DO.l(n,dis,h) ) ;
        report_tech('Costs: variable O&M',dis,n) = c_vom(n,dis) * sum( h , G_L.l(n,dis,h) ) ;
        report_tech('Costs: variable O&M',nondis,n) = c_vom(n,nondis) * sum( h , G_RES.l(n,nondis,h) ) ;
        report_tech('Costs: variable O&M',rsvr,n) = c_m_rsvr(n,rsvr) * sum( h , RSVR_OUT.l(n,rsvr,h) )
%reserves%$ontext
        + sum( (h,reserves_up) , RP_RSVR.l(n,reserves_up,rsvr,h) * phi_reserves_call(n,reserves_up,h) * c_m_rsvr(n,rsvr) )
        - sum( (h,reserves_do) , RP_RSVR.l(n,reserves_do,rsvr,h) * phi_reserves_call(n,reserves_do,h) * c_m_rsvr(n,rsvr) )
$ontext
$offtext
;
        report_tech('Costs: variable O&M',sto,n) = c_m_sto_in(n,sto) * sum( h , STO_IN.l(n,sto,h) ) + c_m_sto_out(n,sto) * sum( h , STO_OUT.l(n,sto,h) )
%prosumage%$ontext
         + sum( h, c_m_sto_out(n,sto) * ( STO_OUT_PRO2PRO.l(n,sto,h) + STO_OUT_M2PRO.l(n,sto,h) + STO_OUT_PRO2M.l(n,sto,h) + STO_OUT_M2M.l(n,sto,h)) + c_m_sto_in(n,sto) * ( sum( res , STO_IN_PRO2PRO.l(n,res,sto,h) + STO_IN_PRO2M.l(n,res,sto,h)) + STO_IN_M2PRO.l(n,sto,h) + STO_IN_M2M.l(n,sto,h) ) )
$ontext
$offtext
%reserves%$ontext
         + sum( (h,reserves_up) , phi_reserves_call(n,reserves_up,h) * c_m_sto_out(n,sto) * RP_STO_OUT.l(n,reserves_up,sto,h) )
         - sum( (h,reserves_up) , phi_reserves_call(n,reserves_up,h) * c_m_sto_in(n,sto) * RP_STO_IN.l(n,reserves_up,sto,h) )
         - sum( (h,reserves_do) , phi_reserves_call(n,reserves_do,h) * c_m_sto_out(n,sto) * RP_STO_OUT.l(n,reserves_do,sto,h) )
         + sum( (h,reserves_do) , phi_reserves_call(n,reserves_do,h) * c_m_sto_in(n,sto) * RP_STO_IN.l(n,reserves_do,sto,h) )
$ontext
$offtext
;
        report_tech('Costs: variable',dis,n) = report_tech('Costs: variable O&M',dis,n) + report_tech('Costs: variable fuel',dis,n) +  report_tech('Costs: variable WAT aka load change',dis,n) + report_tech('Costs: variable CO2',dis,n) ;
        report_tech('Costs: variable',nondis,n) = report_tech('Costs: variable O&M',nondis,n) + report_tech('Costs: variable curtailment',nondis,n) ;
        report_tech('Costs: variable',sto,n) = report_tech('Costs: variable O&M',sto,n) ;
        report_tech('Costs: variable',rsvr,n) = report_tech('Costs: variable O&M',rsvr,n) ;
%DSM%$ontext
        report_tech('Costs: variable',dsm_curt,n) = sum( h , c_m_dsm_cu(n,dsm_curt)*DSM_CU.l(n,dsm_curt,h) )
%reserves%$ontext
         + sum( (h,reserves_up) , RP_DSM_CU.l(n,reserves_up,dsm_curt,h) * phi_reserves_call(n,reserves_up,h) * c_m_dsm_cu(n,dsm_curt) )
$ontext
$offtext
;
%DSM%$ontext
        report_tech('Costs: variable',dsm_shift,n) = sum( h , c_m_dsm_shift(n,dsm_shift) * DSM_UP_DEMAND.l(n,dsm_shift,h) ) + sum( h , c_m_dsm_shift(n,dsm_shift) * DSM_DO_DEMAND.l(n,dsm_shift,h) )
%reserves%$ontext
        + sum( (h,reserves) , RP_DSM_SHIFT.l(n,reserves,dsm_shift,h) * phi_reserves_call(n,reserves,h) * c_m_dsm_shift(n,dsm_shift) )
$ontext
$offtext
;
        report_tech('Costs: infes','',n) = sum( h , c_infes * G_INFES.l(n,h) ) ;
%EV%$ontext
        report_tech('Costs: PHEV fuel','ev_cumul',n) = sum( (h,ev) , pen_phevfuel(n,ev) * EV_PHEVFUEL.l(n,ev,h) ) ;
        report_tech('Costs: variable','ev_cumul',n) = sum( (h,ev) , c_m_ev_cha(n,ev) * EV_CHARGE.l(n,ev,h) + c_m_ev_dis(n,ev) * EV_DISCHARGE.l(n,ev,h) )
%reserves%$ontext
%EV_EXOG% + sum( (h,reserves_up,ev) , RP_EV_V2G.l(n,reserves_up,ev,h) * phi_reserves_call(n,reserves_up,h) * c_m_ev_dis(n,ev) ) - sum( (h,reserves_do,ev) , RP_EV_V2G.l(n,reserves_do,ev,h) * phi_reserves_call(n,reserves_do,h) * c_m_ev_dis(n,ev) )
$ontext
$offtext
;
%heat%$ontext
        report_tech('Costs: fossil heating','',n) = sum( (bu,hst,h) , pen_heat_fuel(n,bu,hst) * H_STO_IN_FOSSIL.l(n,bu,hst,h) ) ;
        report_tech('Costs: infes','',n) = sum( (bu,ch,h) , c_h_infes * H_INFES.l(n,bu,ch,h) + c_h_dhw_infes * H_DHW_INFES.l(n,bu,ch,h) ) ;
$ontext
$offtext

        report_tech('Capacity share',con,n) = N_TECH.l(n,con) / report_node('generation capacity total',n) + 1e-9 ;
        report_tech('Capacity share',res,n) = N_TECH.l(n,res) / report_node('generation capacity total',n) + 1e-9 ;
        report_tech('Capacity share',rsvr,n) = N_RSVR_P.l(n,rsvr) / report_node('generation capacity total',n) + 1e-9 ;
        report_tech('Capacity share',res,n) = (N_TECH.l(n,res)
%prosumage%$ontext
         + N_RES_PRO.l(n,res)
$ontext
$offtext
         ) / report_node('generation capacity total',n) + 1e-9 ;
        report_tech('Capacity share',sto,n) = (N_STO_P_OUT.l(n,sto)
%prosumage%$ontext
         + N_STO_P_PRO.l(n,sto)
$ontext
$offtext
         ) / report_node('generation capacity total',n) + 1e-9 ;

%P2H2%$ontext
                  report_tech('Capacity share',h2_tech_recon,n) = 0.001 * H2_N_RECON.l(n,h2_tech_recon) / report_node('generation capacity total',n) + 1e-9 ;
$ontext
$offtext


        report_tech('renshares in nodal gross demand',res,n) = sum( h, G_L.l(n,res,h) + G_RES.l(n,res,h) - corr_fac_nondis(n,res,h)
%prosumage%$ontext
         + G_MARKET_PRO2M.l(n,res,h) + G_RES_PRO.l(n,res,h) + sum( sto , STO_IN_PRO2PRO.l(n,res,sto,h) + STO_IN_PRO2M.l(n,res,sto,h) )
$ontext
$offtext
         ) / gross_energy_demand(n) ;
        report_tech('renshares in nodal gross demand',rsvr,n) = sum( h , RSVR_OUT.l(n,rsvr,h) - corr_fac_rsvr(n,rsvr,h) ) / gross_energy_demand(n) ;
        report_tech('conshares in nodal gross demand',con,n) = sum( h, G_L.l(n,con,h) ) / gross_energy_demand(n) ;
        report_tech('renshares in nodal net generation',res,n)$report_node('energy generated net',n) = sum( h, G_L.l(n,res,h) + G_RES.l(n,res,h) - corr_fac_nondis(n,res,h)
%prosumage%$ontext
         + G_MARKET_PRO2M.l(n,res,h) + G_RES_PRO.l(n,res,h) + sum( sto , STO_IN_PRO2PRO.l(n,res,sto,h) )
$ontext
$offtext
) / report_node('energy generated net',n)  ;
        report_tech('renshares in nodal net generation',rsvr,n)$report_node('energy generated net',n) = sum( h, RSVR_OUT.l(n,rsvr,h) - corr_fac_rsvr(n,rsvr,h)) / report_node('energy generated net',n)  ;
        report_tech('conshares in nodal net generation',con,n)$report_node('energy generated net',n) = sum( h, G_L.l(n,con,h)) / report_node('energy generated net',n) ;

        report_tech('curtailment of fluct res absolute',res,n) =  sum(h, CU.l(n,res,h)
%prosumage%$ontext
         + CU_PRO.l(n,res,h)
$ontext
$offtext
         ) * %sec_hour% ;
        report_tech('curtailment of fluct res relative',res,n)$( report_tech('curtailment of fluct res absolute',res,n) AND ( sum(h, G_RES.l(n,res,h) - corr_fac_nondis(n,res,h)) + sum(h, CU.l(n,res,h))
%prosumage%$ontext
         + sum( h, G_RES_PRO.l(n,res,h) ) + sum( h , CU.l(n,res,h) )
$ontext
$offtext
         ) > card(h)*eps_rep_abs )
         =  sum( h , CU.l(n,res,h)
%prosumage%$ontext
         + CU_PRO.l(n,res,h)
$ontext
$offtext
         ) / ( sum( h , phi_res(n,res,h) * (N_TECH.l(n,res)
%prosumage%$ontext
         + N_RES_PRO.l(n,res)
$ontext
$offtext
         )))
;
        report_tech('Storage out total non-reserves',sto,n) = sum(h, report_tech_hours('generation storage',sto,h,n) ) * %sec_hour% ;
        report_tech('Storage in total non-reserves',sto,n) = sum(h, report_tech_hours('storage loading',sto,h,n) ) * %sec_hour% ;
        report_tech('Storage losses',sto,n) = sum(h, (1-eta_sto_in(n,sto))*report_tech_hours('storage loading',sto,h,n) ) + sum(h, (1/eta_sto_out(n,sto)-1)*report_tech_hours('generation storage',sto,h,n) ) + sum( h$(ord(h)<card(h)) , (1-eta_sto_self(n,sto)) * STO_L.l(n,sto,h) )

 ;

        report_tech('FLH',con,n)$( N_TECH.l(n,con) > eps_rep_ins) = sum( h , G_L.l(n,con,h) ) / N_TECH.l(n,con) ;
        report_tech('FLH',res,n)$( N_TECH.l(n,res) > eps_rep_ins) = sum( h , G_L.l(n,res,h) + G_RES.l(n,res,h) - corr_fac_nondis(n,res,h)
%prosumage%$ontext
         + G_MARKET_PRO2M.l(n,res,h) + sum( sto , STO_IN_PRO2PRO.l(n,res,sto,h)) + G_RES_PRO.l(n,res,h)
$ontext
$offtext
         ) / (N_TECH.l(n,res)
%prosumage%$ontext
         + N_RES_PRO.l(n,res)
$ontext
$offtext
         ) ;
        report_tech('FLH',rsvr,n)$( N_RSVR_P.l(n,rsvr) > eps_rep_ins) = sum( h , RSVR_OUT.l(n,rsvr,h) - corr_fac_rsvr(n,rsvr,h)) / N_RSVR_P.l(n,rsvr) ;
        report_tech('FLH',sto,n)$( N_STO_P_OUT.l(n,sto) > eps_rep_ins) = ( report_tech('Storage out total non-reserves',sto,n)) / N_STO_P_OUT.l(n,sto) ;

* Note: trucks that transport H2 or synfuels also have CO2 emissions, which are not considered here
        report_tech('CO2 emissions',dis,n)$eta(n,dis) = sum( h , carbon_content(n,dis)/eta(n,dis) * G_L.l(n,dis,h) ) ;


                 report_tech('capacities conventional',con,n)$(report_tech('capacities conventional',con,n) < eps_rep_ins) = 0 ;
                 report_tech('capacities renewable',res,n)$(report_tech('capacities renewable',res,n) < eps_rep_ins) = 0 ;
                 report_tech('capacities storage MW',sto,n)$(report_tech('capacities storage MW',sto,n) < eps_rep_ins) = 0 ;
                 report_tech('capacities storage MWh',sto,n)$(report_tech('capacities storage MWh',sto,n) < eps_rep_ins) = 0 ;
                 report_tech('capacities reservoir MW',rsvr,n)$(report_tech('capacities reservoir MW',rsvr,n) < eps_rep_ins) = 0 ;
                 report_tech('capacities reservoir MWh',rsvr,n)$(report_tech('capacities reservoir MWh',rsvr,n) < eps_rep_ins) = 0 ;
                 report_tech('renshares in nodal gross demand',res,n)$(report_tech('renshares in nodal gross demand',res,n) < eps_rep_rel) = 0 ;
                 report_tech('renshares in nodal gross demand',rsvr,n)$(report_tech('renshares in nodal gross demand',rsvr,n) < eps_rep_rel) = 0 ;
                 report_tech('conshares in nodal gross demand',con,n)$(report_tech('conshares in nodal gross demand',con,n) < eps_rep_rel) = 0 ;
                 report_tech('renshares in nodal net generation',res,n)$(report_tech('renshares in nodal net generation',res,n) < eps_rep_rel) = 0 ;
                 report_tech('renshares in nodal net generation',rsvr,n)$(report_tech('renshares in nodal net generation',rsvr,n) < eps_rep_rel) = 0 ;
                 report_tech('conshares in nodal net generation',con,n)$(report_tech('conshares in nodal net generation',con,n) < eps_rep_rel) = 0 ;
                 report_tech('curtailment of fluct res absolute',res,n)$(report_tech('curtailment of fluct res absolute',res,n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_tech('curtailment of fluct res relative',res,n)$(report_tech('curtailment of fluct res relative',res,n) < eps_rep_rel) = 0 ;
                 report_tech('Capacity share',con,n)$(report_tech('Capacity share',con,n) < eps_rep_rel) = 0 ;
                 report_tech('Capacity share',res,n)$(report_tech('Capacity share',res,n) < eps_rep_rel) = 0 ;
                 report_tech('Capacity share',rsvr,n)$(report_tech('Capacity share',rsvr,n) < eps_rep_rel) = 0 ;
                 report_tech('Capacity share',sto,n)$(report_tech('Capacity share',sto,n) < eps_rep_rel) = 0 ;
                 report_tech('FLH',con,n)$(report_tech('FLH',con,n) < eps_rep_abs) = 0 ;
                 report_tech('FLH',res,n)$(report_tech('FLH',res,n) < eps_rep_abs) = 0 ;
                 report_tech('FLH',rsvr,n)$(report_tech('FLH',rsvr,n) < eps_rep_abs) = 0 ;
                 report_tech('FLH',sto,n)$(report_tech('FLH',sto,n) < eps_rep_abs) = 0 ;
                 report_tech('Storage out total non-reserves',sto,n)$(report_tech('Storage out total non-reserves',sto,n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_tech('Storage in total non-reserves',sto,n)$(report_tech('Storage in total non-reserves',sto,n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_tech('Costs: investment MW',con,n)$(report_tech('Costs: investment MW',con,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: investment MW',res,n)$(report_tech('Costs: investment MW',res,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: investment MW',rsvr,n)$(report_tech('Costs: investment MW',rsvr,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: investment MWh',rsvr,n)$(report_tech('Costs: investment MWh',rsvr,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: investment MW',sto,n)$(report_tech('Costs: investment MW',sto,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: investment MWh',sto,n)$(report_tech('Costs: investment MWh',sto,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: fix MW',tech,n)$(report_tech('Costs: fix MW',tech,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: fix MW',sto,n)$(report_tech('Costs: fix MW',sto,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: fix MWh',sto,n)$(report_tech('Costs: fix MWh',sto,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: fix MW',rsvr,n)$(report_tech('Costs: fix MW',rsvr,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: fix MWh',rsvr,n)$(report_tech('Costs: fix MWh',rsvr,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: variable O&M',dis,n)$(report_tech('Costs: variable O&M',dis,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: variable O&M',nondis,n)$(report_tech('Costs: variable O&M',nondis,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: variable O&M',rsvr,n)$(report_tech('Costs: variable O&M',rsvr,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: variable O&M',sto,n)$(report_tech('Costs: variable O&M',sto,n) < eps_rep_abs) = 0 ;

                 report_tech('Costs: variable curtailment',nondis,n)$(report_tech('Costs: variable curtailment',nondis,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: variable fuel',dis,n)$(report_tech('Costs: variable fuel',dis,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: variable WAT aka load change',dis,n)$(report_tech('Costs: variable WAT aka load change',dis,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: variable CO2',con,n)$(report_tech('Costs: variable CO2',con,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: variable',dis,n)$(report_tech('Costs: variable',dis,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: variable',nondis,n)$(report_tech('Costs: variable',nondis,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: variable',sto,n)$(report_tech('Costs: variable',sto,n) < eps_rep_abs) = 0 ;
                 report_tech('Costs: variable',rsvr,n)$(report_tech('Costs: variable',rsvr,n) < eps_rep_abs) = 0 ;
                 report_tech('CO2 emissions',dis,n)$(report_tech('CO2 emissions',dis,n) < eps_rep_abs) = 0 ;


* ----------------------------------------------------------------------------

* REPORT NODE
        report_node('renshare in nodal gross demand',n) = sum(res, report_tech('renshares in nodal gross demand',res,n)) + sum( rsvr , report_tech('renshares in nodal gross demand',rsvr,n)) ;
        report_node('conshare in nodal gross demand',n) = sum(con, report_tech('conshares in nodal gross demand',con,n) ) ;
        report_node('net import share in nodal gross demand',n) = - report_node('net exports',n) / gross_energy_demand(n) ;
        report_node('renshare in nodal net generation',n) = sum(res, report_tech('renshares in nodal net generation',res,n)) + sum( rsvr , report_tech('renshares in nodal net generation',rsvr,n)) ;
        report_node('conshare in nodal net generation',n) = sum(con, report_tech('conshares in nodal net generation',con,n) ) ;

                 report_node('renshare in nodal gross demand',n) $(report_node('renshare in nodal gross demand',n) < eps_rep_rel ) = 0 ;
                 report_node('conshare in nodal gross demand',n)$(report_node('conshare in nodal gross demand',n) < eps_rep_rel ) = 0 ;
                 report_node('renshare in nodal net generation',n)$(report_node('renshare in nodal net generation',n) < eps_rep_rel ) = 0 ;
                 report_node('conshare in nodal net generation',n)$(report_node('conshare in nodal net generation',n) < eps_rep_rel ) = 0 ;


* ----------------------------------------------------------------------------

* REPORT TECH
        report_tech('Yearly energy',con,n) = sum( h , G_L.l(n,con,h) )  ;
        report_tech('Yearly energy',res,n) = sum( h , G_L.l(n,res,h) + G_RES.l(n,res,h) - corr_fac_nondis(n,res,h)
%prosumage%$ontext
         + G_MARKET_PRO2M.l(n,res,h) + sum(sto , STO_IN_PRO2PRO.l(n,res,sto,h) + STO_IN_PRO2M.l(n,res,sto,h)) + G_RES_PRO.l(n,res,h)
$ontext
$offtext
         ) ;
        report_tech('Yearly energy',rsvr,n) = sum( h , RSVR_OUT.l(n,rsvr,h) - corr_fac_rsvr(n,rsvr,h) ) ;
        report_tech('Yearly energy',sto,n) = sum( h , STO_OUT.l(n,sto,h) - corr_fac_sto(n,sto,h)
%prosumage%$ontext
         + STO_OUT_PRO2PRO.l(n,sto,h) + STO_OUT_PRO2M.l(n,sto,h) + STO_OUT_M2PRO.l(n,sto,h) + STO_OUT_M2M.l(n,sto,h)
$ontext
$offtext
         ) ;

%P2H2%$ontext
                report_tech('Yearly energy',h2_tech_recon,n) = 0.001 * sum ( (h2_channel,h) , H2_E_RECON_OUT.l(n,h2_channel,h2_tech_recon,h) ) ;
$ontext
$offtext

        report_tech('Yearly energy','infes',n) = sum( h , G_INFES.l(n,h) )  ;

        report_tech('Energy share in nodal gross generation',con,n) = sum( h , G_L.l(n,con,h) ) / report_node('energy generated gross',n) * %sec_hour% + 1e-9 ;
        report_tech('Energy share in nodal gross generation',res,n) = sum( h , G_L.l(n,res,h) + G_RES.l(n,res,h) - corr_fac_nondis(n,res,h)
%prosumage%$ontext
         + G_MARKET_PRO2M.l(n,res,h) + sum(sto , STO_IN_PRO2PRO.l(n,res,sto,h) + STO_IN_PRO2M.l(n,res,sto,h)) + G_RES_PRO.l(n,res,h)
$ontext
$offtext
         )/ report_node('energy generated gross',n) * %sec_hour% + 1e-9 ;
        report_tech('Energy share in nodal gross generation',rsvr,n) = sum( h , RSVR_OUT.l(n,rsvr,h) - corr_fac_rsvr(n,rsvr,h) ) / report_node('energy generated gross',n) * %sec_hour% + 1e-9 ;


        report_tech('Energy share in nodal gross generation',sto,n) = sum( h , STO_OUT.l(n,sto,h) - corr_fac_sto(n,sto,h)
%prosumage%$ontext
         + STO_OUT_PRO2PRO.l(n,sto,h) + STO_OUT_PRO2M.l(n,sto,h) + STO_OUT_M2PRO.l(n,sto,h) + STO_OUT_M2M.l(n,sto,h)
$ontext
$offtext
         ) / report_node('energy generated gross',n) * %sec_hour% + 1e-9 ;

***************
*    /\_/\    *
*   ( o.o )   *    >+++> prosumage missing
*    > ^ <    *
***************

%reserves% report_tech('FLH',sto,n)$( N_STO_P_OUT.l(n,sto) > eps_rep_ins) = report_tech('Storage out total non-reserves',sto,n) / (N_STO_P_OUT.l(n,sto) ) ;
%reserves% report_tech('Storage cycles',sto,n)$( N_STO_E.l(n,sto) > eps_rep_ins) = report_tech('Storage out total non-reserves',sto,n) / (N_STO_E.l(n,sto)) * %sec_hour% ;

                 report_tech('Yearly energy',con,n)$(report_tech('Yearly energy',con,n) < eps_rep_ins ) = 0 ;
                 report_tech('Yearly energy',res,n)$(report_tech('Yearly energy',res,n) < eps_rep_ins ) = 0 ;
                 report_tech('Yearly energy',rsvr,n)$(report_tech('Yearly energy',rsvr,n) < eps_rep_ins ) = 0 ;
                 report_tech('Yearly energy',sto,n)$(report_tech('Yearly energy',sto,n) < eps_rep_ins ) = 0 ;
                                 report_tech('Yearly energy',h2_tech_recon,n)$(report_tech('Yearly energy',h2_tech_recon,n) < eps_rep_ins ) = 0 ;
                 report_tech('Yearly energy','infes',n)$(report_tech('Yearly energy','infes',n) < eps_rep_abs) = 0 ;

                 report_tech('Energy share in nodal gross generation',con,n)$(report_tech('Energy share in nodal gross generation',con,n) < eps_rep_rel) = 0 ;
                 report_tech('Energy share in nodal gross generation',res,n)$(report_tech('Energy share in nodal gross generation',res,n) < eps_rep_rel) = 0 ;
                 report_tech('Energy share in nodal gross generation',rsvr,n)$(report_tech('Energy share in nodal gross generation',rsvr,n) < eps_rep_rel) = 0 ;
                 report_tech('Energy share in nodal gross generation',sto,n)$(report_tech('Energy share in nodal gross generation',sto,n) < eps_rep_rel) = 0 ;
%reserves%       report_tech('FLH',sto,n)$(report_tech('FLH',sto,n) < eps_rep_abs) = 0 ;
%reserves%       report_tech('Storage cycles',sto,n)$(report_tech('Storage cycles',sto,n) < eps_rep_abs) = 0 ;


* ----------------------------------------------------------------------------

* RPEORT
        report('curtailment of fluct res absolute') = sum( n , report_node('curtailment of fluct res absolute',n) ) ;
        report('curtailment of fluct res relative') = sum( n , report_node('curtailment of fluct res absolute',n) ) / sum((res,h,n), phi_res(n,res,h) * (N_TECH.l(n,res)
%prosumage%$ontext
         + N_RES_PRO.l(n,res)
$ontext
$offtext
         )) ;
        report('bio not utilized absolute')$(sum( n , m_e(n,'bio'))) = sum( n , report_node('bio not utilized absolute',n) ) ;
        report('bio not utilized relative')$(sum( n , m_e(n,'bio'))) = report('bio not utilized absolute') / sum( n , m_e(n,'bio')) ;
        report('Capacity total') = sum( n , report_node('Capacity total',n)) ;
        report('energy demand gross') = sum( n , report_node('energy demand gross',n) ) ;
        report('energy demand total') = sum ( n , report_node('energy demand total',n) ) ;
        report('energy generated net') =  sum( n , report_node('energy generated net',n)) ;
        report('energy generated gross') =  sum( n , report_node('energy generated gross',n)) ;
*        report('gross trade share') = sum( (h,n) , report_hours('gross imports',h,n))/ sum( (h,n) , report_hours('energy demanded',h,n) ) ;
        report('renshare total') = sum( (h,n) , sum( res , G_L.l(n,res,h) + G_RES.l(n,res,h) - corr_fac_nondis(n,res,h)
%prosumage%$ontext
         + phi_res(n,res,h) * N_RES_PRO.l(n,res) - CU_PRO.l(n,res,h)
$ontext
$offtext
         ) + sum( rsvr , RSVR_OUT.l(n,rsvr,h) - corr_fac_rsvr(n,rsvr,h))) / sum( n , gross_energy_demand(n) ) ;
        report('conshare total') = sum( (h,n) , sum( con , G_L.l(n,con,h)) ) / sum( n, gross_energy_demand(n) ) ;
        report('Energy total') = sum( n , report_node('Energy total',n) ) ;

                 report('curtailment of fluct res absolute')$(report('curtailment of fluct res absolute') < eps_rep_abs*card(h)) = 0 ;
                 report('curtailment of fluct res relative')$(report('curtailment of fluct res relative') < eps_rep_rel) = 0 ;
                 report('bio not utilized absolute')$(report('bio not utilized absolute') < eps_rep_abs*card(h)) = 0 ;
                 report('bio not utilized relative')$(report('bio not utilized relative') < eps_rep_rel) = 0 ;
                 report('Capacity total')$(report('Capacity total') < eps_rep_ins) = 0 ;
                 report('gross trade share')$(report('gross trade share') < eps_rep_rel ) = 0 ;
                 report('renshare total')$(report('renshare total') < eps_rep_rel ) = 0 ;
                 report('conshare total')$(report('conshare total') < eps_rep_rel ) = 0 ;
                 report('Energy total')$(report('Energy total') < eps_rep_abs) = 0 ;


* ----------------------------------------------------------------------------

* DSM
%DSM%$ontext
        report_tech_hours('load curtailment (non-reserves)',dsm_curt,h,n)$(feat_node('dsm',n)) = DSM_CU.l(n,dsm_curt,h) ;
        report_tech_hours('load shift pos (non-reserves)',dsm_shift,h,n)$(feat_node('dsm',n)) = DSM_UP_DEMAND.l(n,dsm_shift,h) ;
        report_tech_hours('load shift neg (non-reserves)',dsm_shift,h,n)$(feat_node('dsm',n)) = DSM_DO_DEMAND.l(n,dsm_shift,h) ;
        report_tech('FLH',dsm_shift,n)$(feat_node('dsm',n) AND N_DSM_SHIFT.l(n,dsm_shift) > eps_rep_ins) = sum( (h,hh) , DSM_DO.l(n,dsm_shift,h,hh) ) / N_DSM_SHIFT.l(n,dsm_shift) ;
        report_tech('capacities load curtailment',dsm_curt,n)$(feat_node('dsm',n)) =  N_DSM_CU.l(n,dsm_curt) ;
        report_tech('capacities load shift',dsm_shift,n)$(feat_node('dsm',n)) =  N_DSM_SHIFT.l(n,dsm_shift) ;
        report_tech('Capacity share',dsm_curt,n)$(feat_node('dsm',n)) = N_DSM_CU.l(n,dsm_curt) / report_node('Generation capacity total',n) + 1e-9 ;
        report_tech('Capacity share',dsm_shift,n)$(feat_node('dsm',n)) = N_DSM_SHIFT.l(n,dsm_shift) / report_node('Generation capacity total',n) + 1e-9 ;
        report_tech('Energy share in nodal gross generation',dsm_curt,n)$(feat_node('dsm',n)) = sum( h , DSM_CU.l(n,dsm_curt,h) - corr_fac_dsm_cu(n,dsm_curt,h))   / report_node('energy generated gross',n) * %sec_hour% + 1e-9 ;
        report_tech('Energy share in nodal gross generation',dsm_shift,n)$(feat_node('dsm',n)) = sum( h ,DSM_DO_DEMAND.l(n,dsm_shift,h) - corr_fac_dsm_shift(n,dsm_shift,h) ) / report_node('energy generated gross',n) * %sec_hour% + 1e-9 ;

        report_tech('Load shift pos absolute (total)',dsm_shift,n)$(feat_node('dsm',n)) = sum( h , DSM_UP.l(n,dsm_shift,h)) ;
        report_tech('Load shift neg absolute (total)',dsm_shift,n)$(feat_node('dsm',n)) = sum( (h,hh) , DSM_DO.l(n,dsm_shift,h,hh)) ;
        report_node('load curtailment absolute (non-reserves)',n)$(feat_node('dsm',n)) =  sum((dsm_curt,h), DSM_CU.l(n,dsm_curt,h)) * %sec_hour% ;
        report_node('load shift pos absolute (non-reserves)',n)$(feat_node('dsm',n)) =  sum((dsm_shift,h), DSM_UP_DEMAND.l(n,dsm_shift,h)) * %sec_hour% ;
        report_node('load shift neg absolute (non-reserves)',n)$(feat_node('dsm',n)) =  sum((dsm_shift,h), DSM_DO_DEMAND.l(n,dsm_shift,h)) * %sec_hour% ;

                 report_tech_hours('load curtailment (non-reserves)',dsm_curt,h,n)$(report_tech_hours('load curtailment (non-reserves)',dsm_curt,h,n) < eps_rep_abs) = 0 ;
                 report_tech_hours('load shift pos (non-reserves)',dsm_shift,h,n)$(report_tech_hours('load shift pos (non-reserves)',dsm_shift,h,n) < eps_rep_abs) = 0 ;
                 report_tech_hours('load shift neg (non-reserves)',dsm_shift,h,n)$(report_tech_hours('load shift neg (non-reserves)',dsm_shift,h,n) < eps_rep_abs) = 0 ;

                 report_tech('FLH',dsm_shift,n)$(report_tech('FLH',dsm_shift,n) < eps_rep_abs) = 0 ;
                 report_tech('capacities load curtailment',dsm_curt,n)$(report_tech('capacities load curtailment',dsm_curt,n) < eps_rep_ins) = 0 ;
                 report_tech('capacities load shift',dsm_shift,n)$(report_tech('capacities load shift',dsm_shift,n) < eps_rep_ins) = 0 ;
                 report_tech('Capacity share',dsm_curt,n)$(report_tech('Capacity share',dsm_curt,n) < eps_rep_rel) = 0 ;
                 report_tech('Capacity share',dsm_shift,n)$(report_tech('Capacity share',dsm_shift,n) < eps_rep_rel) = 0 ;
                 report_tech('Energy share in nodal gross generation',dsm_curt,n)$(report_tech('Energy share in nodal gross generation',dsm_curt,n) < eps_rep_rel) = 0 ;
                 report_tech('Energy share in nodal gross generation',dsm_shift,n)$(report_tech('Energy share in nodal gross generation',dsm_shift,n) < eps_rep_rel) = 0 ;
                 report_tech('Load shift pos absolute (total)',dsm_shift,n)$(report_tech('Load shift pos absolute (total)',dsm_shift,n) < card(h)*eps_rep_abs) = 0 ;
                 report_tech('Load shift neg absolute (total)',dsm_shift,n)$(report_tech('Load shift neg absolute (total)',dsm_shift,n) < card(h)*eps_rep_abs) = 0 ;
*                 report_tech('Load shift pos absolute (wholesale)',dsm_shift,n)$(report_tech('Load shift pos absolute (wholesale)',dsm_shift,n) < card(h)*eps_rep_abs) = 0 ;
*                 report_tech('Load shift neg absolute (wholesale)',dsm_shift,n)$(report_tech('Load shift neg absolute (wholesale)',dsm_shift,n) < card(h)*eps_rep_abs) = 0 ;
*                 report_tech('Load shift pos absolute (reserves)',dsm_shift,n)$(report_tech('Load shift pos absolute (reserves)',dsm_shift,n) < card(h)*eps_rep_abs) = 0 ;
                 report_node('load curtailment absolute (non-reserves)',n)$(report_node('load curtailment absolute (non-reserves)',n) < card(h)*eps_rep_abs*%sec_hour%) = 0 ;
                 report_node('load shift pos absolute (non-reserves)',n)$(report_node('load shift pos absolute (non-reserves)',n) < card(h)*eps_rep_abs*%sec_hour%) = 0 ;
                 report_node('load shift neg absolute (non-reserves)',n)$(report_node('load shift neg absolute (non-reserves)',n) < card(h)*eps_rep_abs*%sec_hour%) = 0 ;
$ontext
$offtext


* ----------------------------------------------------------------------------

* EV
%EV%$ontext
        report_tech_hours('EV charge',ev,h,n)$(feat_node('ev',n)) =  EV_CHARGE.l(n,ev,h) ;
        report_tech_hours('EV discharge',ev,h,n)$(feat_node('ev',n)) =  EV_DISCHARGE.l(n,ev,h) ;
        report_tech_hours('EV phevfuel consumption',ev,h,n)$(feat_node('ev',n)) =  EV_PHEVFUEL.l(n,ev,h);
        report_tech_hours('EV electrical consumption',ev,h,n)$(feat_node('ev',n)) =  EV_GED.l(n,ev,h);
        report_tech_hours('EV battery level',ev,h,n)$(feat_node('ev',n)) =  EV_L.l(n,ev,h) ;

        report_tech('Energy share in nodal gross generation','ev_cumul',n)$(feat_node('ev',n)) = (sum( (h,ev) , EV_DISCHARGE.l(n,ev,h)) -  sum( h , corr_fac_ev(n,h)))/ report_node('energy generated gross',n) * %sec_hour% + 1e-9 ;
        report_tech('EV charge total wholesale','ev_cumul',n)$(feat_node('ev',n)) = sum((h,ev), report_tech_hours('EV charge',ev,h,n) ) * %sec_hour% ;
        report_tech('EV discharge total wholesale','ev_cumul',n)$(feat_node('ev',n)) = sum((h,ev), report_tech_hours('EV discharge',ev,h,n) ) * %sec_hour% ;
        report_tech('EV phevfuel total consumption','ev_cumul',n)$(feat_node('ev',n)) = sum((h,ev), report_tech_hours('EV phevfuel consumption',ev,h,n) ) * %sec_hour% ;
        report_tech('EV electrical total consumption','ev_cumul',n)$(feat_node('ev',n)) = sum((h,ev), report_tech_hours('EV electrical consumption',ev,h,n) ) * %sec_hour% ;
        report_tech('EV share of electrical consumption','ev_cumul',n)$(feat_node('ev',n) AND sum((h,ev), report_tech_hours('EV electrical consumption',ev,h,n) + report_tech_hours('EV phevfuel consumption',ev,h,n)) > card(h)*eps_rep_abs) = sum((h,ev), report_tech_hours('EV electrical consumption',ev,h,n) ) * %sec_hour% / (sum((h,ev), report_tech_hours('EV electrical consumption',ev,h,n) + report_tech_hours('EV phevfuel consumption',ev,h,n) ) ) ;
        report_tech('EV share of phevfuel consumption','ev_cumul',n)$(feat_node('ev',n) AND sum((h,ev), report_tech_hours('EV electrical consumption',ev,h,n) + report_tech_hours('EV phevfuel consumption',ev,h,n)) > card(h)*eps_rep_abs) = sum((h,ev), report_tech_hours('EV phevfuel consumption',ev,h,n) ) * %sec_hour% / (sum((h,ev), report_tech_hours('EV electrical consumption',ev,h,n) + report_tech_hours('EV phevfuel consumption',ev,h,n) ) ) ;
        report_node('gross_energy_demand w/o EV',n)$feat_node('ev',n) = gross_energy_demand(n) - sum( (ev,h) , EV_GED.l(n,ev,h) ) ;
        report_node('EV electricity demand',n)$(feat_node('ev',n)) = sum( (ev,h) , EV_GED.l(n,ev,h)) ;

***************
*    /\_/\    *
*   ( o.o )   *    >+++> TO BE IMPROVED/CORRECTED
*    > ^ <    *
***************

*        report_node('renshare EV',n)$feat_node('ev',n) = (report_node('renshare in nodal gross demand',n) - report_node('gross_energy_demand w/o EV',n) * report_node('EV electricity demand',n) ) / report_node('energy demand gross',n) ;
*        report_node('conshare EV',n)$feat_node('ev',n) = 1 - report_node('renshare EV',n) ;
*%EV_FREE%%EV_DEFAULT%        report_node('renshare EV',n)$(feat_node('ev',n)) = 1 ;
*%EV_FREE%%EV_DEFAULT%        report_node('conshare EV',n)$(feat_node('ev',n)) = 0 ;

                 report_tech_hours('EV charge',ev,h,n)$(report_tech_hours('EV charge',ev,h,n) < eps_rep_abs) = 0 ;
                 report_tech_hours('EV discharge',ev,h,n)$(report_tech_hours('EV discharge',ev,h,n) < eps_rep_abs) = 0 ;
                 report_tech_hours('EV phevfuel consumption',ev,h,n)$(report_tech_hours('EV phevfuel consumption',ev,h,n) < eps_rep_abs) = 0 ;
                 report_tech_hours('EV electrical consumption',ev,h,n)$(report_tech_hours('EV electrical consumption',ev,h,n) < eps_rep_abs) = 0 ;
                 report_tech_hours('EV battery level',ev,h,n)$(report_tech_hours('EV battery level',ev,h,n) < eps_rep_abs) = 0 ;
                 report_tech('Energy share in nodal gross generation','ev_cumul',n)$(report_tech('Energy share in nodal gross generation','ev_cumul',n) < eps_rep_rel) = 0 ;
                 report_tech('EV charge total wholesale','ev_cumul',n)$(report_tech('EV charge total wholesale','ev_cumul',n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_tech('EV discharge total wholesale','ev_cumul',n)$(report_tech('EV discharge total wholesale','ev_cumul',n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_tech('EV phevfuel total consumption','ev_cumul',n)$(report_tech('EV phevfuel total consumption','ev_cumul',n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_tech('EV electrical total consumption','ev_cumul',n)$(report_tech('EV electrical total consumption','ev_cumul',n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_tech('EV share of electrical consumption','ev_cumul',n)$(report_tech('EV share of electrical consumption','ev_cumul',n) < eps_rep_rel) = 0 ;
                 report_tech('EV share of phevfuel consumption','ev_cumul',n)$(report_tech('EV share of phevfuel consumption','ev_cumul',n) < eps_rep_rel) = 0 ;
                 report_node('EV_electricity_demand',n)$(report_node('EV_electricity_demand',n) < card(h)*eps_rep_abs) = 0 ;
*                 report_node('renshare EV',n)$(report_node('renshare EV',n) < eps_rep_rel) = eps ;
*                 report_node('conshare EV',n)$(report_node('conshare EV',n) < eps_rep_rel) = eps ;
                 report_node('gross_energy_demand w/o EV',n)$(report_node('gross_energy_demand w/o EV',n) < eps_rep_abs) = 0 ;
$ontext
$offtext


* ----------------------------------------------------------------------------

* Reserves
%reserves_endogenous%$ontext
         report_reserves('reserve provision requirements',reserves_prim,n) = feat_node('reserves',n) * phi_reserves_pr_up(n) * sum( reserves_nonprim ,  (1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum( nondis , reserves_slope(n,reserves_nonprim,nondis) * (N_TECH.l(n,nondis) + N_RES_PRO.l(n,nondis))/1000) )) ) ;
         report_reserves('reserve provision requirements',reserves_nonprim,n) = feat_node('reserves',n) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum( nondis , reserves_slope(n,reserves_nonprim,nondis) *  (N_TECH.l(n,nondis) + N_RES_PRO.l(n,nondis))/1000) ) ;

***************
*    /\_/\    *
*   ( o.o )   *    >+++> marginals missing
*    > ^ <    *
***************

         report_reserves_tech_hours('Reserves provision',reserves,'required',h,n)$(ord(h) > 1) = feat_node('reserves',n) * (report_reserves('reserve provision requirements',reserves,n)) ;
         report_reserves_tech_hours('Reserves activation',reserves,'required',h,n)$(ord(h) > 1) = feat_node('reserves',n) * (phi_reserves_call(n,reserves,h) * report_reserves('reserve provision requirements',reserves,n)) ;
$ontext
$offtext
%reserves_exogenous%$ontext
         report_reserves_hours('reserve provision requirements',reserves_prim,h,n) = feat_node('reserves',n) * reserves_exogenous(n,reserves_prim,h) ;
         report_reserves_hours('reserve provision requirements',reserves_nonprim,h,n) = feat_node('reserves',n) * reserves_exogenous(n,reserves_nonprim,h) ;
         report_reserves_tech_hours('Reserves provision',reserves,'required',h,n)$(ord(h) > 1) = feat_node('reserves',n) * (report_reserves_hours('reserve provision requirements',reserves,h,n)) ;
         report_reserves_tech_hours('Reserves activation',reserves,'required',h,n)$(ord(h) > 1) = feat_node('reserves',n) * (phi_reserves_call(n,reserves,h) * report_reserves_hours('reserve provision requirements',reserves,h,n)) ;
$ontext
$offtext

%reserves%$ontext
         report_reserves_tech_hours('Reserves provision',reserves,dis,h,n)$(feat_node('reserves',n)) = RP_DIS.l(n,reserves,dis,h) ;
         report_reserves_tech_hours('Reserves activation',reserves,dis,h,n)$(feat_node('reserves',n)) =  RP_DIS.l(n,reserves,dis,h)*phi_reserves_call(n,reserves,h) ;
         report_reserves_tech_hours('Reserves provision',reserves,nondis,h,n)$(feat_node('reserves',n)) = RP_NONDIS.l(n,reserves,nondis,h) ;
         report_reserves_tech_hours('Reserves activation',reserves,nondis,h,n)$(feat_node('reserves',n)) = RP_NONDIS.l(n,reserves,nondis,h)*phi_reserves_call(n,reserves,h) ;
         report_reserves_tech_hours('Reserves provision',reserves,rsvr,h,n)$(feat_node('reserves',n)) = RP_RSVR.l(n,reserves,rsvr,h) ;
         report_reserves_tech_hours('Reserves activation',reserves,rsvr,h,n)$(feat_node('reserves',n)) = RP_RSVR.l(n,reserves,rsvr,h)*phi_reserves_call(n,reserves,h) ;
         report_reserves_tech_hours('Reserves provision',reserves,sto,h,n)$(feat_node('reserves',n)) = RP_STO_IN.l(n,reserves,sto,h) + RP_STO_OUT.l(n,reserves,sto,h) ;
         report_reserves_tech_hours('Reserves activation',reserves,sto,h,n)$(feat_node('reserves',n)) = (RP_STO_IN.l(n,reserves,sto,h) + RP_STO_OUT.l(n,reserves,sto,h))*phi_reserves_call(n,reserves,h) ;

         report_reserves_tech('Reserves provision ratio',reserves,dis,n)$(feat_node('reserves',n) AND sum( h , G_L.l(n,dis,h) + corr_fac_dis(n,dis,h)) > card(h)*eps_rep_abs)= sum( h , RP_DIS.l(n,reserves,dis,h) ) / sum( h ,  G_L.l(n,dis,h) + corr_fac_dis(n,dis,h)) ;
         report_reserves_tech('Reserves activation ratio',reserves,dis,n)$(feat_node('reserves',n) AND sum( h , G_L.l(n,dis,h) + corr_fac_dis(n,dis,h)) > card(h)*eps_rep_abs) = sum( h ,  RP_DIS.l(n,reserves,dis,h) * phi_reserves_call(n,reserves,h)) / sum( h , G_L.l(n,dis,h) + corr_fac_dis(n,dis,h) ) ;
         report_reserves_tech('Reserves provision ratio',reserves,nondis,n)$(feat_node('reserves',n) AND sum( h , G_RES.l(n,nondis,h)) >  card(h)*eps_rep_abs) = sum( h , RP_NONDIS.l(n,reserves,nondis,h) ) / sum( h , G_RES.l(n,nondis,h)) ;
         report_reserves_tech('Reserves activation ratio',reserves,nondis,n)$(feat_node('reserves',n) AND sum( h , G_RES.l(n,nondis,h)) > card(h)*eps_rep_abs) = sum( h , RP_NONDIS.l(n,reserves,nondis,h) * phi_reserves_call(n,reserves,h)) / sum( h , G_RES.l(n,nondis,h) ) ;
         report_reserves_tech('Reserves provision ratio',reserves,rsvr,n)$(feat_node('reserves',n) AND sum( h , RSVR_OUT.l(n,rsvr,h)) >  card(h)*eps_rep_abs) = sum( h , RP_RSVR.l(n,reserves,rsvr,h) ) / sum( h , RSVR_OUT.l(n,rsvr,h) ) ;
         report_reserves_tech('Reserves activation ratio',reserves,rsvr,n)$(feat_node('reserves',n) AND sum( h , RSVR_OUT.l(n,rsvr,h)) >  card(h)*eps_rep_abs) = sum( h , RP_RSVR.l(n,reserves,rsvr,h) * phi_reserves_call(n,reserves,h) ) / sum( h , RSVR_OUT.l(n,rsvr,h) ) ;
         report_reserves_tech('Reserves provision ratio (storage out positive reserves)',reserves_up,sto,n)$(feat_node('reserves',n) AND  sum(h,STO_OUT.l(n,sto,h)) > card(h)*eps_rep_abs ) = sum( h , RP_STO_OUT.l(n,reserves_up,sto,h) ) / sum( h , STO_OUT.l(n,sto,h) ) ;
         report_reserves_tech('Reserves provision ratio (storage out negative reserves)',reserves_do,sto,n)$(feat_node('reserves',n) AND  sum(h,STO_OUT.l(n,sto,h)) > card(h)*eps_rep_abs ) = sum( h , RP_STO_OUT.l(n,reserves_do,sto,h) ) / sum( h , STO_OUT.l(n,sto,h) ) ;
         report_reserves_tech('Reserves provision ratio (storage in positive reserves)',reserves_up,sto,n)$(feat_node('reserves',n) AND  sum(h,STO_IN.l(n,sto,h)) > card(h)*eps_rep_abs ) = sum( h , RP_STO_IN.l(n,reserves_up,sto,h) ) / sum( h , STO_IN.l(n,sto,h) ) ;
         report_reserves_tech('Reserves provision ratio (storage in negative reserves)',reserves_do,sto,n)$(feat_node('reserves',n) AND  sum(h,STO_IN.l(n,sto,h)) > card(h)*eps_rep_abs ) = sum( h , RP_STO_IN.l(n,reserves_do,sto,h) ) / sum( h , STO_IN.l(n,sto,h) ) ;
         report_reserves_tech('Reserves activation ratio (storage out positive reserves)',reserves_up,sto,n)$(feat_node('reserves',n) AND  sum(h,STO_OUT.l(n,sto,h)) > card(h)*eps_rep_abs ) = sum( h , RP_STO_OUT.l(n,reserves_up,sto,h) * phi_reserves_call(n,reserves_up,h) ) / sum( h , STO_OUT.l(n,sto,h) ) ;
         report_reserves_tech('Reserves activation ratio (storage out negative reserves)',reserves_do,sto,n)$(feat_node('reserves',n) AND  sum(h,STO_OUT.l(n,sto,h)) > card(h)*eps_rep_abs ) = sum( h , RP_STO_OUT.l(n,reserves_do,sto,h) * phi_reserves_call(n,reserves_do,h) ) / sum( h , STO_OUT.l(n,sto,h) ) ;
         report_reserves_tech('Reserves activation ratio (storage in positive reserves)',reserves_up,sto,n)$(feat_node('reserves',n) AND  sum(h,STO_IN.l(n,sto,h)) > card(h)*eps_rep_abs ) = sum( h , RP_STO_IN.l(n,reserves_up,sto,h) * phi_reserves_call(n,reserves_up,h) ) / sum( h , STO_IN.l(n,sto,h) ) ;
         report_reserves_tech('Reserves activation ratio (storage in negative reserves)',reserves_do,sto,n)$(feat_node('reserves',n) AND  sum(h,STO_IN.l(n,sto,h)) > card(h)*eps_rep_abs ) = sum( h , RP_STO_IN.l(n,reserves_do,sto,h) * phi_reserves_call(n,reserves_do,h) ) / sum( h , STO_IN.l(n,sto,h) ) ;
$ontext
$offtext

%reserves_endogenous%$ontext
         report_reserves_tech('reserve provision shares',reserves_nonprim,dis,n)$(feat_node('reserves',n)) = sum( h , RP_DIS.l(n,reserves_nonprim,dis,h)) / ((card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum( nondis ,reserves_slope(n,reserves_nonprim,nondis) * (N_TECH.l(n,nondis) + N_RES_PRO.l(n,nondis))/1000) ) ) ;
         report_reserves_tech('reserve provision shares',reserves_nonprim,nondis,n)$(feat_node('reserves',n)) = sum( h , RP_NONDIS.l(n,reserves_nonprim,nondis,h)) / ((card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum( nondisnondis ,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis) + N_RES_PRO.l(n,nondisnondis))/1000) )) ;
         report_reserves_tech('reserve provision shares',reserves_nonprim,rsvr,n)$(feat_node('reserves',n)) = sum( h , RP_RSVR.l(n,reserves_nonprim,rsvr,h)) / ((card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis) + N_RES_PRO.l(n,nondisnondis))/1000) )) ;
         report_reserves_tech('reserve provision shares',reserves_nonprim,sto,n)$(feat_node('reserves',n)) = sum( h , RP_STO_IN.l(n,reserves_nonprim,sto,h) + RP_STO_OUT.l(n,reserves_nonprim,sto,h)) / ((card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000) )) ;
         report_reserves_tech('reserve provision shares',reserves_prim,dis,n)$(feat_node('reserves',n)) = sum( h , RP_DIS.l(n,reserves_prim,dis,h)) / (phi_reserves_pr_up(n)* sum( reserves_nonprim ,  ((card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000) )) )) ;
         report_reserves_tech('reserve provision shares',reserves_prim,nondis,n)$(feat_node('reserves',n)) = sum( h ,RP_NONDIS.l(n,reserves_prim,nondis,h)) / (phi_reserves_pr_up(n) * sum( reserves_nonprim ,  ((card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )) )) ;
         report_reserves_tech('reserve provision shares',reserves_prim,rsvr,n)$(feat_node('reserves',n)) = sum( h , RP_RSVR.l(n,reserves_prim,rsvr,h)) / (phi_reserves_pr_up(n) * sum( reserves_nonprim ,  ((card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )) )) ;
         report_reserves_tech('reserve provision shares',reserves_prim,sto,n)$(feat_node('reserves',n)) = sum( h , RP_STO_IN.l(n,reserves_prim,sto,h) + RP_STO_OUT.l(n,reserves_prim,sto,h)) / (phi_reserves_pr_up(n) * sum( reserves_nonprim ,  ((card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis) + N_RES_PRO.l(n,nondisnondis))/1000) )) ));
         report_reserves_tech('reserve activation shares',reserves_prim,dis,n)$(feat_node('reserves',n)) = sum(h,RP_DIS.l(n,reserves_prim,dis,h)*phi_reserves_call(n,reserves_prim,h)) / sum( h , phi_reserves_call(n,reserves_prim,h) *  phi_reserves_pr_up(n) * sum( reserves_nonprim , 1000 * phi_reserves_share(n,reserves_nonprim) * ( reserves_intercept(n,reserves_nonprim) + sum(nondisnondis , reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000 ) ) )   ) ;
         report_reserves_tech('reserve activation shares',reserves_prim,nondis,n)$(feat_node('reserves',n)) = sum(h,RP_NONDIS.l(n,reserves_prim,nondis,h)*phi_reserves_call(n,reserves_prim,h)) / sum( h , phi_reserves_call(n,reserves_prim,h) *  phi_reserves_pr_up(n)* sum( reserves_nonprim , 1000 * phi_reserves_share(n,reserves_nonprim) * ( reserves_intercept(n,reserves_nonprim) + sum(nondisnondis , reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis) + N_RES_PRO.l(n,nondisnondis))/1000 ) ) )   ) ;
         report_reserves_tech('reserve activation shares',reserves_prim,rsvr,n)$(feat_node('reserves',n)) = sum(h,RP_RSVR.l(n,reserves_prim,rsvr,h)*phi_reserves_call(n,reserves_prim,h)) / sum( h , phi_reserves_call(n,reserves_prim,h) *  phi_reserves_pr_up(n)* sum( reserves_nonprim , 1000 * phi_reserves_share(n,reserves_nonprim) * ( reserves_intercept(n,reserves_nonprim) + sum(nondisnondis , reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis) + N_RES_PRO.l(n,nondisnondis))/1000 ) ) )   ) ;
         report_reserves_tech('reserve activation shares',reserves_prim,sto,n)$(feat_node('reserves',n)) = sum(h,(RP_STO_IN.l(n,reserves_prim,sto,h) + RP_STO_OUT.l(n,reserves_prim,sto,h))*phi_reserves_call(n,reserves_prim,h)) / sum( h , phi_reserves_call(n,reserves_prim,h) *  phi_reserves_pr_up(n) * sum( reserves_nonprim , 1000 * phi_reserves_share(n,reserves_nonprim) * ( reserves_intercept(n,reserves_nonprim) + sum( nondisnondis , reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000 ) ) )   );
         report_reserves_tech('reserve activation shares',reserves_nonprim,dis,n)$(feat_node('reserves',n)) = sum(h,RP_DIS.l(n,reserves_nonprim,dis,h)*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )) ;
         report_reserves_tech('reserve activation shares',reserves_nonprim,nondis,n)$(feat_node('reserves',n)) = sum(h,RP_NONDIS.l(n,reserves_nonprim,nondis,h)*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis) + N_RES_PRO.l(n,nondisnondis))/1000) )) ;
         report_reserves_tech('reserve activation shares',reserves_nonprim,rsvr,n)$(feat_node('reserves',n)) = sum(h,RP_RSVR.l(n,reserves_nonprim,rsvr,h)*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000) )) ;
         report_reserves_tech('reserve activation shares',reserves_nonprim,sto,n)$(feat_node('reserves',n)) = sum(h,(RP_STO_IN.l(n,reserves_nonprim,sto,h) + RP_STO_OUT.l(n,reserves_nonprim,sto,h))*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis) + N_RES_PRO.l(n,nondisnondis))/1000) )) ;
$ontext
$offtext

%reserves_exogenous%$ontext
         report_reserves_tech('reserve provision shares',reserves_nonprim,dis,n)$(feat_node('reserves',n)) = sum( h , RP_DIS.l(n,reserves_nonprim,dis,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_nonprim,h))  ;
         report_reserves_tech('reserve provision shares',reserves_nonprim,nondis,n)$(feat_node('reserves',n)) = sum( h , RP_NONDIS.l(n,reserves_nonprim,nondis,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_nonprim,h)) ;
         report_reserves_tech('reserve provision shares',reserves_nonprim,rsvr,n)$(feat_node('reserves',n)) = sum( h , RP_RSVR.l(n,reserves_nonprim,rsvr,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_nonprim,h)) ;
         report_reserves_tech('reserve provision shares',reserves_nonprim,sto,n)$(feat_node('reserves',n)) = sum( h , RP_STO_IN.l(n,reserves_nonprim,sto,h) + RP_STO_OUT.l(n,reserves_nonprim,sto,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_nonprim,h)) ;
         report_reserves_tech('reserve provision shares',reserves_prim,dis,n)$(feat_node('reserves',n)) = sum( h , RP_DIS.l(n,reserves_prim,dis,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_prim,h)) ;
         report_reserves_tech('reserve provision shares',reserves_prim,nondis,n)$(feat_node('reserves',n)) = sum( h , RP_NONDIS.l(n,reserves_prim,nondis,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_prim,h)) ;
         report_reserves_tech('reserve provision shares',reserves_prim,rsvr,n)$(feat_node('reserves',n)) = sum( h , RP_RSVR.l(n,reserves_prim,rsvr,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_prim,h)) ;
         report_reserves_tech('reserve provision shares',reserves_prim,sto,n)$(feat_node('reserves',n)) = sum( h , RP_STO_IN.l(n,reserves_prim,sto,h) + RP_STO_OUT.l(n,reserves_prim,sto,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_prim,h)) ;
         report_reserves_tech('reserve activation shares',reserves_prim,dis,n)$(feat_node('reserves',n)) = sum(h,RP_DIS.l(n,reserves_prim,dis,h)*phi_reserves_call(n,reserves_prim,h)) / sum( h , phi_reserves_call(n,reserves_prim,h) * reserves_exogenous(n,reserves_prim,h) )     ;
         report_reserves_tech('reserve activation shares',reserves_prim,nondis,n)$(feat_node('reserves',n)) = sum(h,RP_NONDIS.l(n,reserves_prim,nondis,h)*phi_reserves_call(n,reserves_prim,h)) / sum( h , phi_reserves_call(n,reserves_prim,h) * reserves_exogenous(n,reserves_prim,h) ) ;
         report_reserves_tech('reserve activation shares',reserves_prim,rsvr,n)$(feat_node('reserves',n)) = sum(h,RP_RSVR.l(n,reserves_prim,rsvr,h)*phi_reserves_call(n,reserves_prim,h)) / sum( h , phi_reserves_call(n,reserves_prim,h) * reserves_exogenous(n,reserves_prim,h) ) ;
         report_reserves_tech('reserve activation shares',reserves_prim,sto,n)$(feat_node('reserves',n)) = sum(h,(RP_STO_IN.l(n,reserves_prim,sto,h) + RP_STO_OUT.l(n,reserves_prim,sto,h))*phi_reserves_call(n,reserves_prim,h)) / sum( h , phi_reserves_call(n,reserves_prim,h) * reserves_exogenous(n,reserves_prim,h) );
         report_reserves_tech('reserve activation shares',reserves_nonprim,dis,n)$(feat_node('reserves',n)) = sum(h,RP_DIS.l(n,reserves_nonprim,dis,h)*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * reserves_exogenous(n,reserves_nonprim,h) ) ;
         report_reserves_tech('reserve activation shares',reserves_nonprim,nondis,n)$(feat_node('reserves',n)) = sum(h,RP_NONDIS.l(n,reserves_nonprim,nondis,h)*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * reserves_exogenous(n,reserves_nonprim,h) ) ;
         report_reserves_tech('reserve activation shares',reserves_nonprim,rsvr,n)$(feat_node('reserves',n)) = sum(h,RP_RSVR.l(n,reserves_nonprim,rsvr,h)*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * reserves_exogenous(n,reserves_nonprim,h) ) ;
         report_reserves_tech('reserve activation shares',reserves_nonprim,sto,n)$(feat_node('reserves',n)) = sum(h,(RP_STO_IN.l(n,reserves_nonprim,sto,h) + RP_STO_OUT.l(n,reserves_nonprim,sto,h))*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * reserves_exogenous(n,reserves_nonprim,h) ) ;
$ontext
$offtext

%reserves%$ontext
         report_tech('Storage positive reserves activation by storage in',sto,n)$(feat_node('reserves',n)) = sum( (h,reserves_up) , RP_STO_IN.l(n,reserves_up,sto,h) * phi_reserves_call(n,reserves_up,h)) * %sec_hour% ;
         report_tech('Storage negative reserves activation by storage in',sto,n)$(feat_node('reserves',n)) = sum( (h,reserves_do) , RP_STO_IN.l(n,reserves_do,sto,h) * phi_reserves_call(n,reserves_do,h)) * %sec_hour% ;
         report_tech('Storage positive reserves activation by storage out',sto,n)$(feat_node('reserves',n)) = sum( (h,reserves_up) , RP_STO_OUT.l(n,reserves_up,sto,h) * phi_reserves_call(n,reserves_up,h)) * %sec_hour% ;
         report_tech('Storage negative reserves activation by storage out',sto,n)$(feat_node('reserves',n)) = sum( (h,reserves_do) ,RP_STO_OUT.l(n,reserves_do,sto,h) * phi_reserves_call(n,reserves_do,h)) * %sec_hour% ;
         report_tech('Load shift pos absolute (non-reserves)',dsm_shift,n)$(feat_node('reserves',n)) = sum( h , DSM_UP_DEMAND.l(n,dsm_shift,h) ) ;
         report_tech('Load shift neg absolute (non-reserves)',dsm_shift,n)$(feat_node('reserves',n)) = sum( h , DSM_DO_DEMAND.l(n,dsm_shift,h) ) ;

         report_tech('FLH',sto,n)$(feat_node('reserves',n) AND N_STO_P_IN.l(n,sto) > eps_rep_ins) = ( report_tech('Storage out total non-reserves',sto,n)
                        + sum( (h,reserves_up) , RP_STO_OUT.l(n,reserves_up,sto,h) * phi_reserves_call(n,reserves_up,h)) * %sec_hour%
                        - sum( (h,reserves_do) ,RP_STO_OUT.l(n,reserves_do,sto,h) * phi_reserves_call(n,reserves_do,h)) * %sec_hour%
                        ) / N_STO_P_IN.l(n,sto) ;
         report_tech('Storage cycles',sto,n)$(feat_node('reserves',n) AND N_STO_E.l(n,sto) > eps_rep_ins) = ( report_tech('Storage out total non-reserves',sto,n)
                        + sum( (h,reserves_up) , RP_STO_OUT.l(n,reserves_up,sto,h) * phi_reserves_call(n,reserves_up,h)) * %sec_hour%
                        - sum( (h,reserves_do) , RP_STO_OUT.l(n,reserves_do,sto,h) * phi_reserves_call(n,reserves_do,h)) * %sec_hour%
                        ) / N_STO_E.l(n,sto) * %sec_hour% ;
*         report_tech('Storage EP-ratio',sto,n)$(feat_node('reserves',n) AND (N_STO_P_IN.l(n,sto) + N_STO_P_PRO.l(n,sto) ) > eps_rep_ins AND (N_STO_E.l(n,sto) + N_STO_E_PRO.l(n,sto) ) * %sec_hour% > eps_rep_ins ) = (N_STO_E.l(n,sto) + N_STO_E_PRO.l(n,sto) ) * %sec_hour% / (N_STO_P.l(n,sto) + N_STO_P_PRO.l(n,sto) ) ;


%reserves_exogenous%                 report_reserves('reserve provision requirements',reserves_prim,n)$(feat_node('reserves',n) AND report_reserves('reserve provision requirements',reserves_prim,n) < eps_rep_abs) = 0 ;
%reserves_exogenous%                 report_reserves('reserve provision requirements',reserves_nonprim,n)$(feat_node('reserves',n) AND report_reserves('reserve provision requirements',reserves_nonprim,n) < eps_rep_abs) = 0 ;
%reserves_endogenous%                report_reserves_hours('reserve provision requirements',reserves_prim,h,n)$(feat_node('reserves',n) AND report_reserves_hours('reserve provision requirements',reserves_prim,h,n) < eps_rep_abs) = 0 ;
%reserves_endogenous%                report_reserves_hours('reserve provision requirements',reserves_nonprim,h,n)$(feat_node('reserves',n) AND report_reserves_hours('reserve provision requirements',reserves_nonprim,h,n) < eps_rep_abs) = 0 ;

                 report_reserves_tech_hours('Reserves provision',reserves,'required',h,n)$(report_reserves_tech_hours('Reserves provision',reserves,'required',h,n) < eps_rep_abs ) = 0 ;
                 report_reserves_tech_hours('Reserves activation',reserves,'required',h,n)$(report_reserves_tech_hours('Reserves activation',reserves,'required',h,n) < eps_rep_abs ) = 0 ;
                 report_reserves_tech_hours('Reserves provision',reserves,dis,h,n)$(report_reserves_tech_hours('Reserves provision',reserves,dis,h,n) < eps_rep_abs ) = 0;
                 report_reserves_tech_hours('Reserves activation',reserves,dis,h,n)$(report_reserves_tech_hours('Reserves activation',reserves,dis,h,n) < eps_rep_abs ) = 0 ;
                 report_reserves_tech_hours('Reserves provision',reserves,nondis,h,n)$(report_reserves_tech_hours('Reserves provision',reserves,nondis,h,n) < eps_rep_abs ) = 0 ;
                 report_reserves_tech_hours('Reserves activation',reserves,nondis,h,n)$(report_reserves_tech_hours('Reserves activation',reserves,nondis,h,n) < eps_rep_abs ) = 0 ;
                 report_reserves_tech_hours('Reserves provision',reserves,sto,h,n)$(report_reserves_tech_hours('Reserves provision',reserves,sto,h,n) < eps_rep_abs ) = 0 ;
                 report_reserves_tech_hours('Reserves activation',reserves,sto,h,n)$(report_reserves_tech_hours('Reserves activation',reserves,sto,h,n) < eps_rep_abs ) = 0 ;
                 report_reserves_tech_hours('Reserves provision',reserves,rsvr,h,n)$(report_reserves_tech_hours('Reserves provision',reserves,rsvr,h,n) < eps_rep_abs) = 0 ;
                 report_reserves_tech_hours('Reserves activation',reserves,rsvr,h,n)$(report_reserves_tech_hours('Reserves activation',reserves,rsvr,h,n) < eps_rep_abs) = 0 ;

                 report_reserves_tech('Reserves provision ratio',reserves,dis,n)$(report_reserves_tech('Reserves provision ratio',reserves,dis,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('Reserves activation ratio',reserves,dis,n)$(report_reserves_tech('Reserves activation ratio',reserves,dis,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('Reserves provision ratio',reserves,nondis,n)$(report_reserves_tech('Reserves provision ratio',reserves,nondis,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('Reserves activation ratio',reserves,nondis,n)$(report_reserves_tech('Reserves activation ratio',reserves,nondis,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('Reserves provision ratio',reserves,rsvr,n)$(report_reserves_tech('Reserves provision ratio',reserves,rsvr,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('Reserves activation ratio',reserves,rsvr,n)$(report_reserves_tech('Reserves activation ratio',reserves,rsvr,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('Reserves provision ratio (storage out positive reserves)',reserves,sto,n)$(report_reserves_tech('Reserves provision ratio (storage out positive reserves)',reserves,sto,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('Reserves provision ratio (storage out negative reserves)',reserves,sto,n)$(report_reserves_tech('Reserves provision ratio (storage out negative reserves)',reserves,sto,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('Reserves provision ratio (storage in positive reserves)',reserves,sto,n)$(report_reserves_tech('Reserves provision ratio (storage in positive reserves)',reserves,sto,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('Reserves provision ratio (storage in negative reserves)',reserves,sto,n)$(report_reserves_tech('Reserves provision ratio (storage in negative reserves)',reserves,sto,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('Reserves activation ratio (storage out positive reserves)',reserves,sto,n)$(report_reserves_tech('Reserves activation ratio (storage out positive reserves)',reserves,sto,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('Reserves activation ratio (storage out negative reserves)',reserves,sto,n)$(report_reserves_tech('Reserves activation ratio (storage out negative reserves)',reserves,sto,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('Reserves activation ratio (storage in positive reserves)',reserves,sto,n)$(report_reserves_tech('Reserves activation ratio (storage in positive reserves)',reserves,sto,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('Reserves activation ratio (storage in negative reserves)',reserves,sto,n)$(report_reserves_tech('Reserves activation ratio (storage in negative reserves)',reserves,sto,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve provision shares',reserves_nonprim,dis,n)$(report_reserves_tech('reserve provision shares',reserves_nonprim,dis,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve provision shares',reserves_nonprim,nondis,n)$(report_reserves_tech('reserve provision shares',reserves_nonprim,nondis,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve provision shares',reserves_nonprim,rsvr,n)$(report_reserves_tech('reserve provision shares',reserves_nonprim,rsvr,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve provision shares',reserves_nonprim,sto,n)$(report_reserves_tech('reserve provision shares',reserves_nonprim,sto,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve provision shares',reserves_prim,dis,n)$(report_reserves_tech('reserve provision shares',reserves_prim,dis,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve provision shares',reserves_prim,nondis,n)$(report_reserves_tech('reserve provision shares',reserves_prim,nondis,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve provision shares',reserves_prim,rsvr,n)$(report_reserves_tech('reserve provision shares',reserves_prim,rsvr,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve provision shares',reserves_prim,sto,n)$(report_reserves_tech('reserve provision shares',reserves_prim,sto,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_prim,dis,n)$(report_reserves_tech('reserve activation shares',reserves_prim,dis,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_prim,nondis,n)$(report_reserves_tech('reserve activation shares',reserves_prim,nondis,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_prim,rsvr,n)$(report_reserves_tech('reserve activation shares',reserves_prim,rsvr,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_prim,sto,n)$(report_reserves_tech('reserve activation shares',reserves_prim,sto,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_nonprim,dis,n)$(report_reserves_tech('reserve activation shares',reserves_nonprim,dis,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_nonprim,nondis,n)$(report_reserves_tech('reserve activation shares',reserves_nonprim,nondis,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_nonprim,rsvr,n)$(report_reserves_tech('reserve activation shares',reserves_nonprim,rsvr,n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_nonprim,sto,n)$(report_reserves_tech('reserve activation shares',reserves_nonprim,sto,n) < eps_rep_rel) = 0 ;

                 report_tech('Storage positive reserves activation by storage in',sto,n)$(report_tech('Storage positive reserves activation by storage in',sto,n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_tech('Storage negative reserves activation by storage in',sto,n)$(report_tech('Storage negative reserves activation by storage in',sto,n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_tech('Storage positive reserves activation by storage out',sto,n)$(report_tech('Storage positive reserves activation by storage out',sto,n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_tech('Storage negative reserves activation by storage out',sto,n)$(report_tech('Storage negative reserves activation by storage out',sto,n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_tech('FLH',sto,n)$(report_tech('FLH',sto,n) < eps_rep_abs) = 0 ;
                 report_tech('Storage cycles',sto,n)$(report_tech('Storage cycles',sto,n) < eps_rep_abs) = 0 ;
                 report_tech('Storage EP-ratio',sto,n)$(report_tech('Storage EP-ratio',sto,n) < eps_rep_rel) = 0 ;
                 report_tech('Load shift pos absolute (non-reserves)',dsm_shift,n)$(report_tech('Load shift pos absolute (non-reserves)',dsm_shift,n) < card(h) * eps_rep_abs) = 0 ;
                 report_tech('Load shift neg absolute (non-reserves)',dsm_shift,n)$(report_tech('Load shift neg absolute (non-reserves)',dsm_shift,n) < card(h) * eps_rep_abs) = 0 ;
$ontext
$offtext


* ----------------------------------------------------------------------------

* Reserves and DSM
%DSM%$ontext
%reserves_endogenous%$ontext
        report_reserves_tech('reserve provision shares',reserves_nonprim,dsm_curt,n)$(feat_node('dsm',n) AND feat_node('reserves',n)) = (sum( h , RP_DSM_CU.l(n,reserves_nonprim,dsm_curt,h)) / ( (card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )))  ;
        report_reserves_tech('reserve provision shares',reserves_nonprim,dsm_shift,n)$(feat_node('dsm',n) AND feat_node('reserves',n)) = (sum( h , RP_DSM_SHIFT.l(n,reserves_nonprim,dsm_shift,h)) / ( (card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )))   ;
        report_reserves_tech('reserve activation shares',reserves_nonprim,dsm_curt,n)$(feat_node('dsm',n) AND feat_node('reserves',n)) = (sum( h , RP_DSM_CU.l(n,reserves_nonprim,dsm_curt,h)*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )))  ;
        report_reserves_tech('reserve activation shares',reserves_nonprim,dsm_shift,n)$(feat_node('dsm',n) AND feat_node('reserves',n)) = (sum( h , RP_DSM_SHIFT.l(n,reserves_nonprim,dsm_shift,h) * phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )))   ;
$ontext
$offtext

%reserves_exogenous%$ontext
        report_reserves_tech('reserve provision shares',reserves_nonprim,dsm_curt,n)$(feat_node('dsm',n) AND feat_node('reserves',n)) = sum( h , RP_DSM_CU.l(n,reserves_nonprim,dsm_curt,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_nonprim,h)) ;
        report_reserves_tech('reserve provision shares',reserves_nonprim,dsm_shift,n)$(feat_node('dsm',n) AND feat_node('reserves',n)) = sum( h , RP_DSM_SHIFT.l(n,reserves_nonprim,dsm_shift,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_nonprim,h))   ;
        report_reserves_tech('reserve activation shares',reserves_nonprim,dsm_curt,n)$(feat_node('dsm',n) AND feat_node('reserves',n)) = sum( h , RP_DSM_CU.l(n,reserves_nonprim,dsm_curt,h)*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * reserves_exogenous(n,reserves_nonprim,h))  ;
        report_reserves_tech('reserve activation shares',reserves_nonprim,dsm_shift,n)$(feat_node('dsm',n) AND feat_node('reserves',n)) = sum( h , RP_DSM_SHIFT.l(n,reserves_nonprim,dsm_shift,h) * phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * reserves_exogenous(n,reserves_nonprim,h))   ;
$ontext
$offtext
%DSM%$ontext
%reserves%$ontext
        report_reserves_tech_hours('Reserves provision',reserves,dsm_shift,h,n)$(feat_node('dsm',n) AND feat_node('reserves',n)) = RP_DSM_SHIFT.l(n,reserves,dsm_shift,h)  ;
        report_reserves_tech_hours('Reserves activation',reserves,dsm_shift,h,n)$(feat_node('dsm',n) AND feat_node('reserves',n)) = RP_DSM_SHIFT.l(n,reserves,dsm_shift,h)*phi_reserves_call(n,reserves,h)  ;
        report_reserves_tech_hours('Reserves provision',reserves,dsm_curt,h,n)$(feat_node('dsm',n) AND feat_node('reserves',n)) = RP_DSM_CU.l(n,reserves,dsm_curt,h)  ;
        report_reserves_tech_hours('Reserves activation',reserves,dsm_curt,h,n)$(feat_node('dsm',n) AND feat_node('reserves',n)) = RP_DSM_CU.l(n,reserves,dsm_curt,h)*phi_reserves_call(n,reserves,h)  ;

        report_tech('Load shift pos absolute (reserves)',dsm_shift,n)$(feat_node('dsm',n) AND feat_node('reserves',n)) = report_tech('Load shift pos absolute (total)',dsm_shift,n) - report_tech('Load shift pos absolute (non-reserves)',dsm_shift,n) ;
        report_tech('Load shift neg absolute (reserves)',dsm_shift,n)$(feat_node('dsm',n) AND feat_node('reserves',n)) = report_tech('Load shift neg absolute (total)',dsm_shift,n) - report_tech('Load shift neg absolute (non-reserves)',dsm_shift,n) ;

                 report_reserves_tech('reserve provision shares',reserves_nonprim,dsm_curt,n)$(report_reserves_tech('reserve provision shares',reserves_nonprim,dsm_curt,n) < eps_rep_rel ) = 0 ;
                 report_reserves_tech('reserve provision shares',reserves_nonprim,dsm_shift,n)$(report_reserves_tech('reserve provision shares',reserves_nonprim,dsm_shift,n) < eps_rep_rel ) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_nonprim,dsm_curt,n)$(report_reserves_tech('reserve activation shares',reserves_nonprim,dsm_curt,n) < eps_rep_rel ) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_nonprim,dsm_shift,n)$(report_reserves_tech('reserve activation shares',reserves_nonprim,dsm_shift,n) < eps_rep_rel ) = 0 ;
                 report_reserves_tech_hours('Reserves provision',reserves,dsm_shift,h,n)$(report_reserves_tech_hours('Reserves provision',reserves,dsm_shift,h,n) < eps_rep_abs) = 0 ;
                 report_reserves_tech_hours('Reserves activation',reserves,dsm_shift,h,n)$(report_reserves_tech_hours('Reserves activation',reserves,dsm_shift,h,n) < eps_rep_abs ) = 0 ;
                 report_reserves_tech_hours('Reserves provision',reserves,dsm_curt,h,n)$(report_reserves_tech_hours('Reserves provision',reserves,dsm_curt,h,n) < eps_rep_abs ) = 0 ;
                 report_reserves_tech_hours('Reserves activation',reserves,dsm_curt,h,n)$(report_reserves_tech_hours('Reserves activation',reserves,dsm_curt,h,n) < eps_rep_abs ) = 0 ;
                 report_tech('Load shift neg absolute (reserves)',dsm_shift,n)$(report_tech('Load shift neg absolute (reserves)',dsm_shift,n) < card(h)*eps_rep_abs) = 0 ;
                 report_tech('Load shift pos absolute (reserves)',dsm_shift,n)$(report_tech('Load shift pos absolute (reserves)',dsm_shift,n) < card(h)*eps_rep_abs) = 0 ;
$ontext
$offtext


* ----------------------------------------------------------------------------


* Reserves and EV
%EV%$ontext
%reserves%$ontext
       report_reserves_tech_hours('Reserves provision',reserves,'ev_cum',h,n)$(feat_node('ev',n) AND feat_node('reserves',n)) =  sum( ev , RP_EV_V2G.l(n,reserves,ev,h))+ sum( ev , RP_EV_G2V.l(n,reserves,ev,h))  ;
       report_reserves_tech_hours('Reserves activation',reserves,'ev_cum',h,n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( ev , phi_reserves_call(n,reserves,h) * RP_EV_V2G.l(n,reserves,ev,h))+ sum( ev , phi_reserves_call(n,reserves,h) * RP_EV_G2V.l(n,reserves,ev,h))  ;
$ontext
$offtext
%EV%$ontext
%reserves_endogenous%$ontext
       report_reserves_tech('reserve provision shares',reserves_prim,'ev_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (h,ev), RP_EV_V2G.l(n,reserves_prim,ev,h)+ RP_EV_G2V.l(n,reserves_prim,ev,h)) / (phi_reserves_pr_up(n) * sum( reserves_nonprim ,  ((card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )) ));
       report_reserves_tech('reserve provision shares',reserves_prim,'G2V_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (h,ev), RP_EV_G2V.l(n,reserves_prim,ev,h)) / (phi_reserves_pr_up(n) * sum( reserves_nonprim ,  ((card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )) ));
       report_reserves_tech('reserve provision shares',reserves_prim,'V2G_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (h,ev), RP_EV_V2G.l(n,reserves_prim,ev,h)) / (phi_reserves_pr_up(n) * sum( reserves_nonprim ,  ((card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )) ));
       report_reserves_tech('reserve provision shares',reserves_nonprim,'ev_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (h,ev) ,RP_EV_V2G.l(n,reserves_nonprim,ev,h)+ RP_EV_G2V.l(n,reserves_nonprim,ev,h)) / ((card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000) )) ;
       report_reserves_tech('reserve provision shares',reserves_nonprim,'G2V_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (h,ev) , RP_EV_G2V.l(n,reserves_nonprim,ev,h)) / ((card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )) ;
       report_reserves_tech('reserve provision shares',reserves_nonprim,'V2G_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (h,ev) , RP_EV_V2G.l(n,reserves_nonprim,ev,h)) / ((card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )) ;
       report_reserves_tech('reserve activation shares',reserves_prim,'ev_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum((h,ev),(RP_EV_V2G.l(n,reserves_prim,ev,h) + RP_EV_G2V.l(n,reserves_prim,ev,h))*phi_reserves_call(n,reserves_prim,h)) / sum( h , phi_reserves_call(n,reserves_prim,h) *  phi_reserves_pr_up(n) * sum( reserves_nonprim , 1000 * phi_reserves_share(n,reserves_nonprim) * ( reserves_intercept(n,reserves_nonprim) + sum(nondisnondis , reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000 ) ) )   ) ;
       report_reserves_tech('reserve activation shares',reserves_prim,'G2V_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum((h,ev),RP_EV_G2V.l(n,reserves_prim,ev,h)* phi_reserves_call(n,reserves_prim,h)) / sum( h , phi_reserves_call(n,reserves_prim,h) *  phi_reserves_pr_up(n) * sum( reserves_nonprim , 1000 * phi_reserves_share(n,reserves_nonprim) * ( reserves_intercept(n,reserves_nonprim) + sum(nondisnondis , reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000 ) ) )   ) ;
       report_reserves_tech('reserve activation shares',reserves_prim,'V2G_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum((h,ev),RP_EV_V2G.l(n,reserves_prim,ev,h)* phi_reserves_call(n,reserves_prim,h)) /  sum( h , phi_reserves_call(n,reserves_prim,h) *  phi_reserves_pr_up(n) * sum( reserves_nonprim , 1000 * phi_reserves_share(n,reserves_nonprim) * ( reserves_intercept(n,reserves_nonprim) + sum(nondisnondis , reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000 ) ) )   ) ;
       report_reserves_tech('reserve activation shares',reserves_nonprim,'ev_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = (sum((h,ev),(RP_EV_V2G.l(n,reserves_nonprim,ev,h)+ RP_EV_G2V.l(n,reserves_nonprim,ev,h))*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000) ))) ;
       report_reserves_tech('reserve activation shares',reserves_nonprim,'G2V_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = (sum((h,ev),RP_EV_G2V.l(n,reserves_nonprim,ev,h)*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000) ))) ;
       report_reserves_tech('reserve activation shares',reserves_nonprim,'V2G_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = (sum((h,ev),RP_EV_V2G.l(n,reserves_nonprim,ev,h)*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000) ))) ;
$ontext
$offtext
%EV%$ontext
%reserves_exogenous%$ontext
       report_reserves_tech('reserve provision shares',reserves_prim,'ev_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (h,ev), RP_EV_V2G.l(n,reserves_prim,ev,h)+ RP_EV_G2V.l(n,reserves_prim,ev,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_prim,h)) ;
       report_reserves_tech('reserve provision shares',reserves_prim,'G2V_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (h,ev), RP_EV_G2V.l(n,reserves_prim,ev,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_prim,h)) ;
       report_reserves_tech('reserve provision shares',reserves_prim,'V2G_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (h,ev), RP_EV_V2G.l(n,reserves_prim,ev,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_prim,h)) ;
       report_reserves_tech('reserve provision shares',reserves_nonprim,'ev_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (h,ev) , RP_EV_V2G.l(n,reserves_nonprim,ev,h) + RP_EV_G2V.l(n,reserves_nonprim,ev,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_nonprim,h)) ;
       report_reserves_tech('reserve provision shares',reserves_nonprim,'G2V_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (h,ev) , RP_EV_G2V.l(n,reserves_nonprim,ev,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_nonprim,h)) ;
       report_reserves_tech('reserve provision shares',reserves_nonprim,'V2G_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (h,ev) , RP_EV_V2G.l(n,reserves_nonprim,ev,h)) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_nonprim,h)) ;

***************
*    /\_/\    *
*   ( o.o )   *    >+++> TO BE IMPROVED/CORRECTED
*    > ^ <    *
***************

*       report_reserves_tech('reserve activation shares',reserves_prim,'ev_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum((h,ev),(RP_EV_V2G.l(n,reserves_prim,ev,h))+ RP_EV_G2V.l(n,reserves_prim,ev,h))*phi_reserves_call(n,reserves_prim,h)) / sum( h , phi_reserves_call(n,reserves_prim,h) *  phi_reserves_pr_up(n) * sum( reserves_nonprim , 1000 * phi_reserves_share(n,reserves_nonprim) * ( reserves_intercept(n,reserves_nonprim) + sum(nondisnondis , reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000 ) ) )   ) ;
*       report_reserves_tech('reserve activation shares',reserves_prim,'G2V_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum((h,ev),RP_EV_G2V.l(n,reserves_prim,ev,h)*phi_reserves_call(n,reserves_prim,h)) / sum( h , phi_reserves_call(n,reserves_prim,h) *  phi_reserves_pr_up(n) * sum( reserves_nonprim , 1000 * phi_reserves_share(n,reserves_nonprim) * ( reserves_intercept(n,reserves_nonprim) + sum(nondisnondis , reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000 ) ) )   ) ;
*       report_reserves_tech('reserve activation shares',reserves_prim,'V2G_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum((h,ev),RP_EV_V2G.l(n,reserves_prim,ev,h)*phi_reserves_call(n,reserves_prim,h)) /  sum( h , phi_reserves_call(n,reserves_prim,h) *  phi_reserves_pr_up(n) * sum( reserves_nonprim , 1000 * phi_reserves_share(n,reserves_nonprim) * ( reserves_intercept(n,reserves_nonprim) + sum(nondisnondis , reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000 ) ) )   ) ;
*       report_reserves_tech('reserve activation shares',reserves_nonprim,'ev_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = (sum((h,ev),(RP_EV_V2G.l(n,reserves_nonprim,ev,h)+ RP_EV_G2V.l(n,reserves_nonprim,ev,h))*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000) ))) ;
*       report_reserves_tech('reserve activation shares',reserves_nonprim,'G2V_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = (sum((h,ev),RP_EV_G2V.l(n,reserves_nonprim,ev,h)*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000) ))) ;
*       report_reserves_tech('reserve activation shares',reserves_nonprim,'V2G_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = (sum((h,ev),RP_EV_V2G.l(n,reserves_nonprim,ev,h)*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000) ))) ;
$ontext
$offtext
%EV%$ontext
%reserves%$ontext
       report_tech('EV positive reserves activation by charging','ev_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (ev,h,reserves_up) ,  RP_EV_G2V.l(n,reserves_up,ev,h) * phi_reserves_call(n,reserves_up,h)) * %sec_hour% ;
       report_tech('EV negative reserves activation by charging','ev_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (ev,h,reserves_do) , RP_EV_G2V.l(n,reserves_do,ev,h) * phi_reserves_call(n,reserves_do,h)) * %sec_hour% ;
       report_tech('EV positive reserves activation by discharging','ev_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (ev,h,reserves_up) , RP_EV_V2G.l(n,reserves_up,ev,h) * phi_reserves_call(n,reserves_up,h)) * %sec_hour% ;
       report_tech('EV negative reserves activation by discharging','ev_cum',n)$(feat_node('ev',n) AND feat_node('reserves',n)) = sum( (ev,h,reserves_do) , RP_EV_V2G.l(n,reserves_do,ev,h) * phi_reserves_call(n,reserves_do,h)) * %sec_hour% ;

                 report_reserves_tech_hours('Reserves provision',reserves,'ev_cum',h,n)$(report_reserves_tech_hours('Reserves provision',reserves,'ev_cum',h,n) < eps_rep_abs ) = 0 ;
                 report_reserves_tech_hours('Reserves activation',reserves,'ev_cum',h,n)$(report_reserves_tech_hours('Reserves activation',reserves,'ev_cum',h,n) < eps_rep_abs ) = 0 ;
                 report_reserves_tech('reserve provision shares',reserves_prim,'ev_cum',n)$(report_reserves_tech('reserve provision shares',reserves_prim,'ev_cum',n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve provision shares',reserves_prim,'G2V_cum',n)$(report_reserves_tech('reserve provision shares',reserves_prim,'G2V_cum',n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve provision shares',reserves_prim,'V2G_cum',n)$(report_reserves_tech('reserve provision shares',reserves_prim,'V2G_cum',n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve provision shares',reserves_nonprim,'ev_cum',n)$(report_reserves_tech('reserve provision shares',reserves_nonprim,'ev_cum',n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve provision shares',reserves_nonprim,'G2V_cum',n)$(report_reserves_tech('reserve provision shares',reserves_nonprim,'G2V_cum',n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve provision shares',reserves_nonprim,'V2G_cum',n)$(report_reserves_tech('reserve provision shares',reserves_nonprim,'V2G_cum',n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_prim,'ev_cum',n)$(report_reserves_tech('reserve activation shares',reserves_prim,'ev_cum',n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_prim,'G2V_cum',n)$(report_reserves_tech('reserve activation shares',reserves_prim,'G2V_cum',n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_prim,'V2G_cum',n)$(report_reserves_tech('reserve activation shares',reserves_prim,'V2G_cum',n) < eps_rep_rel) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_nonprim,'ev_cum',n)$(report_reserves_tech('reserve activation shares',reserves_nonprim,'ev_cum',n) < eps_rep_rel ) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_nonprim,'G2V_cum',n)$(report_reserves_tech('reserve activation shares',reserves_nonprim,'G2V_cum',n) < eps_rep_rel ) = 0 ;
                 report_reserves_tech('reserve activation shares',reserves_nonprim,'V2G_cum',n)$(report_reserves_tech('reserve activation shares',reserves_nonprim,'V2G_cum',n) < eps_rep_rel ) = 0 ;
                 report_tech('EV positive reserves activation by charging','ev_cum',n)$(report_tech('EV positive reserves activation by charging','ev_cum',n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_tech('EV negative reserves activation by charging','ev_cum',n)$(report_tech('EV negative reserves activation by charging','ev_cum',n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_tech('EV positive reserves activation by discharging','ev_cum',n)$(report_tech('EV positive reserves activation by discharging','ev_cum',n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
                 report_tech('EV negative reserves activation by discharging','ev_cum',n)$(report_tech('EV negative reserves activation by discharging','ev_cum',n) < eps_rep_abs*card(h)*%sec_hour%) = 0 ;
$ontext
$offtext


* ----------------------------------------------------------------------------

* PROSUMAGE
%prosumage%$ontext
        report_hours('demand prosumers',h,n)$feat_node('prosumage',n) = phi_pro_load(n) * d(n,h) ;
        report_hours('demand market',h,n)$feat_node('prosumage',n) = (1 - phi_pro_load(n)) * d(n,h) ;
        gross_energy_demand_market(n) = gross_energy_demand(n) - gross_energy_demand_prosumers_selfgen(n) ;

        report_prosumage_tech_hours('generation prosumers',res,h,n)$feat_node('prosumage',n) = N_RES_PRO.l(n,res) ;
        report_prosumage_tech_hours('curtailment of fluct res prosumers',res,h,n)$feat_node('prosumage',n) =  CU_PRO.l(n,res,h) ;
        report_prosumage_tech_hours('generation prosumers self-consumption',res,h,n)$feat_node('prosumage',n) = G_RES_PRO.l(n,res,h)  ;
        report_prosumage_tech_hours('generation prosumers to market',res,h,n)$feat_node('prosumage',n) = G_MARKET_PRO2M.l(n,res,h);
        report_prosumage_tech_hours('withdrawal prosumers from market','',h,n)$feat_node('prosumage',n) = G_MARKET_M2PRO.l(n,h) ;
        report_prosumage_tech_hours('storage loading prosumers PRO2PRO',sto,h,n)$feat_node('prosumage',n) =   sum( res , STO_IN_PRO2PRO.l(n,res,sto,h)) ;
        report_prosumage_tech_hours('storage loading prosumers PRO2M',sto,h,n)$feat_node('prosumage',n) =  sum( res , STO_IN_PRO2M.l(n,res,sto,h)) ;
        report_prosumage_tech_hours('storage loading prosumers M2PRO',sto,h,n)$feat_node('prosumage',n) =  STO_IN_M2PRO.l(n,sto,h) ;
        report_prosumage_tech_hours('storage loading prosumers M2M',sto,h,n)$feat_node('prosumage',n) =  STO_IN_M2M.l(n,sto,h) ;
        report_prosumage_tech_hours('storage generation prosumers PRO2PRO',sto,h,n)$feat_node('prosumage',n) =  STO_OUT_PRO2PRO.l(n,sto,h) ;
        report_prosumage_tech_hours('storage generation prosumers PRO2M',sto,h,n)$feat_node('prosumage',n) =  STO_OUT_PRO2M.l(n,sto,h) ;
        report_prosumage_tech_hours('storage generation prosumers M2PRO',sto,h,n)$feat_node('prosumage',n) =  STO_OUT_M2PRO.l(n,sto,h) ;
        report_prosumage_tech_hours('storage generation prosumers M2M',sto,h,n)$feat_node('prosumage',n) =  STO_OUT_M2M.l(n,sto,h) ;
        report_prosumage_tech_hours('storage level prosumers',sto,h,n)$feat_node('prosumage',n) = STO_L_PRO.l(n,sto,h) ;
        report_prosumage_tech_hours('storage level prosumers PRO2PRO',sto,h,n)$feat_node('prosumage',n) =  STO_L_PRO2PRO.l(n,sto,h) ;
        report_prosumage_tech_hours('storage level prosumers PRO2M',sto,h,n)$feat_node('prosumage',n) =  STO_L_PRO2M.l(n,sto,h) ;
        report_prosumage_tech_hours('storage level prosumers M2PRO',sto,h,n)$feat_node('prosumage',n) =  STO_L_M2PRO.l(n,sto,h) ;
        report_prosumage_tech_hours('storage level prosumers M2M',sto,h,n)$feat_node('prosumage',n) =   STO_L_M2M.l(n,sto,h) ;

        report_market_tech_hours('generation market',con,h,n)$feat_node('prosumage',n) = G_L.l(n,con,h) + corr_fac_dis(n,con,h) ;
        report_market_tech_hours('generation market',res,h,n)$feat_node('prosumage',n) = G_L.l(n,res,h) + corr_fac_dis(n,res,h) + G_RES.l(n,res,h) ;
        report_market_tech_hours('generation market',rsvr,h,n)$feat_node('prosumage',n) = RSVR_OUT.l(n,rsvr,h) + corr_fac_rsvr(n,rsvr,h) ;
        report_market_tech_hours('curtailment of fluct res market',res,h,n)$feat_node('prosumage',n) =  CU.l(n,res,h);
        report_market_tech_hours('generation storage market',sto,h,n)$feat_node('prosumage',n) =  STO_OUT.l(n,sto,h) ;
        report_market_tech_hours('storage loading market',sto,h,n)$feat_node('prosumage',n) =  STO_IN.l(n,sto,h) ;
        report_market_tech_hours('storage level market',sto,h,n)$feat_node('prosumage',n) =  STO_L.l(n,sto,h) ;
        report_market_tech_hours('market to prosumer storage M2PRO','Interaction with prosumers',h,n)$feat_node('prosumage',n) =   sum( sto , STO_IN_M2PRO.l(n,sto,h)) ;
        report_market_tech_hours('market to prosumer storage M2M','Interaction with prosumers',h,n)$feat_node('prosumage',n) =   sum( sto , STO_IN_M2M.l(n,sto,h) ) ;
        report_market_tech_hours('prosumer storage to market PRO2M','Interaction with prosumers',h,n)$feat_node('prosumage',n) =  sum( sto , STO_OUT_PRO2M.l(n,sto,h) ) ;
        report_market_tech_hours('prosumer storage to market M2M','Interaction with prosumers',h,n)$feat_node('prosumage',n) =   sum( sto , STO_OUT_M2M.l(n,sto,h) ) ;
        report_market_tech_hours('energy market to prosumer','Interaction with prosumers',h,n)$feat_node('prosumage',n) =  G_MARKET_M2PRO.l(n,h)    ;
        report_market_tech_hours('energy prosumer to market','Interaction with prosumers',h,n)$feat_node('prosumage',n) =  sum( res , G_MARKET_PRO2M.l(n,res,h) )  ;

        report_node('gross energy demand market',n)$feat_node('prosumage',n) = gross_energy_demand_market(n) ;
        report_node('gross energy demand prosumers self generation',n)$feat_node('prosumage',n) = gross_energy_demand_prosumers_selfgen(n) ;

        report_prosumage_tech('capacities renewable prosumers',res,n)$feat_node('prosumage',n) =  N_RES_PRO.l(n,res)  ;
        report_prosumage_tech('capacities storage MW prosumers',sto,n)$feat_node('prosumage',n) =  N_STO_P_PRO.l(n,sto) ;
        report_prosumage_tech('capacities storage MWh prosumers',sto,n)$feat_node('prosumage',n) = N_STO_E_PRO.l(n,sto) * %sec_hour% ;
        report_prosumage_tech('Storage in total prosumers PRO2PRO',sto,n)$feat_node('prosumage',n) = sum( h, report_prosumage_tech_hours('storage loading prosumers PRO2PRO',sto,h,n)) ;
        report_prosumage_tech('Storage in total prosumers PRO2PRO',sto,n)$feat_node('prosumage',n) = sum( h, report_prosumage_tech_hours('storage loading prosumers PRO2PRO',sto,h,n));
        report_prosumage_tech('Storage in total prosumers PRO2M',sto,n)$feat_node('prosumage',n) = sum( h, report_prosumage_tech_hours('storage loading prosumers PRO2M',sto,h,n));
        report_prosumage_tech('Storage in total prosumers M2PRO',sto,n)$feat_node('prosumage',n) = sum( h, report_prosumage_tech_hours('storage loading prosumers M2PRO',sto,h,n));
        report_prosumage_tech('Storage in total prosumers M2M',sto,n)$feat_node('prosumage',n) = sum( h, report_prosumage_tech_hours('storage loading prosumers M2M',sto,h,n));
        report_prosumage_tech('Storage out total prosumers PRO2PRO',sto,n)$feat_node('prosumage',n) = sum( h, report_prosumage_tech_hours('storage generation prosumers PRO2PRO',sto,h,n)) ;
        report_prosumage_tech('Storage out total prosumers PRO2M',sto,n)$feat_node('prosumage',n) = sum( h, report_prosumage_tech_hours('storage generation prosumers PRO2M',sto,h,n)) ;
        report_prosumage_tech('Storage out total prosumers M2PRO',sto,n)$feat_node('prosumage',n) = sum( h, report_prosumage_tech_hours('storage generation prosumers M2PRO',sto,h,n)) ;
        report_prosumage_tech('Storage out total prosumers M2M',sto,n)$feat_node('prosumage',n) =  sum( h, report_prosumage_tech_hours('storage generation prosumers M2M',sto,h,n)) ;
        report_prosumage_tech('Storage out total prosumers',sto,n)$feat_node('prosumage',n) = report_prosumage_tech('Storage out total prosumers PRO2PRO',sto,n) + report_prosumage_tech('Storage out total prosumers PRO2M',sto,n) + report_prosumage_tech('Storage out total prosumers M2PRO',sto,n) + report_prosumage_tech('Storage out total prosumers M2M',sto,n) ;
        report_prosumage_tech('Generation total prosumers PRO2M','',n)$feat_node('prosumage',n) =  sum( (h,res) , report_prosumage_tech_hours('generation prosumers to market',res,h,n)) ;
        report_prosumage_tech('Withdrawal total prosumers M2PRO','',n)$feat_node('prosumage',n) =  sum( h , report_prosumage_tech_hours('withdrawal prosumers from market','',h,n)) ;
        report_prosumage_tech('generation prosumers self-consumption','',n)$feat_node('prosumage',n) =  sum( (res,h) , report_prosumage_tech_hours('generation prosumers self-consumption',res,h,n)) ;
        report_prosumage_tech('consumption share prosumers',res,n)$(feat_node('prosumage',n) AND  sum( h , phi_pro_load(n) * d(n,h) ) ) = sum( h , G_RES_PRO.l(n,res,h))  / sum( h , phi_pro_load(n) * d(n,h) );
        report_prosumage_tech('consumption share prosumers',sto,n)$(feat_node('prosumage',n) AND sum( h , phi_pro_load(n) * d(n,h) ) ) =  sum( h , STO_OUT_PRO2PRO.l(n,sto,h) + STO_OUT_M2PRO.l(n,sto,h) ) /  sum( h , phi_pro_load(n) * d(n,h) );
        report_prosumage_tech('consumption share prosumers','market',n)$ sum( h , phi_pro_load(n) * d(n,h)  ) = sum( h , G_MARKET_M2PRO.l(n,h) ) / sum( h , phi_pro_load(n) * d(n,h)  );
        report_prosumage_tech('curtailment of fluct res absolute prosumers',res,n)$feat_node('prosumage',n) =  sum(h, CU_PRO.l(n,res,h) ) * %sec_hour% ;
        report_prosumage_tech('curtailment of fluct res relative prosumers',res,n)$(report_prosumage_tech('curtailment of fluct res absolute prosumers',res,n) AND N_RES_PRO.l(n,res) > eps_rep_abs ) =  sum(h, CU_PRO.l(n,res,h) )/ sum( h , phi_res(n,res,h) * N_RES_PRO.l(n,res))  ;
        report_prosumage_tech('average market value storage in PRO2PRO',sto,n)$(report_prosumage_tech('Storage in total prosumers PRO2PRO',sto,n) > eps_rep_abs*card(h)) = sum( h , report_hours('price',h,n) * sum( res , STO_IN_PRO2PRO.l(n,res,sto,h))) / report_prosumage_tech('Storage in total prosumers PRO2PRO',sto,n) ;
        report_prosumage_tech('average market value storage in PRO2M',sto,n)$(report_prosumage_tech('Storage in total prosumers PRO2M',sto,n) > eps_rep_abs*card(h)) = sum( h , report_hours('price',h,n) * sum( res , STO_IN_PRO2M.l(n,res,sto,h))) / report_prosumage_tech('Storage in total prosumers PRO2M',sto,n) ;
        report_prosumage_tech('average market value storage in M2PRO',sto,n)$(report_prosumage_tech('Storage in total prosumers M2PRO',sto,n) > eps_rep_abs*card(h)) = sum( h , report_hours('price',h,n) * STO_IN_M2PRO.l(n,sto,h)) / report_prosumage_tech('Storage in total prosumers M2PRO',sto,n) ;
        report_prosumage_tech('average market value storage in M2M',sto,n)$(report_prosumage_tech('Storage in total prosumers M2M',sto,n) > eps_rep_abs*card(h)) = sum( h , report_hours('price',h,n) * STO_IN_M2M.l(n,sto,h)) / report_prosumage_tech('Storage in total prosumers M2M',sto,n) ;
        report_prosumage_tech('average market value storage out PRO2PRO',sto,n)$(report_prosumage_tech('Storage out total prosumers PRO2PRO',sto,n) > eps_rep_abs*card(h)) = sum( h , report_hours('price',h,n) * STO_OUT_PRO2PRO.l(n,sto,h)) / report_prosumage_tech('Storage in total prosumers PRO2PRO',sto,n) ;
        report_prosumage_tech('average market value storage out PRO2M',sto,n)$(report_prosumage_tech('Storage out total prosumers PRO2M',sto,n) > eps_rep_abs*card(h)) = sum( h , report_hours('price',h,n) * STO_OUT_PRO2M.l(n,sto,h)) / report_prosumage_tech('Storage in total prosumers PRO2M',sto,n) ;
        report_prosumage_tech('average market value storage out M2PRO',sto,n)$(report_prosumage_tech('Storage out total prosumers M2PRO',sto,n) > eps_rep_abs*card(h)) = sum( h , report_hours('price',h,n) * STO_OUT_M2PRO.l(n,sto,h)) / report_prosumage_tech('Storage in total prosumers M2PRO',sto,n) ;
        report_prosumage_tech('average market value storage out M2M',sto,n)$(report_prosumage_tech('Storage out total prosumers M2M',sto,n) > eps_rep_abs*card(h)) = sum( h , report_hours('price',h,n) * STO_OUT_M2M.l(n,sto,h)) / report_prosumage_tech('Storage in total prosumers M2M',sto,n) ;
        report_prosumage_tech('average market value generation PRO2M','',n)$(report_prosumage_tech('Generation total prosumers PRO2M','',n) > eps_rep_abs*card(h)) = sum( h , report_hours('price',h,n) * sum( res , G_MARKET_PRO2M.l(n,res,h))) / report_prosumage_tech('Generation total prosumers PRO2M','',n) ;
        report_prosumage_tech('average market value withdrawal M2PRO','',n)$(report_prosumage_tech('Withdrawal total prosumers M2PRO','',n) > eps_rep_abs*card(h)) = sum( h , report_hours('price',h,n) * G_MARKET_M2PRO.l(n,h)) / report_prosumage_tech('Withdrawal total prosumers M2PRO','',n) ;
        report_prosumage_tech('average market value generation PRO2PRO','',n)$(report_prosumage_tech('generation prosumers self-consumption','',n) > eps_rep_abs*card(h)) = sum( h , report_hours('price',h,n) *  sum( res , G_RES_PRO.l(n,res,h))) / report_prosumage_tech('generation prosumers self-consumption','',n) ;
        report_prosumage_tech('FLH prosumers',res,n)$(feat_node('prosumage',n) AND N_RES_PRO.l(n,res) > eps_rep_ins) = sum( h , G_MARKET_PRO2M.l(n,res,h) + sum( sto , STO_IN_PRO2PRO.l(n,res,sto,h)) + G_RES_PRO.l(n,res,h) ) / N_RES_PRO.l(n,res)  ;
%reserves% report_prosumage_tech('FLH prosumers',sto,n)$(feat_node('prosumage',n) AND N_STO_P_PRO.l(n,sto) > eps_rep_ins) = report_prosumage_tech('Storage out total prosumers',sto,n) / N_STO_P_PRO.l(n,sto) ;
%reserves% report_prosumage_tech('Storage cycles prosumers',sto,n)$(feat_node('prosumage',n) AND N_STO_E_PRO.l(n,sto) > eps_rep_ins) = report_prosumage_tech('Storage out total prosumers',sto,n) /N_STO_E_PRO.l(n,sto) * %sec_hour% ;
        report_prosumage_tech('Storage EP-ratio prosumers',sto,n)$(feat_node('prosumage',n) AND N_STO_P_PRO.l(n,sto)  > eps_rep_ins AND N_STO_E_PRO.l(n,sto)  * %sec_hour% > eps_rep_ins ) = N_STO_E_PRO.l(n,sto)  * %sec_hour% / N_STO_P_PRO.l(n,sto)  ;

        report_market_tech('capacities renewable market',res,n)$feat_node('prosumage',n) =  N_TECH.l(n,res)  ;
        report_market_tech('capacities reservoir MW market',rsvr,n)$feat_node('prosumage',n) = N_RSVR_P.l(n,rsvr) ;
        report_market_tech('capacities reservoir MWh market',rsvr,n)$feat_node('prosumage',n) =  N_RSVR_E.l(n,rsvr) ;
        report_market_tech('capacities storage MW in market',sto,n)$feat_node('prosumage',n) =  N_STO_P_IN.l(n,sto) ;
        report_market_tech('capacities storage MW out market',sto,n)$feat_node('prosumage',n) =  N_STO_P_OUT.l(n,sto) ;
        report_market_tech('capacities storage MWh market',sto,n)$feat_node('prosumage',n) =  N_STO_E.l(n,sto) * %sec_hour% ;
        report_market_tech('curtailment of fluct res absolute market',res,n)$feat_node('prosumage',n) =  sum(h, CU.l(n,res,h) ) * %sec_hour% ;
        report_market_tech('curtailment of fluct res relative market',res,n)$(report_market_tech('curtailment of fluct res absolute market',res,n) AND sum(h, G_RES.l(n,res,h) - corr_fac_nondis(n,res,h) ) + sum(h, CU.l(n,res,h)) > card(h)*eps_rep_abs ) =  sum(h, CU.l(n,res,h) )/( sum(h,  G_RES.l(n,res,h) - corr_fac_nondis(n,res,h) + CU.l(n,res,h))) ;
        report_market_tech('capacities conventional market',con,n)$feat_node('prosumage',n) =  N_TECH.l(n,con) ;
        report_market_tech('Storage out total market non-reserves',sto,n)$feat_node('prosumage',n) = sum(h, report_market_tech_hours('generation storage market',sto,h,n) ) * %sec_hour% ;
        report_market_tech('Storage in total market non-reserves',sto,n)$feat_node('prosumage',n) = sum(h, report_market_tech_hours('storage loading market',sto,h,n) ) * %sec_hour% ;
        report_market_tech('FLH market non-reserves',res,n)$(feat_node('prosumage',n) AND N_TECH.l(n,res) > eps_rep_ins) = sum( h , G_RES.l(n,res,h) + G_L.l(n,res,h)- corr_fac_dis(n,res,h) ) / N_TECH.l(n,res) ;
        report_market_tech('FLH market non-reserves',con,n)$(feat_node('prosumage',n) AND N_TECH.l(n,con) > eps_rep_ins) = sum( h ,G_L.l(n,con,h))  / N_TECH.l(n,con) ;
%reserves% report_market_tech('FLH market non-reserves',sto,n)$(feat_node('prosumage',n) AND N_STO_P_OUT.l(n,sto) > eps_rep_ins) = report_market_tech('Storage out total market non-reserves',sto,n) / N_STO_P_OUT.l(n,sto) ;
%reserves% report_market_tech('Storage cycles market non-reserves',sto,n)$(feat_node('prosumage',n) AND N_STO_E.l(n,sto) > eps_rep_ins) = report_market_tech('Storage out total market non-reserves',sto,n) / N_STO_E.l(n,sto) * %sec_hour% ;
        report_market_tech('Storage EP-ratio market',sto,n)$(feat_node('prosumage',n) AND N_STO_P_OUT.l(n,sto)  > eps_rep_ins AND N_STO_E.l(n,sto)  * %sec_hour% > eps_rep_ins ) = N_STO_E.l(n,sto)  * %sec_hour% /N_STO_P_OUT.l(n,sto)  ;

        report_prosumage('gross energy demand prosumers',n)$feat_node('prosumage',n) =  gross_energy_demand_prosumers(n) ;
        report_prosumage('gross energy demand prosumers from market',n)$feat_node('prosumage',n) = gross_energy_demand_prosumers_market(n) ;
        report_prosumage('gross energy demand prosumers self generation',n)$feat_node('prosumage',n) = gross_energy_demand_prosumers_selfgen(n) ;
        report_prosumage('self-generation share prosumers total',n)$(feat_node('prosumage',n) AND  gross_energy_demand_prosumers(n) ) = gross_energy_demand_prosumers_selfgen(n) / gross_energy_demand_prosumers(n)  ;
        report_prosumage('market share prosumers',n)$(feat_node('prosumage',n) AND   gross_energy_demand_prosumers(n) )= gross_energy_demand_prosumers_market(n) /  gross_energy_demand_prosumers(n)  ;
        report_prosumage('curtailment of fluct res absolute prosumers',n)$feat_node('prosumage',n) = sum((res,h), CU_PRO.l(n,res,h)) * %sec_hour% ;
        report_prosumage('curtailment of fluct res relative prosumers',n)$(feat_node('prosumage',n) AND sum( (res,h) , phi_res(n,res,h) * N_RES_PRO.l(n,res)))  =  sum( (h,res) , CU_PRO.l(n,res,h) ) /sum( (res,h) , phi_res(n,res,h) * N_RES_PRO.l(n,res) ) ;
        report_prosumage('share self-generation curtailed',n)$(feat_node('prosumage',n) AND  sum( (res,h) , phi_res(n,res,h) *N_RES_PRO.l(n,res)) ) =  sum( (h,res) , CU_PRO.l(n,res,h) ) / sum( (res,h) , phi_res(n,res,h) * N_RES_PRO.l(n,res) ) ;
        report_prosumage('share self-generation direct consumption',n)$(feat_node('prosumage',n) AND sum( (res,h) , phi_res(n,res,h) * N_RES_PRO.l(n,res) )) = sum( (h,res) , G_RES_PRO.l(n,res,h) ) / sum( (res,h) , phi_res(n,res,h) * N_RES_PRO.l(n,res) ) ;
        report_prosumage('share self-generation to market',n)$(feat_node('prosumage',n) AND sum( (res,h) , phi_res(n,res,h) * N_RES_PRO.l(n,res)) ) = sum( (h,res) , G_MARKET_PRO2M.l(n,res,h) ) /  sum( (res,h) , phi_res(n,res,h) * N_RES_PRO.l(n,res) ) ;
        report_prosumage('share self-generation stored PRO2PRO',n)$(feat_node('prosumage',n) AND  sum( (res,h) , phi_res(n,res,h) * N_RES_PRO.l(n,res)) ) = sum( (h,sto,res) , STO_IN_PRO2PRO.l(n,res,sto,h) ) /  sum( (res,h) , phi_res(n,res,h) * N_RES_PRO.l(n,res) ) ;
        report_prosumage('share self-generation stored PRO2M',n)$(feat_node('prosumage',n) AND sum( (res,h) , phi_res(n,res,h) * N_RES_PRO.l(n,res)) ) =  sum( (h,sto,res) , STO_IN_PRO2M.l(n,res,sto,h) ) /  sum( (res,h) , phi_res(n,res,h) * N_RES_PRO.l(n,res) ) ;
        report_prosumage('Capacity total prosumers',n)$feat_node('prosumage',n) = sum( res , N_RES_PRO.l(n,res) ) + sum( sto , N_STO_P_PRO.l(n,sto) ) ;

        report_prosumage_tech('Capacity share prosumers',res,n)$report_prosumage('Capacity total prosumers',n) = N_RES_PRO.l(n,res)  / report_prosumage('Capacity total prosumers',n) + 1e-9 ;
        report_prosumage_tech('Capacity share prosumers',sto,n)$report_prosumage('Capacity total prosumers',n) = N_STO_P_PRO.l(n,sto)  / report_prosumage('Capacity total prosumers',n) + 1e-9 ;

        report_market('gross energy demand market',n)$feat_node('prosumage',n) = gross_energy_demand_market(n) ;
        report_market('curtailment of fluct res absolute market',n)$feat_node('prosumage',n) = sum((res,h), CU.l(n,res,h)) * %sec_hour% ;
        report_market('curtailment of fluct res relative market',n)$(feat_node('prosumage',n) AND sum( (h,res) , phi_res(n,res,h)* N_TECH.l(n,res))  > eps_rep_abs*card(res)*card(h) ) = sum( (res,h), CU.l(n,res,h))/  sum( (res,h) , phi_res(n,res,h) * N_TECH.l(n,res) ) ;
        report_market('Share market energy transferred to prosumer consumption',n)$feat_node('prosumage',n) = sum( h, G_MARKET_M2PRO.l(n,h) ) /  gross_energy_demand_market(n)  ;
        report_market('Share market energy transferred to prosumer storage M2PRO',n)$feat_node('prosumage',n) = sum( h,  sum( sto , STO_IN_M2PRO.l(n,sto,h)) ) / gross_energy_demand_market(n)  ;
        report_market('Share market energy transferred to prosumer storage M2M',n)$feat_node('prosumage',n) = sum( h,  sum( sto , STO_IN_M2M.l(n,sto,h)) ) / gross_energy_demand_market(n)  ;
        report_market('Share market energy tranferred from prosumer generation',n)$feat_node('prosumage',n) = sum( h,  sum( res , G_MARKET_PRO2M.l(n,res,h)) ) / gross_energy_demand_market(n)  ;
        report_market('Share market energy transferred from prosumer storage PRO2M',n)$feat_node('prosumage',n) = sum( h,  sum( sto , STO_OUT_PRO2M.l(n,sto,h)) ) /  gross_energy_demand_market(n)  ;
        report_market('Share market energy transferred from prosumer storage M2M',n)$feat_node('prosumage',n) = sum( h,  sum( sto , STO_OUT_M2M.l(n,sto,h)) ) / gross_energy_demand_market(n)  ;
        report_market('Capacity total market',n)$feat_node('prosumage',n) = sum( tech , N_TECH.l(n,tech)) + sum( sto , N_STO_P_OUT.l(n,sto) ) + sum( rsvr ,  N_RSVR_P.l(n,rsvr)) + sum( dsm_curt ,  N_DSM_CU.l(n,dsm_curt)) + sum( dsm_shift ,  N_DSM_SHIFT.l(n,dsm_shift)) ;
        report_market('Energy demand total market',n)$feat_node('prosumage',n) = (sum( h , (1 - phi_pro_load(n)) * d(n,h) ) + sum( h , G_MARKET_M2PRO.l(n,h)) + sum( (sto,h) , STO_IN.l(n,sto,h) + STO_IN_M2M.l(n,sto,h) + STO_IN_M2PRO.l(n,sto,h) ) ) * %sec_hour%
%prosumage%$ontext
%DSM%$ontext
        + sum( (dsm_shift,h) , DSM_UP_DEMAND.l(n,dsm_shift,h) ) * %sec_hour%
$ontext
$offtext
%prosumage%$ontext
%EV%$ontext
         + sum( (ev,h) , EV_CHARGE.l(n,ev,h) ) * %sec_hour%
$ontext
$offtext
%prosumage%$ontext
%reserves%$ontext
         + sum( h , reserves_activated(n,h))
$ontext
$offtext
;

%prosumage%$ontext
        report_market('energy generated gross market',n)$feat_node('prosumage',n) = sum( h ,  sum( dis ,G_L.l(n,dis,h)) + sum( nondis ,G_RES.l(n,nondis,h)) + sum( res , G_MARKET_PRO2M.l(n,res,h) - corr_fac_nondis(n,res,h)) + sum( rsvr , RSVR_OUT.l(n,rsvr,h)- corr_fac_rsvr(n,rsvr,h)) + sum( sto , STO_OUT_M2M.l(n,sto,h) + STO_OUT.l(n,sto,h) - corr_fac_sto(n,sto,h)) + sum( dsm_shift , DSM_DO_DEMAND.l(n,dsm_shift,h) - corr_fac_dsm_shift(n,dsm_shift,h)) + sum( dsm_curt , DSM_CU.l(n,dsm_curt,h)) ) ;
$ontext
$offtext
%prosumage%$ontext
%EV%$ontext
        report_market('energy generated gross market',n)$feat_node('prosumage',n) = report_market('energy generated gross market',n)$feat_node('prosumage',n) + sum( h , sum( ev , EV_DISCHARGE.l(n,ev,h) - corr_fac_ev(n,h) ) ) ;
$ontext
$offtext
%prosumage%$ontext
        report_market_tech('Capacity share market',tech,n)$(feat_node('prosumage',n) AND report_market('Capacity total market',n)) = N_TECH.l(n,tech)  / report_market('Capacity total market',n) + 1e-9 ;
        report_market_tech('Capacity share market',rsvr,n)$feat_node('prosumage',n) = N_RSVR_P.l(n,rsvr)  / report_market('Capacity total market',n) + 1e-9 ;
        report_market_tech('Capacity share market',sto,n)$feat_node('prosumage',n) = N_STO_P_OUT.l(n,sto)  / report_market('Capacity total market',n) + 1e-9 ;
        report_market_tech('Energy share in gross nodal market generation',con,n)$(feat_node('prosumage',n) AND report_market('energy generated gross market',n)) = sum( h , G_L.l(n,con,h) ) / report_market('energy generated gross market',n) * %sec_hour% + 1e-9 ;
        report_market_tech('Energy share in gross nodal market generation',res,n)$(feat_node('prosumage',n) AND report_market('energy generated gross market',n)) = sum( h , G_L.l(n,res,h) + G_MARKET_PRO2M.l(n,res,h) + G_RES.l(n,res,h) - corr_fac_nondis(n,res,h) ) / report_market('energy generated gross market',n) * %sec_hour% + 1e-9 ;
        report_market_tech('Energy share in gross nodal market generation',rsvr,n)$(feat_node('prosumage',n) AND report_market('energy generated gross market',n)) = sum( h , RSVR_OUT.l(n,rsvr,h) - corr_fac_rsvr(n,rsvr,h) ) / report_market('energy generated gross market',n) * %sec_hour% + 1e-9 ;
        report_market_tech('Energy share in gross nodal market generation',sto,n)$(feat_node('prosumage',n) AND report_market('energy generated gross market',n)) = sum( h , STO_OUT_M2M.l(n,sto,h) + STO_OUT.l(n,sto,h) - corr_fac_sto(n,sto,h) ) / report_market('energy generated gross market',n) * %sec_hour% + 1e-9 ;

                 report_hours('demand prosumers',h,n)$(report_hours('demand prosumers',h,n) < eps_rep_abs) = 0 ;
                 report_hours('demand market',h,n)$(report_hours('demand market',h,n) < eps_rep_abs) = 0 ;

                 report_prosumage_tech_hours('generation prosumers',res,h,n)$(report_prosumage_tech_hours('generation prosumers',res,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('curtailment of fluct res prosumers',res,h,n)$(report_prosumage_tech_hours('curtailment of fluct res prosumers',res,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('generation prosumers self-consumption',res,h,n)$(report_prosumage_tech_hours('generation prosumers self-consumption',res,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('generation prosumers to market',res,h,n)$(report_prosumage_tech_hours('generation prosumers to market',res,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('withdrawal prosumers from market','',h,n)$(report_prosumage_tech_hours('withdrawal prosumers from market','',h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('storage loading prosumers PRO2PRO',sto,h,n)$(report_prosumage_tech_hours('storage loading prosumers PRO2PRO',sto,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('storage loading prosumers PRO2M',sto,h,n)$(report_prosumage_tech_hours('storage loading prosumers PRO2M',sto,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('storage loading prosumers M2PRO',sto,h,n)$(report_prosumage_tech_hours('storage loading prosumers M2PRO',sto,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('storage loading prosumers M2M',sto,h,n)$(report_prosumage_tech_hours('storage loading prosumers M2M',sto,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('storage generation prosumers PRO2PRO',sto,h,n)$(report_prosumage_tech_hours('storage generation prosumers PRO2PRO',sto,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('storage generation prosumers PRO2M',sto,h,n)$(report_prosumage_tech_hours('storage generation prosumers PRO2M',sto,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('storage generation prosumers M2PRO',sto,h,n)$(report_prosumage_tech_hours('storage generation prosumers M2PRO',sto,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('storage generation prosumers M2M',sto,h,n)$(report_prosumage_tech_hours('storage generation prosumers M2M',sto,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('storage level prosumers',sto,h,n)$(report_prosumage_tech_hours('storage level prosumers',sto,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('storage level prosumers PRO2PRO',sto,h,n)$(report_prosumage_tech_hours('storage level prosumers PRO2PRO',sto,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('storage level prosumers PRO2M',sto,h,n)$(report_prosumage_tech_hours('storage level prosumers PRO2M',sto,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('storage level prosumers M2PRO',sto,h,n)$(report_prosumage_tech_hours('storage level prosumers M2PRO',sto,h,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech_hours('storage level prosumers M2M',sto,h,n)$(report_prosumage_tech_hours('storage level prosumers M2M',sto,h,n) < eps_rep_abs) = 0 ;

                 report_market_tech_hours('generation market',con,h,n)$(report_market_tech_hours('generation market',con,h,n) < eps_rep_abs) = 0 ;
                 report_market_tech_hours('generation market',res,h,n)$(report_market_tech_hours('generation market',res,h,n) < eps_rep_abs) = 0 ;
                 report_market_tech_hours('generation market',rsvr,h,n)$(report_market_tech_hours('generation market',rsvr,h,n) < eps_rep_abs) = 0 ;
                 report_market_tech_hours('curtailment of fluct res market',res,h,n)$(report_market_tech_hours('curtailment of fluct res market',res,h,n) < eps_rep_abs) =  0 ;
                 report_market_tech_hours('generation storage market',sto,h,n)$(report_market_tech_hours('generation storage market',sto,h,n) < eps_rep_abs) =  0 ;
                 report_market_tech_hours('storage loading market',sto,h,n)$(report_market_tech_hours('storage loading market',sto,h,n) < eps_rep_abs) = 0 ;
                 report_market_tech_hours('storage level market',sto,h,n)$(report_market_tech_hours('storage level market',sto,h,n) < eps_rep_abs) = 0 ;
                 report_market_tech_hours('market to prosumer storage M2PRO','Interaction with prosumers',h,n)$(report_market_tech_hours('market to prosumer storage M2PRO','Interaction with prosumers',h,n) < eps_rep_abs) = 0 ;
                 report_market_tech_hours('market to prosumer storage M2M','Interaction with prosumers',h,n)$(report_market_tech_hours('market to prosumer storage M2M','Interaction with prosumers',h,n) < eps_rep_abs) = 0 ;
                 report_market_tech_hours('prosumer storage to market PRO2M','Interaction with prosumers',h,n)$(report_market_tech_hours('prosumer storage to market PRO2M','Interaction with prosumers',h,n) < eps_rep_abs) = 0 ;
                 report_market_tech_hours('prosumer storage to market M2M','Interaction with prosumers',h,n)$(report_market_tech_hours('prosumer storage to market M2M','Interaction with prosumers',h,n) < eps_rep_abs) = 0 ;
                 report_market_tech_hours('energy market to prosumer','Interaction with prosumers',h,n)$(report_market_tech_hours('energy market to prosumer','Interaction with prosumers',h,n) < eps_rep_abs) = 0 ;
                 report_market_tech_hours('energy prosumer to market','Interaction with prosumers',h,n)$(report_market_tech_hours('energy prosumer to market','Interaction with prosumers',h,n) < eps_rep_abs) = 0 ;

                 report_prosumage_tech('capacities renewable prosumers',res,n)$(report_prosumage_tech('capacities renewable prosumers',res,n) < eps_rep_ins) =  0 ;
                 report_prosumage_tech('capacities storage MW prosumers',sto,n)$(report_prosumage_tech('capacities storage MW prosumers',sto,n) < eps_rep_ins) =  0 ;
                 report_prosumage_tech('capacities storage MWh prosumers',sto,n)$(report_prosumage_tech('capacities storage MWh prosumers',sto,n) < eps_rep_ins) =  0 ;
                 report_prosumage_tech('consumption share prosumers',sto,n)$(report_prosumage_tech('consumption share prosumers',sto,n) < eps_rep_rel) = 0 ;
                 report_prosumage_tech('consumption share prosumers',res,n)$(report_prosumage_tech('consumption share prosumers',res,n) < eps_rep_rel) = 0 ;
                 report_prosumage_tech('consumption share prosumers','market',n)$(report_prosumage_tech('consumption share prosumers','market',n) < eps_rep_rel) = 0 ;
                 report_prosumage_tech('curtailment of fluct res absolute prosumers',res,n)$(report_prosumage_tech('curtailment of fluct res absolute prosumers',res,n) < eps_rep_ins) = 0 ;
                 report_prosumage_tech('curtailment of fluct res relative prosumers',res,n)$(report_prosumage_tech('curtailment of fluct res relative prosumers',res,n) < eps_rep_rel) = 0 ;
                 report_prosumage_tech('Storage in total prosumers PRO2PRO',sto,n)$(report_prosumage_tech('Storage in total prosumers PRO2PRO',sto,n)< eps_rep_abs*card(h)) = 0 ;
                 report_prosumage_tech('Storage in total prosumers PRO2M',sto,n)$(report_prosumage_tech('Storage in total prosumers PRO2M',sto,n)< eps_rep_abs*card(h)) = 0 ;
                 report_prosumage_tech('Storage in total prosumers M2PRO',sto,n)$(report_prosumage_tech('Storage in total prosumers M2PRO',sto,n)< eps_rep_abs*card(h)) = 0 ;
                 report_prosumage_tech('Storage in total prosumers M2M',sto,n)$(report_prosumage_tech('Storage in total prosumers M2M',sto,n)< eps_rep_abs*card(h)) = 0 ;
                 report_prosumage_tech('Storage out total prosumers PRO2PRO',sto,n)$(report_prosumage_tech('Storage out total prosumers PRO2PRO',sto,n)< eps_rep_abs*card(h)) = 0 ;
                 report_prosumage_tech('Storage out total prosumers PRO2M',sto,n)$(report_prosumage_tech('Storage out total prosumers PRO2M',sto,n)< eps_rep_abs*card(h)) = 0 ;
                 report_prosumage_tech('Storage out total prosumers M2PRO',sto,n)$(report_prosumage_tech('Storage out total prosumers M2PRO',sto,n)< eps_rep_abs*card(h)) = 0 ;
                 report_prosumage_tech('Storage out total prosumers M2M',sto,n)$(report_prosumage_tech('Storage out total prosumers M2M',sto,n)< eps_rep_abs*card(h)) = 0 ;
                 report_prosumage_tech('Storage out total prosumers',sto,n)$(report_prosumage_tech('Storage out total prosumers',sto,n)< eps_rep_abs*card(h)) = 0 ;
                 report_prosumage_tech('Generation total prosumers PRO2M','',n)$(report_prosumage_tech('Generation total prosumers PRO2M','',n) < eps_rep_abs*card(h)) = 0 ;
                 report_prosumage_tech('Withdrawal total prosumers M2PRO','',n)$(report_prosumage_tech('Withdrawal total prosumers M2PRO','',n) < eps_rep_abs*card(h)) = 0 ;
                 report_prosumage_tech('generation prosumers self-consumption','',n)$(report_prosumage_tech('generation prosumers self-consumption','',n) < eps_rep_abs * card(h)) = 0 ;
                 report_prosumage_tech('average market value storage in PRO2PRO',sto,n)$(report_prosumage_tech('average market value storage in PRO2PRO',sto,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech('average market value storage in PRO2M',sto,n)$(report_prosumage_tech('average market value storage in PRO2M',sto,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech('average market value storage in M2PRO',sto,n)$(report_prosumage_tech('average market value storage in M2PRO',sto,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech('average market value storage in M2M',sto,n)$(report_prosumage_tech('average market value storage in M2M',sto,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech('average market value storage out PRO2PRO',sto,n)$(report_prosumage_tech('average market value storage out PRO2PRO',sto,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech('average market value storage out PRO2M',sto,n)$(report_prosumage_tech('average market value storage out PRO2M',sto,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech('average market value storage out M2PRO',sto,n)$(report_prosumage_tech('average market value storage out M2PRO',sto,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech('average market value storage out M2M',sto,n)$(report_prosumage_tech('average market value storage out M2M',sto,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech('average market value generation PRO2M','',n)$(report_prosumage_tech('average market value generation PRO2M','',n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech('average market value withdrawal M2PRO','',n)$(report_prosumage_tech('average market value withdrawal M2PRO','',n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech('average market value generation PRO2PRO','',n)$(report_prosumage_tech('average market value generation PRO2PRO','',n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech('Capacity share prosumers',res,n)$(report_prosumage_tech('Capacity share prosumers',res,n) < eps_rep_rel) = 0 ;
                 report_prosumage_tech('Capacity share prosumers',sto,n)$(report_prosumage_tech('Capacity share prosumers',sto,n) < eps_rep_rel) = 0 ;
                 report_prosumage_tech('FLH prosumers',res,n)$(report_prosumage_tech('FLH prosumers',res,n) < eps_rep_abs) = 0 ;
%reserves%       report_prosumage_tech('Storage cycles prosumers',sto,n)$(report_prosumage_tech('Storage cycles prosumers',sto,n) < eps_rep_abs) = 0 ;
%reserves%       report_prosumage_tech('FLH',sto,n)$(report_prosumage_tech('FLH',sto,n) < eps_rep_abs) = 0 ;
                 report_prosumage_tech('Storage EP-ratio prosumers',sto,n)$(report_prosumage_tech('Storage EP-ratio prosumers',sto,n) < eps_rep_rel) = 0 ;

                 report_market_tech('capacities renewable market',res,n)$(report_market_tech('capacities renewable market',res,n) < eps_rep_abs) =  0 ;
                 report_market_tech('capacities reservoir MW market',rsvr,n)$(report_market_tech('capacities reservoir MW market',rsvr,n) < eps_rep_ins) = 0 ;
                 report_market_tech('capacities reservoir MWh market',rsvr,n)$(report_market_tech('capacities reservoir MWh market',rsvr,n) < eps_rep_ins) = 0 ;
                 report_market_tech('capacities storage MW market',sto,n)$(report_market_tech('capacities storage MW market',sto,n) < eps_rep_abs) =  0 ;
                 report_market_tech('capacities storage MWh market',sto,n)$(report_market_tech('capacities storage MWh market',sto,n) < eps_rep_abs) =  0 ;
                 report_market_tech('curtailment of fluct res absolute market',res,n)$(report_market_tech('curtailment of fluct res absolute market',res,n) < eps_rep_abs*card(h)) = 0 ;
                 report_market_tech('curtailment of fluct res relative market',res,n)$(report_market_tech('curtailment of fluct res relative market',res,n) < eps_rep_rel) = 0 ;
                 report_market_tech('capacities conventional market',con,n)$(report_market_tech('capacities conventional market',con,n) < eps_rep_ins) = 0 ;
                 report_market_tech('capacities renewable market',res,n)$(report_market_tech('capacities renewable market',res,n) < eps_rep_ins) = 0 ;
                 report_market_tech('capacities storage MW market',sto,n)$(report_market_tech('capacities storage MW market',sto,n) < eps_rep_ins) =  0 ;
                 report_market_tech('capacities storage MWh market',sto,n)$(report_market_tech('capacities storage MWh market',sto,n) < eps_rep_ins) = 0 ;
                 report_market_tech('Storage out total market non-reserves',sto,n)$(report_market_tech('Storage out total market non-reserves',sto,n) < eps_rep_abs*card(h)) = 0 ;
                 report_market_tech('Storage in total market non-reserves',sto,n)$(report_market_tech('Storage in total market non-reserves',sto,n) < eps_rep_abs*card(h)) = 0 ;
                 report_market_tech('FLH market non-reserves',res,n)$(report_market_tech('FLH market non-reserves',res,n) < eps_rep_abs) = 0 ;
                 report_market_tech('FLH market non-reserves',con,n)$(report_market_tech('FLH market non-reserves',con,n) < eps_rep_abs) = 0 ;
%reserves%       report_market_tech('FLH market non-reserves',sto,n)$(report_market_tech('FLH market non-reserves',sto,n) < eps_rep_abs) = 0 ;
%reserves%       report_market_tech('Storage cycles market non-reserves',sto,n)$(report_market_tech('Storage cycles market non-reserves',sto,n) < eps_rep_abs) = 0 ;
                 report_market_tech('Storage EP-ratio market',sto,n)$(report_market_tech('Storage EP-ratio market',sto,n) < eps_rep_rel) = 0 ;

                 report_prosumage('gross energy demand prosumers',n)$(report_prosumage('gross energy demand prosumers',n) < eps_rep_ins) = 0 ;
                 report_prosumage('gross energy demand prosumers from market',n)$(report_prosumage('gross energy demand prosumers from market',n) < eps_rep_ins) = 0 ;
                 report_prosumage('gross energy demand prosumers self generation',n)$(report_prosumage('gross energy demand prosumers self generation',n) < eps_rep_ins) = 0 ;
                 report_prosumage('self-generation share prosumage total',n)$(report_prosumage('self-generation share prosumage total',n) < eps_rep_rel) = 0 ;
                 report_prosumage('market share prosumage',n)$(report_prosumage('market share prosumage',n) < eps_rep_rel) = 0 ;
                 report_prosumage('curtailment of fluct res absolute prosumers',n)$(report_prosumage('curtailment of fluct res absolute prosumers',n) < eps_rep_ins) = 0 ;
                 report_prosumage('curtailment of fluct res relative prosumers',n)$(report_prosumage('curtailment of fluct res relative prosumers',n) < eps_rep_rel) = 0 ;
                 report_prosumage('share self-generation curtailed',n)$(report_prosumage('share self-generation curtailed',n) < eps_rep_rel) = 0 ;
                 report_prosumage('share self-generation direct consumption',n)$(report_prosumage('share self-generation direct consumption',n) < eps_rep_rel) = 0 ;
                 report_prosumage('share self-generation to market',n)$(report_prosumage('share self-generation to market',n) < eps_rep_rel) = 0 ;
                 report_prosumage('share self-generation stored PRO2PRO',n)$(report_prosumage('share self-generation stored PRO2PRO',n) < eps_rep_rel) = 0 ;
                 report_prosumage('share self-generation stored PRO2M',n)$(report_prosumage('share self-generation stored PRO2M',n) < eps_rep_rel) = 0 ;
                 report_prosumage('Capacity total prosumers',n)$(report_prosumage('Capacity total prosumers',n) < eps_rep_abs) = 0 ;

                 report_market('gross energy demand market',n)$(report_market('gross energy demand market',n) < eps_rep_abs) = 0 ;
                 report_market('curtailment of fluct res absolute market',n)$(report_market('curtailment of fluct res absolute market',n) < eps_rep_abs) = 0 ;
                 report_market('curtailment of fluct res relative market',n)$(report_market('curtailment of fluct res relative market',n) < eps_rep_rel) = 0 ;
                 report_market('Share market energy transferred to prosumer consumption',n)$(report_market('Share market energy transferred to prosumer consumption',n) < eps_rep_rel) = 0 ;
                 report_market('Share market energy transferred to prosumer storage M2PRO',n)$(report_market('Share market energy transferred to prosumer storage M2PRO',n) < eps_rep_rel) = 0 ;
                 report_market('Share market energy transferred to prosumer storage M2M',n)$(report_market('Share market energy transferred to prosumer storage M2M',n) < eps_rep_rel) = 0 ;
                 report_market('Share market energy tranferred from prosumer generation',n)$(report_market('Share market energy tranferred from prosumer generation',n) < eps_rep_rel) = 0 ;
                 report_market('Share market energy transferred from prosumer storage PRO2M',n)$(report_market('Share market energy transferred from prosumer storage PRO2M',n) < eps_rep_rel) = 0 ;
                 report_market('Share market energy transferred from prosumer storage M2M',n)$(report_market('Share market energy transferred from prosumer storage M2M',n) < eps_rep_rel) = 0 ;
                 report_market('Capacity total market',n)$(report_market('Capacity total market',n) < eps_rep_abs) = 0 ;
                 report_market('Energy demand total market',n)$(report_market('Energy demand total market',n) < eps_rep_abs) = 0 ;
                 report_market('energy generated gross market',n)$(report_market('energy generated gross market',n) < eps_rep_abs) = 0 ;

                 report_market_tech('Capacity share market',tech,n)$(report_market_tech('Capacity share market',tech,n) < eps_rep_rel) = 0 ;
                 report_market_tech('Capacity share market',sto,n)$(report_market_tech('Capacity share market',sto,n) < eps_rep_rel) = 0 ;
                 report_market_tech('Capacity share market',rsvr,n)$(report_market_tech('Capacity share market',rsvr,n) < eps_rep_rel) = 0 ;
                 report_market_tech('Energy share in gross nodal market generation',con,n)$(report_market_tech('Energy share in gross nodal market generation',con,n) < eps_rep_rel) = 0 ;
                 report_market_tech('Energy share in gross nodal market generation',res,n)$(report_market_tech('Energy share in gross nodal market generation',res,n) < eps_rep_rel) = 0 ;
                 report_market_tech('Energy share in gross nodal market generation',sto,n)$(report_market_tech('Energy share in gross nodal market generation',sto,n) < eps_rep_rel) = 0 ;
                 report_market_tech('Energy share in gross nodal market generation',rsvr,n)$(report_market_tech('Energy share in gross nodal market generation',rsvr,n) < eps_rep_rel) = 0 ;
$ontext
$offtext


* ----------------------------------------------------------------------------

* PROSUMAGE & DSM
%prosumage%$ontext
%DSM%$ontext
        report_market_tech('Capacity share market',dsm_curt,n)$(feat_node('prosumage',n) AND feat_node('dsm',n) AND report_market('Capacity total market',n)) = N_DSM_CU.l(n,dsm_curt) / report_market('Capacity total market',n) + 1e-9 ;
        report_market_tech('Capacity share market',dsm_shift,n)$(feat_node('prosumage',n) AND feat_node('dsm',n) AND report_market('Capacity total market',n)) = N_DSM_SHIFT.l(n,dsm_shift)  / report_market('Capacity total market',n) + 1e-9 ;
        report_market_tech('capacities load curtailment',dsm_curt,n)$(feat_node('prosumage',n) AND feat_node('dsm',n)) =  N_DSM_CU.l(n,dsm_curt) ;
        report_market_tech('capacities load shift',dsm_shift,n)$(feat_node('prosumage',n) AND feat_node('dsm',n)) =  N_DSM_SHIFT.l(n,dsm_shift) ;
        report_market_tech('FLH market',dsm_curt,n)$(feat_node('prosumage',n) AND feat_node('dsm',n) AND N_DSM_CU.l(n,dsm_curt) > eps_rep_ins) = sum( h , DSM_CU.l(n,dsm_curt,h) - corr_fac_dsm_cu(n,dsm_curt,h) ) / N_DSM_CU.l(n,dsm_curt) ;
        report_market_tech('FLH market',dsm_shift,n)$(feat_node('prosumage',n) AND feat_node('dsm',n) AND N_DSM_SHIFT.l(n,dsm_shift) > eps_rep_ins) = sum( h , DSM_DO_DEMAND.l(n,dsm_shift,h) ) / N_DSM_SHIFT.l(n,dsm_shift) ;
        report_market_tech('Energy share in gross nodal market generation',dsm_curt,n)$(feat_node('prosumage',n) AND feat_node('dsm',n) AND report_market('energy generated gross market',n)) = sum( h , DSM_CU.l(n,dsm_curt,h) - corr_fac_dsm_cu(n,dsm_curt,h) ) / report_market('energy generated gross market',n) * %sec_hour% + 1e-9 ;
        report_market_tech('Energy share in gross nodal market generation',dsm_shift,n)$(feat_node('prosumage',n) AND feat_node('dsm',n) AND report_market('energy generated gross market',n)) = sum( h , DSM_DO_DEMAND.l(n,dsm_shift,h) - corr_fac_dsm_shift(n,dsm_shift,h) ) / report_market('energy generated gross market',n) * %sec_hour% + 1e-9 ;

                 report_market_tech('Capacity share market',dsm_curt,n)$(report_market_tech('Capacity share market',dsm_curt,n) < eps_rep_rel) = 0 ;
                 report_market_tech('Capacity share market',dsm_shift,n)$(report_market_tech('Capacity share market',dsm_shift,n) < eps_rep_rel) = 0 ;
                 report_market_tech('capacities load curtailment',dsm_curt,n)$(report_market_tech('capacities load curtailment',dsm_curt,n) < eps_rep_ins) = 0 ;
                 report_market_tech('capacities load shift',dsm_shift,n)$(report_market_tech('capacities load shift',dsm_shift,n) < eps_rep_ins) = 0 ;
                 report_market_tech('FLH market',dsm_curt,n)$(report_market_tech('FLH market',dsm_curt,n) < eps_rep_abs) = 0 ;
                 report_market_tech('FLH market',dsm_shift,n)$(report_market_tech('FLH market',dsm_shift,n) < eps_rep_abs) = 0 ;
                 report_market_tech('Energy share in gross nodal market generation',dsm_curt,n)$(report_market_tech('Energy share in gross nodal market generation',dsm_curt,n) < eps_rep_rel) = 0 ;
                 report_market_tech('Energy share in gross nodal market generation',dsm_shift,n)$(report_market_tech('Energy share in gross nodal market generation',dsm_shift,n) < eps_rep_rel) = 0 ;
$ontext
$offtext


* ----------------------------------------------------------------------------

* PROSUMAGE & EV
%prosumage%$ontext
%EV%$ontext
        report_market_tech('Energy share in gross nodal market generation','ev_cum',n)$(feat_node('prosumage',n) AND feat_node('ev',n) AND report_market('energy generated gross market',n)) = (sum( (h,ev) , EV_DISCHARGE.l(n,ev,h)) -  sum( h , corr_fac_ev(n,h)))/ report_market('energy generated gross market',n) * %sec_hour% + 1e-9 ;
$ontext
$offtext


* ----------------------------------------------------------------------------

* HEAT
%heat%$ontext
         report_heat_tech_hours('heat demand - heating',n,bu,ch,h)$feat_node('heat',n) = dh(n,bu,ch,h) ;
         report_heat_tech_hours('infeasibility',n,bu,ch,h) =  H_INFES.l(n,bu,ch,h) ;
         report_heat_tech_hours('heat supply direct electric heating',n,bu,ch,h)$feat_node('heat',n) = theta_dir(n,bu,ch) * H_DIR.l(n,bu,ch,h) ;
         report_heat_tech_hours('heat supply SETS',n,bu,ch,h)$feat_node('heat',n) = theta_sets(n,bu,ch) * (H_SETS_OUT.l(n,bu,ch,h) + (1-eta_heat_stat(n,bu,ch)) * H_SETS_LEV.l(n,bu,ch,h-1)) ;
         report_heat_tech_hours('heat supply storage heating',n,bu,ch,h)$feat_node('heat',n) = theta_storage(n,bu,ch) * H_STO_OUT.l(n,bu,ch,h) ;
         report_heat_tech_hours('electricity demand direct electric',n,bu,ch,h)$feat_node('heat',n) = theta_dir(n,bu,ch) * H_DIR.l(n,bu,ch,h) ;
         report_heat_tech_hours('electricity demand SETS',n,bu,ch,h)$feat_node('heat',n) = theta_sets(n,bu,ch) * (H_SETS_IN.l(n,bu,ch,h) + corr_fac_sets(n,bu,ch,h)) ;
         report_heat_tech_hours('electricity demand HP',n,bu,hp,h)$feat_node('heat',n) = theta_hp(n,bu,hp) * ( H_HP_IN.l(n,bu,hp,h) + corr_fac_hp(n,bu,hp,h)) ;
         report_heat_tech_hours('electricity demand hybrid heating',n,bu,hel,h)$feat_node('heat',n) = theta_elec(n,bu,hel) * (H_ELECTRIC_IN.l(n,bu,hel,h) + corr_fac_h_elec(n,bu,hel,h)) ;
         report_heat_tech_hours('fossil fuel demand hybrid heating',n,bu,hfo,h)$feat_node('heat',n) = theta_fossil(n,bu,hfo) * H_STO_IN_FOSSIL.l(n,bu,hfo,h) ;
         report_heat_tech_hours('SETS inflow',n,bu,ch,h)$feat_node('heat',n) = theta_sets(n,bu,ch) * H_SETS_IN.l(n,bu,ch,h) ;
         report_heat_tech_hours('SETS level',n,bu,ch,h)$feat_node('heat',n) = theta_sets(n,bu,ch) * H_SETS_LEV.l(n,bu,ch,h) ;
         report_heat_tech_hours('SETS outflow',n,bu,ch,h)$feat_node('heat',n) = theta_sets(n,bu,ch) * (H_SETS_OUT.l(n,bu,ch,h) + (1-eta_heat_stat(n,bu,ch)) * H_SETS_LEV.l(n,bu,ch,h-1) ) ;
         report_heat_tech_hours('Storage inflow',n,bu,hst,h)$feat_node('heat',n) = theta_hp(n,bu,hst) * H_STO_IN_HP.l(n,bu,hst,h) + theta_elec(n,bu,hst) * H_STO_IN_ELECTRIC.l(n,bu,hst,h) + theta_elec(n,bu,hst) * H_STO_IN_FOSSIL.l(n,bu,hst,h) ;
         report_heat_tech_hours('Storage level',n,bu,ch,h)$feat_node('heat',n) = theta_storage(n,bu,ch) * H_STO_LEV.l(n,bu,ch,h) ;
         report_heat_tech_hours('Storage outflow',n,bu,ch,h)$feat_node('heat',n) = theta_storage(n,bu,ch) * H_STO_OUT.l(n,bu,ch,h) ;

         report_heat_tech_hours('heat demand - DHW',n,bu,ch,h)$feat_node('heat',n) = d_dhw(n,bu,ch,h) ;
         report_heat_tech_hours('DHW supply - direct electric',n,bu,ch,h)$feat_node('heat',n) = theta_dir(n,bu,ch) * H_DHW_DIR.l(n,bu,ch,h) ;
         report_heat_tech_hours('DHW supply - SETS aux electric',n,bu,ch,h)$feat_node('heat',n) = theta_sets(n,bu,ch) * H_DHW_AUX_OUT.l(n,bu,ch,h) ;
         report_heat_tech_hours('DHW supply - storage heating',n,bu,ch,h)$feat_node('heat',n) = theta_storage(n,bu,ch) * H_DHW_STO_OUT.l(n,bu,ch,h) ;
         report_heat_tech_hours('electricity demand DHW - direct electric',n,bu,ch,h)$feat_node('heat',n) = theta_dir(n,bu,ch) * H_DHW_DIR.l(n,bu,ch,h) ;
         report_heat_tech_hours('electricity demand DHW - SETS aux electric',n,bu,ch,h)$feat_node('heat',n) = theta_sets(n,bu,ch) * (H_DHW_AUX_ELEC_IN.l(n,bu,ch,h) + corr_fac_sets_aux(n,bu,ch,h)) ;
         report_heat_tech_hours('DHW - SETS aux hot water storage level',n,bu,ch,h)$feat_node('heat',n) = theta_sets(n,bu,ch) * H_DHW_AUX_LEV.l(n,bu,ch,h) ;

         report_heat_tech_hours('price heat electricity consumption',n,bu,ch,h)$(feat_node('heat',n) AND theta_dir(n,bu,ch) * H_DIR.l(n,bu,ch,h) + theta_sets(n,bu,ch) * H_SETS_IN.l(n,bu,ch,h) + theta_storage(n,bu,ch) * H_HP_IN.l(n,bu,ch,h) + theta_storage(n,bu,ch) * H_ELECTRIC_IN.l(n,bu,ch,h) > 0.25 ) = - con1a_bal.m(n,h) ;
         report_heat_tech_hours('price DHW electricity consumption',n,bu,ch,h)$(feat_node('heat',n) AND theta_dir(n,bu,ch) * H_DHW_DIR.l(n,bu,ch,h) + theta_sets(n,bu,ch) * H_DHW_AUX_ELEC_IN.l(n,bu,ch,h) > 0.25 ) = - con1a_bal.m(n,h) ;

                 report_heat_tech_hours('heat demand - heating',n,bu,ch,h)$(report_heat_tech_hours('heat demand - heating',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('infeasibility',n,bu,ch,h)$(report_heat_tech_hours('infeasibility',n,bu,ch,h) < eps_rep_abs ) = 0 ;
                 report_heat_tech_hours('heat supply direct electric heating',n,bu,ch,h)$(report_heat_tech_hours('heat supply direct electric heating',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('heat supply SETS',n,bu,ch,h)$(report_heat_tech_hours('heat supply SETS',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('heat supply storage heating',n,bu,ch,h)$(report_heat_tech_hours('heat supply storage heating',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('electricity demand direct electric',n,bu,ch,h)$(report_heat_tech_hours('electricity demand direct electric',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('electricity demand SETS',n,bu,ch,h)$(report_heat_tech_hours('electricity demand SETS',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('electricity demand HP',n,bu,hp,h)$(report_heat_tech_hours('electricity demand HP',n,bu,hp,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('electricity demand hybrid heating',n,bu,hel,h)$(report_heat_tech_hours('electricity demand hybrid heating',n,bu,hel,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('fossil fuel demand hybrid heating',n,bu,hfo,h)$(report_heat_tech_hours('fossil fuel demand hybrid heating',n,bu,hfo,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('SETS inflow',n,bu,ch,h)$(report_heat_tech_hours('SETS inflow',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('SETS level',n,bu,ch,h)$(report_heat_tech_hours('SETS level',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('SETS outflow',n,bu,ch,h)$(report_heat_tech_hours('SETS outflow',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('Storage inflow',n,bu,hst,h)$(report_heat_tech_hours('Storage inflow',n,bu,hst,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('Storage level',n,bu,ch,h)$(report_heat_tech_hours('Storage level',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('Storage outflow',n,bu,ch,h)$(report_heat_tech_hours('Storage outflow',n,bu,ch,h) < eps_rep_abs) = 0 ;

                 report_heat_tech_hours('heat demand - DHW',n,bu,ch,h)$(report_heat_tech_hours('heat demand - DHW',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('DHW supply - direct electric',n,bu,ch,h)$(report_heat_tech_hours('DHW supply - direct electric',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('DHW supply - SETS aux electric',n,bu,ch,h)$(report_heat_tech_hours('DHW supply - SETS aux electric',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('DHW supply - storage heating',n,bu,ch,h)$(report_heat_tech_hours('DHW supply - storage heating',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('electricity demand DHW - direct electric',n,bu,ch,h)$(report_heat_tech_hours('electricity demand DHW - direct electric',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('electricity demand DHW - SETS aux electric',n,bu,ch,h)$(report_heat_tech_hours('electricity demand DHW - SETS aux electric',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('DHW - SETS aux hot water storage level',n,bu,ch,h)$(report_heat_tech_hours('DHW - SETS aux hot water storage level',n,bu,ch,h) < eps_rep_abs) = 0 ;

                 report_heat_tech_hours('price heat electricity consumption',n,bu,ch,h)$(report_heat_tech_hours('price heat electricity consumption',n,bu,ch,h) < eps_rep_abs) = 0 ;
                 report_heat_tech_hours('price DHW electricity consumption',n,bu,ch,h)$( report_heat_tech_hours('price DHW electricity consumption',n,bu,ch,h)< eps_rep_abs) = 0 ;


         report_heat_tech('heat supply',n,bu,ch)$feat_node('heat',n) = sum( h , theta_dir(n,bu,ch) * H_DIR.l(n,bu,ch,h) + theta_sets(n,bu,ch) * (H_SETS_OUT.l(n,bu,ch,h) + (1-eta_heat_stat(n,bu,ch)) * H_SETS_LEV.l(n,bu,ch,h-1)) + theta_storage(n,bu,ch) * H_STO_OUT.l(n,bu,ch,h) ) ;
         report_heat_tech('DHW supply',n,bu,ch)$feat_node('heat',n) = sum( h , theta_dir(n,bu,ch) * H_DHW_DIR.l(n,bu,ch,h) + theta_sets(n,bu,ch) * H_DHW_AUX_OUT.l(n,bu,ch,h) + theta_storage(n,bu,ch) * H_DHW_STO_OUT.l(n,bu,ch,h) ) ;
         report_heat_tech('total costs',n,bu,ch)$feat_node('heat',n) = sum( h , report_heat_tech_hours('price heat electricity consumption',n,bu,ch,h) * ( theta_dir(n,bu,ch) * H_DIR.l(n,bu,ch,h) + theta_sets(n,bu,ch) * (H_SETS_IN.l(n,bu,ch,h) + corr_fac_sets(n,bu,ch,h)) + theta_hp(n,bu,ch) * ( H_HP_IN.l(n,bu,ch,h) + corr_fac_hp(n,bu,ch,h)) + theta_elec(n,bu,ch) * H_ELECTRIC_IN.l(n,bu,ch,h) + corr_fac_h_elec(n,bu,ch,h)) + report_heat_tech_hours('price DHW electricity consumption',n,bu,ch,h) * (theta_dir(n,bu,ch) * H_DHW_DIR.l(n,bu,ch,h) + theta_sets(n,bu,ch) * (H_DHW_AUX_ELEC_IN.l(n,bu,ch,h) + corr_fac_sets_aux(n,bu,ch,h))) ) ;
         report_heat_tech('total electricity consumption',n,bu,ch)$feat_node('heat',n) = sum( h , theta_dir(n,bu,ch) * H_DIR.l(n,bu,ch,h) + theta_sets(n,bu,ch) * (H_SETS_IN.l(n,bu,ch,h) + corr_fac_sets(n,bu,ch,h)) + theta_hp(n,bu,ch) * ( H_HP_IN.l(n,bu,ch,h) + corr_fac_hp(n,bu,ch,h)) + theta_elec(n,bu,ch) * H_ELECTRIC_IN.l(n,bu,ch,h) + corr_fac_h_elec(n,bu,ch,h) + theta_dir(n,bu,ch) * H_DHW_DIR.l(n,bu,ch,h) + theta_sets(n,bu,ch) * (H_DHW_AUX_ELEC_IN.l(n,bu,ch,h) + corr_fac_sets_aux(n,bu,ch,h)) ) ;
         report_heat_tech('average price heat electricity consumption',n,bu,ch)$(feat_node('heat',n) AND report_heat_tech('total electricity consumption',n,bu,ch)) = report_heat_tech('total costs',n,bu,ch) / report_heat_tech('total electricity consumption',n,bu,ch) ;
         report_heat_tech('average price heat electricity consumption over all bu',n,'over all bu',ch)$(feat_node('heat',n) AND report_heat_tech('total electricity consumption',n,'bu1',ch)) = sum( bu, report_heat_tech('average price heat electricity consumption',n,bu,ch) * report_heat_tech('total electricity consumption',n,bu,ch) ) / sum( bu, report_heat_tech('total electricity consumption',n,bu,ch) ) ;
         report_heat_tech('Storage cycles (inflow)',n,bu,ch)$(feat_node('heat',n) AND n_heat_e(n,bu,ch)) = sum( h , theta_sets(n,bu,ch) * (H_SETS_IN.l(n,bu,ch,h) + corr_fac_sets(n,bu,ch,h)) + theta_hp(n,bu,ch) * ( H_HP_IN.l(n,bu,ch,h) + corr_fac_hp(n,bu,ch,h)))/n_heat_e(n,bu,ch);

                 report_heat_tech('heat supply',n,bu,ch)$(report_heat_tech('heat supply',n,bu,ch) < eps_rep_abs) = 0 ;
                 report_heat_tech('DHW supply',n,bu,ch)$(report_heat_tech('DHW supply',n,bu,ch) < eps_rep_abs) = 0 ;
                 report_heat_tech('total costs',n,bu,ch)$(report_heat_tech('total costs',n,bu,ch) < eps_rep_abs) = 0 ;
                 report_heat_tech('total electricity consumption',n,bu,ch)$(report_heat_tech('total electricity consumption',n,bu,ch) < eps_rep_abs) = 0 ;
                 report_heat_tech('average price heat electricity consumption',n,bu,ch)$(report_heat_tech('average price heat electricity consumption',n,bu,ch) < eps_rep_abs) = 0 ;
                 report_heat_tech('average price heat electricity consumption over all bu',n,'over all bu',ch)$(report_heat_tech('average price heat electricity consumption over all bu',n,'over all bu',ch) < eps_rep_abs) = 0 ;
                 report_heat_tech('Storage cycles (inflow)',n,bu,ch)$(report_heat_tech('Storage cycles (inflow)',n,bu,ch) < eps_rep_abs) = 0 ;
$ontext
$offtext


* ----------------------------------------------------------------------------

* Reserves and heat
%heat%$ontext
%reserves_endogenous%$ontext
        report_reserves_tech('reserve provision shares',reserves_nonprim,'sets',n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = (sum( h ,  sum( bu , theta_sets(n,bu,'setsh') * RP_SETS.l(n,reserves_nonprim,bu,'setsh',h))) / ( (card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) *  (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )))  ;
        report_reserves_tech('reserve activation shares',reserves_nonprim,'sets',n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = (sum( h ,  sum( bu , theta_sets(n,bu,'setsh') * RP_SETS.l(n,reserves_nonprim,bu,'setsh',h))*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )))  ;
*        report_reserves_tech('reserve provision shares',reserves_nonprim,hst,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = (sum( h , sum( bu , theta_storage(n,bu,hst) * (RP_HP.l(n,reserves_nonprim,bu,hst,h) + RP_H_ELEC.l(n,reserves_nonprim,bu,hst,h) ))) / ( (card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000) ))  ;
*        report_reserves_tech('reserve activation shares',reserves_nonprim,hst,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = (sum( h ,  sum( bu , theta_storage(n,bu,hst) * (RP_HP.l(n,reserves_nonprim,bu,hst,h) + RP_H_ELEC.l(n,reserves_nonprim,bu,hst,h)  ))*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) ))  ;
        report_reserves_tech('reserve provision shares',,reserves_nonprim,hst,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = (sum( h ,  sum( bu , theta_storage(n,bu,hst) * (RP_H_ELEC.l(n,reserves_nonprim,bu,hst,h) + RP_HP.l(n,reserves_nonprim,bu,hst,h)))) / ( (card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )))  ;
        report_reserves_tech('reserve activation shares',reserves_nonprim,hst,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = (sum( h ,  sum( bu , theta_storage(n,bu,hst) * (RP_H_ELEC.l(n,reserves_nonprim,bu,hst,h) + RP_HP.l(n,reserves_nonprim,bu,hst,h)))*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000) )))  ;
        report_reserves_tech('reserve provision shares',reserves_nonprim,'sets aux',n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = (sum( h ,  sum( bu , theta_sets(n,bu,'setsh') * RP_SETS_AUX.l(n,reserves_nonprim,bu,'setsh',h))) / ( (card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )))  ;
        report_reserves_tech('reserve activation shares',reserves_nonprim,'sets aux',n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = (sum( h ,  sum( bu , theta_sets(n,bu,'setsh') * RP_SETS_AUX.l(n,reserves_nonprim,bu,'setsh',h))*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) )))  ;
$ontext
$offtext
%heat%$ontext
%reserves_exogenous%$ontext

$ontext
* Note: to be reviewed / corrected
        report_reserves_tech('reserve provision shares',reserves_nonprim,'sets',n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = sum( h , sum( bu , theta_sets(n,bu,'setsh') * P_SETS.l(n,reserves_nonprim,bu,'setsh',h))) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_nonprim,h)) ;
        report_reserves_tech('reserve activation shares',reserves_nonprim,'sets',n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = sum( h ,  sum( bu , theta_sets(n,bu,'setsh') * RP_SETS.l(n,reserves_nonprim,bu,'setsh',h))*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * reserves_exogenous(n,reserves_nonprim,h) )  ;
*        report_reserves_tech('reserve provision shares',reserves_nonprim,hst,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = (sum( h , sum( bu , theta_storage(n,bu,hst) * (RP_HP.l(n,reserves_nonprim,bu,hst,h) + RP_H_ELEC.l(n,reserves_nonprim,bu,hst,h) ))) / ( (card(h)-1) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+ N_RES_PRO.l(n,nondisnondis))/1000) ))  ;
*        report_reserves_tech('reserve activation shares',reserves_nonprim,hst,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = (sum( h , sum( bu , theta_storage(n,bu,hst) * (RP_HP.l(n,reserves_nonprim,bu,hst,h) + RP_H_ELEC.l(n,reserves_nonprim,bu,hst,h)  ))*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * 1000 * phi_reserves_share(n,reserves_nonprim) * (reserves_intercept(n,reserves_nonprim) + sum(nondisnondis,reserves_slope(n,reserves_nonprim,nondisnondis) * (N_TECH.l(n,nondisnondis)+N_RES_PRO.l(n,nondisnondis))/1000) ))  ;
        report_reserves_tech('reserve provision shares',reserves_nonprim,hst,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = sum( h , sum( bu , theta_storage(n,bu,hst) * (RP_H_ELEC.l(n,reserves_nonprim,bu,hst,h) + RP_HP.l(n,reserves_nonprim,bu,hst,h)))) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_nonprim,h)) ;
        report_reserves_tech('reserve activation shares',reserves_nonprim,hst,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = sum( h , sum( bu , theta_storage(n,bu,hst) * (RP_H_ELEC.l(n,reserves_nonprim,bu,hst,h) + RP_HP.l(n,reserves_nonprim,bu,hst,h)))*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * reserves_exogenous(n,reserves_nonprim,h) )  ;
        report_reserves_tech('reserve provision shares',reserves_nonprim,'sets aux',n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = sum( h ,  sum( bu , theta_sets(n,bu,'setsh') * RP_SETS_AUX.l(n,reserves_nonprim,bu,'setsh',h))) / sum( h$(ord(h) > 1), reserves_exogenous(n,reserves_nonprim,h)) ;
        report_reserves_tech('reserve activation shares',reserves_nonprim,'sets aux',n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = sum( h , sum( bu , theta_sets(n,bu,'setsh') * RP_SETS_AUX.l(n,reserves_nonprim,bu,'setsh',h))*phi_reserves_call(n,reserves_nonprim,h)) / sum( h , phi_reserves_call(n,reserves_nonprim,h) * reserves_exogenous(n,reserves_nonprim,h) )  ;
$ontext
$offtext

%heat%$ontext
%reserves%$ontext
        report_reserves_tech_hours('Reserves provision',reserves,'sets aux',h,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = sum( bu , theta_sets(n,bu,'setsh') * RP_SETS_AUX.l(n,reserves,bu,'setsh',h))  ;
        report_reserves_tech_hours('Reserves activation',reserves,'sets aux',h,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = sum( bu , theta_sets(n,bu,'setsh') * RP_SETS_AUX.l(n,reserves,bu,'setsh',h))*phi_reserves_call(n,reserves,h)  ;
        report_reserves_tech_hours('Reserves provision',reserves,'sets',h,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = sum( bu , theta_sets(n,bu,'setsh') * RP_SETS.l(n,reserves,bu,'setsh',h))  ;
        report_reserves_tech_hours('Reserves activation',reserves,'sets',h,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = sum( bu , theta_sets(n,bu,'setsh') * RP_SETS.l(n,reserves,bu,'setsh',h))*phi_reserves_call(n,reserves,h)  ;
        report_reserves_tech_hours('Reserves provision',reserves,hp,h,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = sum( bu , theta_hp(n,bu,hp) * RP_HP.l(n,reserves,bu,hp,h))  ;
        report_reserves_tech_hours('Reserves activation',reserves,hp,h,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = sum( bu , theta_hp(n,bu,hp) * RP_HP.l(n,reserves,bu,hp,h))*phi_reserves_call(n,reserves,h)  ;
        report_reserves_tech_hours('Reserves provision',reserves,hel,h,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = sum( bu , theta_storage(n,bu,hel) * RP_H_ELEC.l(n,reserves,bu,hel,h))  ;
        report_reserves_tech_hours('Reserves activation',reserves,hel,h,n)$(feat_node('heat',n) AND feat_node('reserves',n) ) = sum( bu , theta_storage(n,bu,hel) * RP_H_ELEC.l(n,reserves,bu,hel,h))*phi_reserves_call(n,reserves,h)  ;

                  report_reserves_tech('reserve provision shares',reserves_nonprim,'sets',n)$( report_reserves_tech('reserve provision shares',reserves_nonprim,'sets',n) < eps_rep_rel) = 0 ;
                  report_reserves_tech('reserve activation shares',reserves_nonprim,'sets',n)$( report_reserves_tech('reserve activation shares',reserves_nonprim,'sets',n) < eps_rep_rel) = 0 ;
                  report_reserves_tech('reserve provision shares',reserves_nonprim,hst,n)$( report_reserves_tech('reserve provision shares',reserves_nonprim,hst,n) < eps_rep_rel) = 0 ;
                  report_reserves_tech('reserve activation shares',reserves_nonprim,hst,n)$( report_reserves_tech('reserve activation shares',reserves_nonprim,hst,n) < eps_rep_rel) = 0 ;
*                  report_reserves_tech('reserve provision shares',reserves_nonprim,hel,n)$( report_reserves_tech('reserve provision shares',reserves_nonprim,hel,n) < eps_rep_rel) = 0 ;
*                  report_reserves_tech('reserve activation shares',reserves_nonprim,hel,n)$( report_reserves_tech('reserve activation shares',reserves_nonprim,hel,n) < eps_rep_rel) = 0 ;
                  report_reserves_tech('reserve provision shares',reserves_nonprim,'sets aux',n)$( report_reserves_tech('reserve provision shares',reserves_nonprim,'sets aux',n) < eps_rep_rel) = 0 ;
                  report_reserves_tech('reserve activation shares',reserves_nonprim,'sets aux',n)$( report_reserves_tech('reserve activation shares',reserves_nonprim,'sets aux',n) < eps_rep_rel) = 0 ;

                  report_reserves_tech_hours('Reserves provision',reserves,'sets aux',h,n)$(report_reserves_tech_hours('Reserves provision',reserves,'sets aux',h,n) < eps_rep_abs) = 0 ;
                  report_reserves_tech_hours('Reserves activation',reserves,'sets aux',h,n)$(report_reserves_tech_hours('Reserves activation',reserves,'sets aux',h,n) < eps_rep_abs) = 0 ;
                  report_reserves_tech_hours('Reserves provision',reserves,'sets',h,n)$(report_reserves_tech_hours('Reserves provision',reserves,'sets',h,n) < eps_rep_abs) = 0 ;
                  report_reserves_tech_hours('Reserves activation',reserves,'sets',h,n)$( report_reserves_tech_hours('Reserves activation',reserves,'sets',h,n) < eps_rep_abs) = 0 ;
                  report_reserves_tech_hours('Reserves provision',reserves,hp,h,n)$( report_reserves_tech_hours('Reserves provision',reserves,hp,h,n) < eps_rep_abs) = 0 ;
                  report_reserves_tech_hours('Reserves activation',reserves,hp,h,n)$( report_reserves_tech_hours('Reserves activation',reserves,hp,h,n) < eps_rep_abs) = 0 ;
                  report_reserves_tech_hours('Reserves provision',reserves,hel,h,n)$( report_reserves_tech_hours('Reserves provision',reserves,hel,h,n) < eps_rep_abs) = 0 ;
                  report_reserves_tech_hours('Reserves activation',reserves,hel,h,n)$( report_reserves_tech_hours('Reserves activation',reserves,hel,h,n) < eps_rep_abs) = 0 ;

* ----------------------------------------------------------------------------

$ontext
$offtext

* P2H2
%P2H2%$ontext

*** Capacities

h2_report_electrolysis_cap('Electrolysis Capacity (centralized)',h2_tech,n) =  max( H2_N_PROD_CENT.l(n,h2_tech) , EPS ) ;
h2_report_electrolysis_cap('Electrolysis Capacity (decentralized)',h2_tech,n) =  max( H2_N_PROD_DECENT.l(n,h2_tech) , EPS ) ;


h2_report_recon_cap('Reconversion Capacity',h2_tech_recon,n) =  max( H2_N_RECON.l(n,h2_tech_recon) , EPS ) ;


h2_report_aux_cap_tech2channel('Aux. prod. site - tech2channel',h2_tech,h2_channel,n) =  max( H2_N_PROD_AUX.l(n,h2_tech,h2_channel) , EPS ) ;

h2_report_aux_cap_channel2tech('Aux. recon. site - channel2tech',h2_tech_recon,h2_channel,n) =  max( H2_N_RECON_AUX.l(n,h2_channel,h2_tech_recon) , EPS ) ;


h2_report_infrastructure_cap_channel('HYD LIQ',h2_channel,n) =  max(  H2_N_HYD_LIQ.l(n,h2_channel) , EPS ) ;

h2_report_infrastructure_cap_channel('Prod. site storage',h2_channel,n) = max(  H2_N_STO.l(n,h2_channel) , EPS ) ;

h2_report_infrastructure_cap_channel('Aux. bf. trans.',h2_channel,n) =  max(  H2_N_AUX_PRETRANS.l(n,h2_channel) , EPS ) ;

h2_report_infrastructure_cap_channel('Transportation',h2_channel,n) =  max(  H2_N_TRANS.l(n,h2_channel) , EPS ) ;

h2_report_infrastructure_cap_channel('Aux. bf. LP storage',h2_channel,n) =  max(  H2_N_AUX_BFLP_STO.l(n,h2_channel) , EPS ) ;

h2_report_infrastructure_cap_channel('LP storage',h2_channel,n) =  max(  H2_N_LP_STO.l(n,h2_channel) , EPS ) ;

h2_report_infrastructure_cap_channel('DEHYD EVAP',h2_channel,n) =  max(  H2_N_DEHYD_EVAP.l(n,h2_channel) , EPS ) ;

h2_report_infrastructure_cap_channel('Aux. bf. MP storage',h2_channel,n) =  max(  H2_N_AUX_BFMP_STO.l(n,h2_channel) , EPS ) ;

h2_report_infrastructure_cap_channel('MP storage ',h2_channel,n) =  max(  H2_N_MP_STO.l(n,h2_channel) , EPS ) ;

h2_report_infrastructure_cap_channel('Aux. bf. HP storage',h2_channel,n) =  max( H2_N_AUX_BFHP_STO.l(n,h2_channel) , EPS ) ;

h2_report_infrastructure_cap_channel('HP storage',h2_channel,n) = max(  H2_N_HP_STO.l(n,h2_channel) , EPS ) ;

h2_report_infrastructure_cap_channel('Aux. bf. fueling',h2_channel,n) =  max(  H2_N_AUX_BFFUEL.l(n,h2_channel) , EPS ) ;


h2_report_h2mobiliy_shares('H2mobility shares', h2_channel,n) = max(  H2_CHANNEL_SHARE.l(n,h2_channel) , EPS ) ;


*** Flows & Levels

h2_report_electrolysis_flows('Production outflow in kWh of H2',h,n,h2_tech,h2_channel) = max( H2_PROD_OUT.l(n,h2_tech,h2_channel,h) , EPS ) ;
h2_report_recon_flows('Recon outflow in kWh',h,n,h2_tech_recon,h2_channel) = max( H2_E_RECON_OUT.l(n,h2_channel,h2_tech_recon,h) , EPS ) ;


h2_report_bypass_1_flow('Bypass 1 outflow',h,n,h2_channel) = max( sum(h2_tech,H2_BYPASS_1.l(n,h2_tech,h2_channel,h)) , EPS ) ;
h2_report_bypass_2_flow('Bypass 2 outflow',h,n,h2_channel) = max( H2_BYPASS_2.l(n,h2_channel,h) , EPS ) ;


h2_report_prod_sto_level('Production storage levels',h,n,h2_channel) = max( H2_STO_P_L.l(n,h2_channel,h) , EPS ) ;

h2_report_LP_sto_level('LP storage levels',h,n,h2_channel) = max( H2_LP_STO_L.l(n,h2_channel,h) , EPS ) ;

h2_report_MP_sto_level('MP storage levels',h,n,h2_channel) = max( H2_MP_STO_L.l(n,h2_channel,h) , EPS ) ;

h2_report_HP_sto_level('HP levels',h,n,h2_channel) = max( H2_HP_STO_L.l(n,h2_channel,h) , EPS ) ;


h2_other_flows('HYD LIQ flow out',h2_channel,n,h) = max( EPS , H2_HYD_LIQ_OUT.l(n,h2_channel,h) ) ;

h2_other_flows('Prod. site storage flow in',h2_channel,n,h) = max( EPS , H2_STO_P_IN.l(n,h2_channel,h) ) ;

h2_other_flows('Prod. site storage flow out',h2_channel,n,h) = max( EPS , H2_STO_P_OUT.l(n,h2_channel,h) ) ;

h2_other_flows('LP storage fill. flow in',h2_channel,n,h) = max( EPS , H2_AUX_BFLP_STO_IN.l(n,h2_channel,h) ) ;

h2_other_flows('LP storage fill. flow out',h2_channel,n,h) = max( EPS , H2_LP_STO_OUT.l(n,h2_channel,h) ) ;

h2_other_flows('DEHYD EVAP flow out',h2_channel,n,h) = max( EPS , H2_DEHYD_EVAP_OUT.l(n,h2_channel,h) ) ;

h2_other_flows('Fill. site storage flow in',h2_channel,n,h) = max( EPS , H2_AUX_BFHP_STO_OUT.l(n,h2_channel,h) ) ;

h2_other_flows('Fill. site storage flow out',h2_channel,n,h) = max( EPS , H2_HP_STO_OUT.l(n,h2_channel,h) ) ;


h2_report_filling_flow('Filling outflow',h,n) = sum( h2_channel , max( H2_HP_STO_OUT.l(n,h2_channel,h) , EPS ) ) ;


*** Costs

h2_fixed_costs_electrolysis_aux('Weights electrolysis - dis. by technology/channel',h2_tech,h2_channel,n) = max( EPS , ( sum( h , max( EPS ,  H2_PROD_OUT.l(n,h2_tech,h2_channel,h) )$( h2_channel_wo_decent_set(h2_channel) ) ) / max( 0.0000000000001 , sum( (h2_channel_alias,h) , max( EPS ,  H2_PROD_OUT.l(n,h2_tech,h2_channel_alias,h) )$( h2_channel_wo_decent_set(h2_channel_alias) ) ) ) ) + 1$( NOT h2_channel_wo_decent_set(h2_channel) ) ) ;

h2_fixed_costs_electrolysis_aux('Weights aux production site - dis. by technology/channel',h2_tech,h2_channel,n) =  max( EPS , h2_prod_aux_sw(n,h2_tech,h2_channel) * h2_prod_aux_c_a_overnight(n,h2_tech,h2_channel) * H2_N_PROD_AUX.l(n,h2_tech,h2_channel) *  sum(h2_channel_alias, 1$((h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias)) and sum (h,H2_PROD_AUX_OUT.l(n,h2_channel_alias,h)) > 0 ) ) * sum (h,H2_PROD_AUX_OUT.l(n,h2_channel,h))/(0.000000000001 +  sum(h,sum(h2_channel_alias,H2_PROD_AUX_OUT.l(n,h2_channel_alias,h)$(h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias))))  ) ) ;

h2_fixed_costs_reconversion('Weights reconversion - dis. by recon technology/channel',h2_tech_recon,h2_channel,n) = max( EPS , ( sum( h , max( EPS ,  H2_E_RECON_OUT.l(n,h2_channel,h2_tech_recon,h) ) $( h2_recon_set(n,h2_channel) ) ) / max( 0.0000000000001 , sum( (h2_channel_alias,h) , max( EPS ,  H2_E_RECON_OUT.l(n,h2_channel_alias,h2_tech_recon,h) )$( h2_recon_set(n,h2_channel_alias) ) ) ) )  ) ;


h2_fixed_costs_electrolysis_aux('Costs electrolysis - weighted - dis. by technology/channel',h2_tech,h2_channel,n) = max( EPS , ( sum( h , max( EPS , H2_PROD_OUT.l(n,h2_tech,h2_channel,h) )$( h2_channel_wo_decent_set(h2_channel) ) ) / max( 0.0000000000001 , sum( (h2_channel_alias,h) , max( EPS ,  H2_PROD_OUT.l(n,h2_tech,h2_channel_alias,h) )$( h2_channel_wo_decent_set(h2_channel_alias) ) ) ) ) *  H2_N_PROD_CENT.l(n,h2_tech) * ( h2_prod_c_ad_a_overnight(n,h2_tech) + h2_prod_c_ad_a_fix(n,h2_tech) + h2_prod_c_ad_a_fix2(n,h2_tech) ) + 1$( NOT h2_channel_wo_decent_set(h2_channel) ) * H2_N_PROD_DECENT.l(n,h2_tech) * ( h2_prod_c_a_overnight(n,h2_tech) + h2_prod_c_a_fix(n,h2_tech) + h2_prod_c_a_fix2(n,h2_tech) ) ) ;

h2_fixed_costs_electrolysis_aux('Costs aux prod. site - weighted - dis. by technology/channel',h2_tech,h2_channel,n) = max( EPS , h2_prod_aux_sw(n,h2_tech,h2_channel) * h2_prod_aux_c_a_overnight(n,h2_tech,h2_channel) * H2_N_PROD_AUX.l(n,h2_tech,h2_channel) *  sum(h2_channel_alias, 1$((h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias)) and sum (h,H2_PROD_AUX_OUT.l(n,h2_channel_alias,h)) > 0 ) ) * sum (h,H2_PROD_AUX_OUT.l(n,h2_channel,h)) /(0.000000000001 +  sum(h,sum(h2_channel_alias,H2_PROD_AUX_OUT.l(n,h2_channel_alias,h)$(h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias))))  ) ) ;

h2_fixed_costs_reconversion('Costs recon. - weighted - dis. by recon',h2_tech_recon,h2_channel,n) = max( EPS , ( sum( h , max( EPS ,  H2_E_RECON_OUT.l(n,h2_channel,h2_tech_recon,h) )$( h2_recon_set(n,h2_channel) ) ) / max( 0.0000000000001 , sum( (h2_channel_alias,h) , max( EPS ,  H2_E_RECON_OUT.l(n,h2_channel_alias,h2_tech_recon,h) )$( h2_recon_set(n,h2_channel_alias) ) ) ) ) * H2_N_RECON.l(n,h2_tech_recon) * ( h2_recon_c_a_overnight(n,h2_tech_recon) )   ) ;

h2_fixed_costs_reconversion('Aux production site - dis. by recon technology/channel',h2_tech_recon,h2_channel,n) = max( EPS , h2_recon_aux_sw(n,h2_tech_recon,h2_channel) * h2_recon_aux_c_a_overnight(n,h2_tech_recon,h2_channel) * H2_N_RECON_AUX.l(n,h2_channel,h2_tech_recon) ) ;


h2_fixed_costs('HYD LIQ - weighted',h2_channel,n) = max( EPS , h2_hyd_liq_sw(n,h2_channel) * h2_hyd_liq_c_a_overnight(n,h2_channel) * H2_N_HYD_LIQ.l(n,h2_channel) *  sum(h2_channel_alias, 1$((h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias)) and  sum (h,H2_HYD_LIQ_OUT.l(n,h2_channel_alias,h)) > 0 ) )  * sum (h,H2_HYD_LIQ_OUT.l(n,h2_channel,h)) /(0.000000000001 + sum(h,sum(h2_channel_alias,H2_HYD_LIQ_OUT.l(n,h2_channel_alias,h)$(h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias))))  )) ;

h2_fixed_costs('Prod. site storage - weighted',h2_channel,n) = max( EPS , h2_sto_p_sw(n,h2_channel) * h2_sto_p_c_a_overnight(n,h2_channel) * H2_N_STO.l(n,h2_channel) *  sum(h2_channel_alias, 1$((h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias)) and sum (h,H2_STO_P_IN.l(n,h2_channel_alias,h)) > 0 ) ) * sum (h,H2_STO_P_IN.l(n,h2_channel,h)) /(0.000000000001 + sum(h,sum(h2_channel_alias,H2_STO_P_IN.l(n,h2_channel_alias,h)$(h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias))))  )) ;


h2_fixed_costs('Aux before trans.',h2_channel,n) = max( EPS , h2_aux_pretrans_sw(n,h2_channel) * h2_aux_pretrans_c_a_overnight(n,h2_channel) * H2_N_AUX_PRETRANS.l(n,h2_channel) ) ;

h2_fixed_costs('Transportation',h2_channel,n) = max( EPS , h2_trans_sw(n,h2_channel) * h2_trans_c_a_overnight(n,h2_channel) * H2_N_TRANS.l(n,h2_channel) ) ;

h2_fixed_costs('Aux bf LP storage',h2_channel,n) = max( EPS , h2_aux_bflp_sto_sw(n,h2_channel) * h2_aux_bflp_sto_c_a_overnight(n,h2_channel) * H2_N_AUX_BFLP_STO.l(n,h2_channel) ) ;

h2_fixed_costs('LP storage',h2_channel,n) = max( EPS , h2_lp_sto_sw(n,h2_channel) * h2_lp_sto_c_a_overnight(n,h2_channel) * H2_N_LP_STO.l(n,h2_channel) ) ;

h2_fixed_costs('DEHYD EVAP',h2_channel,n) = max( EPS , h2_dehyd_evap_sw(n,h2_channel) * h2_dehyd_evap_c_a_overnight(n,h2_channel) * H2_N_DEHYD_EVAP.l(n,h2_channel) ) ;

h2_fixed_costs('Aux bf MP storage',h2_channel,n) = max( EPS , h2_aux_bfmp_sto_sw(n,h2_channel) * h2_aux_bfmp_sto_c_a_overnight(n,h2_channel) * H2_N_AUX_BFMP_STO.l(n,h2_channel) ) ;

h2_fixed_costs('MP storage',h2_channel,n) = max( EPS , h2_MP_sto_sw(n,h2_channel) * h2_MP_sto_c_a_overnight(n,h2_channel) * H2_N_MP_STO.l(n,h2_channel) ) ;

h2_fixed_costs('Aux  BFHP storage',h2_channel,n) = max( EPS , h2_aux_bfhp_sto_sw(n,h2_channel) * aux_bfhp_sto_c_a_overnight(n,h2_channel) * H2_N_AUX_BFHP_STO.l(n,h2_channel) ) ;

h2_fixed_costs('HP storage',h2_channel,n) = max( EPS , h2_hp_sto_sw(n,h2_channel) * h2_hp_sto_c_a_overnight(n,h2_channel) * H2_N_HP_STO.l(n,h2_channel) ) ;

h2_fixed_costs('Aux bf fuel ',h2_channel,n) = max( EPS , h2_aux_bffuel_sw(n,h2_channel) * h2_aux_bffuel_c_a_overnight(n,h2_channel ) * H2_N_AUX_BFFUEL.l(n,h2_channel) ) ;


h2_fixed_costs_oa_by_channel('Overall fixed costs per channel',h2_channel,n) =
sum (h2_tech,h2_fixed_costs_electrolysis_aux('Costs electrolysis - weighted - dis. by technology/channel',h2_tech,h2_channel,n)
           + h2_fixed_costs_electrolysis_aux('Costs aux prod. site - weighted - dis. by technology/channel',h2_tech,h2_channel,n) )
+ h2_fixed_costs('HYD LIQ - weighted',h2_channel,n)
+ h2_fixed_costs('Prod. site storage - weighted',h2_channel,n)
+ h2_fixed_costs('Aux before trans.',h2_channel,n)
+ h2_fixed_costs('Transportation',h2_channel,n)
+ h2_fixed_costs('Aux bf LP storage',h2_channel,n)
+ h2_fixed_costs('LP storage',h2_channel,n)
+ h2_fixed_costs('DEHYD EVAP',h2_channel,n)
+ h2_fixed_costs('Aux bf MP storage',h2_channel,n)
+ h2_fixed_costs('MP storage',h2_channel,n)
+ h2_fixed_costs('Aux  BFHP storage',h2_channel,n)
+ h2_fixed_costs('HP storage',h2_channel,n)
+ h2_fixed_costs('Aux bf fuel ',h2_channel,n)
+sum( h2_tech_recon , h2_fixed_costs_reconversion('Aux production site - dis. by recon technology/channel',h2_tech_recon,h2_channel,n)
                                        + h2_fixed_costs_reconversion('Costs recon. - weighted - dis. by recon',h2_tech_recon,h2_channel,n)
        )
 ;




report_cost('Nodal cost: H2 fuel & recon',n) = sum( h2_channel , h2_fixed_costs_oa_by_channel('Overall fixed costs per channel',h2_channel,n) ) ;
report_cost('Nodal cost: H2 fuel & recon',n)$(report_cost('Nodal cost: H2 fuel & recon',n) < eps_rep_abs) = 0 ;

$ontext
$offtext

        report_cost('Nodal cost: total',n) = report_cost('Nodal cost: dispatch',n) + report_cost('Nodal cost: investment and fix',n) + report_cost('Nodal cost: infeasibility',n)
%EV%$ontext
         + report_cost('Nodal cost: PHEV fuel',n)
         + report_cost('Nodal cost: EV charging and discharging',n)
$ontext
$offtext
%heat%$ontext
         + report_cost('Nodal cost: fossil heating',n)
         + report_cost('Nodal cost: infeasibility heating',n)
$ontext
$offtext
%P2H2%$ontext
                 +  report_cost('Nodal cost: H2 fuel & recon',n)
$ontext
$offtext
;

report_cost('Nodal cost: total',n)$(report_cost('Nodal cost: total',n) < eps_rep_abs) = 0 ;




*****

%P2H2%$ontext

h2_elec_dem_electrolysis_aux_per_h('Electrolysis - by tech.',h2_tech,h2_channel,n,h) = max( EPS ,
max( EPS , H2_E_H2_IN.l(n,h2_tech,h2_channel,h) )
) ;

h2_elec_dem_electrolysis_aux_per_h('Aux prod. site - by tech.',h2_tech,h2_channel,n,h) = max( EPS ,
h2_prod_aux_sw(n,h2_tech,h2_channel) * h2_prod_aux_ed(n,h2_tech,h2_channel) *
max( EPS , H2_PROD_AUX_IN.l(n,h2_tech,h2_channel,h) )
) ;

h2_elec_dem_reconversion_per_h('Aux recon. site - by tech.',h2_tech_recon,h2_channel,n,h) = max( EPS ,
h2_recon_aux_sw(n,h2_tech_recon,h2_channel) * h2_recon_aux_ed(n,h2_tech_recon,h2_channel) *
max( EPS , H2_RECON_AUX_OUT.l(n,h2_channel,h2_tech_recon,h)/(1-H2_eta_recon_aux(n,h2_channel,h2_tech_recon)) )
) ;





h2_elec_dem_per_h('Production',h2_channel,n,h) = max( EPS ,
sum( h2_tech ,
max( EPS , H2_E_H2_IN.l(n,h2_tech,h2_channel,h) )
)
) ;

h2_elec_dem_per_h('Aux prod. site',h2_channel,n,h) = max( EPS ,
sum( h2_tech ,
h2_prod_aux_sw(n,h2_tech,h2_channel) * h2_prod_aux_ed(n,h2_tech,h2_channel) *
max( EPS , H2_PROD_AUX_IN.l(n,h2_tech,h2_channel,h) )
)
) ;

h2_elec_dem_per_h('HYD LIQ',h2_channel,n,h) = max( EPS ,
h2_hyd_liq_sw(n,h2_channel) * h2_hyd_liq_ed(n,h2_channel) *
max( EPS , H2_PROD_AUX_OUT.l(n,h2_channel,h) )
) ;

h2_elec_dem_per_h('Production site storage',h2_channel,n,h) = max( EPS ,
h2_sto_p_sw(n,h2_channel) * h2_sto_p_ed(n,h2_channel) *
max( EPS , H2_STO_P_L.l(n,h2_channel,h) )
) ;

h2_elec_dem_per_h('Aux before trans.',h2_channel,n,h) = max( EPS ,
h2_aux_pretrans_sw(n,h2_channel) * h2_aux_pretrans_ed(n,h2_channel) *
max( EPS , sum(h2_tech,H2_BYPASS_1.l(n,h2_tech,h2_channel,h)) + H2_STO_P_OUT.l(n,h2_channel,h) )
) ;

h2_elec_dem_per_h('Aux before LP storage .',h2_channel,n,h) = max( EPS ,
h2_aux_bflp_sto_sw(n,h2_channel) * h2_aux_bflp_sto_ed(n,h2_channel) *
max( EPS , H2_AUX_BFLP_STO_IN.l(n,h2_channel,h) )
) ;

h2_elec_dem_per_h('LP storage',h2_channel,n,h) = max( EPS ,
h2_lp_sto_sw(n,h2_channel) * h2_lp_sto_ed(n,h2_channel) *
max( EPS , H2_LP_STO_L.l(n,h2_channel,h) )
) ;

h2_elec_dem_per_h('DEHYD EVAP',h2_channel,n,h) = max( EPS ,
h2_dehyd_evap_sw(n,h2_channel) * ( 1 - h2_dehyd_evap_gas_sw(n,h2_channel) ) * h2_dehyd_evap_ed(n,h2_channel) *
max( EPS , H2_LP_STO_OUT.l(n,h2_channel,h) )
) ;

h2_elec_dem_per_h('Aux BFHP storage',h2_channel,n,h) = max( EPS ,
h2_aux_bfhp_sto_sw(n,h2_channel) * h2_aux_bfhp_sto_ed(n,h2_channel) *
max( EPS , H2_AUX_BFHP_STO_IN.l(n,h2_channel,h) )
) ;

h2_elec_dem_per_h('Aux bfMP storage',h2_channel,n,h) = max( EPS ,
h2_aux_bfmp_sto_sw(n,h2_channel) * h2_aux_bfmp_sto_ed(n,h2_channel) *
max( EPS , H2_DEHYD_EVAP_OUT.l(n,h2_channel,h) )
) ;

h2_elec_dem_per_h('MP storage',h2_channel,n,h) = max( EPS ,
h2_mp_sto_sw(n,h2_channel)  *
max( EPS , H2_MP_STO_L.l(n,h2_channel,h) )
) ;

h2_elec_dem_per_h('HP storage',h2_channel,n,h) = max( EPS ,
h2_hp_sto_sw(n,h2_channel) * h2_hp_sto_ed(n,h2_channel) *
max( EPS , H2_HP_STO_L.l(n,h2_channel,h) )
) ;

h2_elec_dem_per_h('Aux bffuel',h2_channel,n,h) = max( EPS ,
h2_aux_bffuel_sw(n,h2_channel) * h2_aux_bffuel_ed(n,h2_channel) *
max( EPS , H2_HP_STO_OUT.l(n,h2_channel,h) )
) ;

h2_elec_dem_per_h('Aux recon. site',h2_channel,n,h) = max( EPS ,
sum( h2_tech_recon ,
h2_recon_aux_sw(n,h2_channel,h2_tech_recon) * h2_recon_aux_ed(n,h2_tech_recon,h2_channel) *
max( EPS , H2_RECON_AUX_OUT.l(n,h2_channel,h2_tech_recon,h)/(1-H2_eta_recon_aux(n,h2_channel,h2_tech_recon)) )
)
) ;



h2_elec_dem_oa_by_channel_per_h('Overall elec. dem. per channel',h2_channel,n,h) =
  h2_elec_dem_per_h('Production',h2_channel,n,h)
+ h2_elec_dem_per_h('Aux prod. site',h2_channel,n,h)
+ h2_elec_dem_per_h('HYD LIQ',h2_channel,n,h)
+ h2_elec_dem_per_h('Production site storage',h2_channel,n,h)
+ h2_elec_dem_per_h('Aux before trans.',h2_channel,n,h)
+ h2_elec_dem_per_h('Aux before LP storage .',h2_channel,n,h)
+ h2_elec_dem_per_h('LP storage',h2_channel,n,h)
+ h2_elec_dem_per_h('DEHYD EVAP',h2_channel,n,h)
+ h2_elec_dem_per_h('Aux bfMP storage',h2_channel,n,h)
+ h2_elec_dem_per_h('MP storage',h2_channel,n,h)
+ h2_elec_dem_per_h('Aux BFHP storage',h2_channel,n,h)
+ h2_elec_dem_per_h('HP storage',h2_channel,n,h)
+ h2_elec_dem_per_h('Aux bffuel',h2_channel,n,h)
+ h2_elec_dem_per_h('Aux recon. site',h2_channel,n,h)
 ;









h2_elec_dem_electrolysis_aux('Production - by tech.',h2_tech,h2_channel,n) = max( EPS ,
sum( h ,
max( EPS , H2_E_H2_IN.l(n,h2_tech,h2_channel,h) )
)
) ;

h2_elec_dem_electrolysis_aux('Aux prod. site - by tech.',h2_tech,h2_channel,n) = max( EPS ,
h2_prod_aux_sw(n,h2_tech,h2_channel) * h2_prod_aux_ed(n,h2_tech,h2_channel) * sum( h ,
max( EPS , H2_PROD_AUX_IN.l(n,h2_tech,h2_channel,h) )
)
) ;

h2_elec_dem_reconversion('Aux recon. site - by tech.',h2_tech_recon,h2_channel,n) = max( EPS ,
h2_recon_aux_sw(n,h2_channel,h2_tech_recon) * h2_recon_aux_ed(n,h2_channel,h2_tech_recon) * sum( h ,
max( EPS , H2_RECON_AUX_OUT.l(n,h2_channel,h2_tech_recon,h)/(1-H2_eta_recon_aux(n,h2_channel,h2_tech_recon)) )
)
) ;


h2_elec_dem('Production',h2_channel,n) = max( EPS ,
sum( h2_tech ,
sum( h ,
max( EPS , H2_E_H2_IN.l(n,h2_tech,h2_channel,h) )
)
)
) ;

h2_elec_dem('Aux prod. site',h2_channel,n) = max( EPS ,
sum( h2_tech ,
h2_prod_aux_sw(n,h2_tech,h2_channel) * h2_prod_aux_ed(n,h2_tech,h2_channel) * sum( h ,
max( EPS , H2_PROD_AUX_IN.l(n,h2_tech,h2_channel,h) )
)
)
) ;

h2_elec_dem('HYD LIQ',h2_channel,n) = max( EPS ,
h2_hyd_liq_sw(n,h2_channel) * h2_hyd_liq_ed(n,h2_channel) * sum( h ,
max( EPS , H2_PROD_AUX_OUT.l(n,h2_channel,h) )
)
) ;

h2_elec_dem('Production site storage',h2_channel,n) = max( EPS ,
h2_sto_p_sw(n,h2_channel) * h2_sto_p_ed(n,h2_channel) * sum( h ,
max( EPS , H2_STO_P_L.l(n,h2_channel,h) )
)
) ;

h2_elec_dem('Aux before trans.',h2_channel,n) = max( EPS ,
h2_aux_pretrans_sw(n,h2_channel) * h2_aux_pretrans_ed(n,h2_channel) * sum( h ,
max( EPS , sum(h2_tech,H2_BYPASS_1.l(n,h2_tech,h2_channel,h)) + H2_STO_P_OUT.l(n,h2_channel,h) )
)
) ;

h2_elec_dem('Aux before LP storage .',h2_channel,n) = max( EPS ,
h2_aux_bflp_sto_sw(n,h2_channel) * h2_aux_bflp_sto_ed(n,h2_channel) * sum( h ,
max( EPS , H2_AUX_BFLP_STO_IN.l(n,h2_channel,h) )
)
) ;

h2_elec_dem('LP storage',h2_channel,n) = max( EPS ,
h2_lp_sto_sw(n,h2_channel) * h2_lp_sto_ed(n,h2_channel) * sum( h ,
max( EPS , H2_LP_STO_L.l(n,h2_channel,h) )
)
) ;

h2_elec_dem('DEHYD EVAP',h2_channel,n) = max( EPS ,
h2_dehyd_evap_sw(n,h2_channel) * ( 1 - h2_dehyd_evap_gas_sw(n,h2_channel) ) * h2_dehyd_evap_ed(n,h2_channel) * sum( h ,
max( EPS , H2_LP_STO_OUT.l(n,h2_channel,h) )
)
) ;

h2_elec_dem('Aux bfmp storage.',h2_channel,n) = max( EPS ,
h2_aux_bfmp_sto_sw(n,h2_channel) * h2_aux_bfmp_sto_ed(n,h2_channel) * sum( h ,
max( EPS , H2_DEHYD_EVAP_OUT.l(n,h2_channel,h) )
)
) ;

h2_elec_dem('MP storage',h2_channel,n) = max( EPS ,
h2_mp_sto_sw(n,h2_channel) * h2_mp_sto_ed(n,h2_channel) * sum( h ,
max( EPS , H2_MP_STO_L.l(n,h2_channel,h) )
)
) ;

h2_elec_dem('Aux BFHP storage',h2_channel,n) = max( EPS ,
h2_aux_bfhp_sto_sw(n,h2_channel) * h2_aux_bfhp_sto_ed(n,h2_channel) * sum( h ,
max( EPS , H2_AUX_BFHP_STO_IN.l(n,h2_channel,h) )
)
) ;

h2_elec_dem('HP storage',h2_channel,n) = max( EPS ,
h2_hp_sto_sw(n,h2_channel) * h2_hp_sto_ed(n,h2_channel) * sum( h ,
max( EPS , H2_HP_STO_L.l(n,h2_channel,h) )
)
) ;

h2_elec_dem('Aux bffuel',h2_channel,n) = max( EPS ,
h2_aux_bffuel_sw(n,h2_channel) * h2_aux_bffuel_ed(n,h2_channel) * sum( h ,
max( EPS , H2_HP_STO_OUT.l(n,h2_channel,h) )
)
) ;


h2_elec_dem('Aux recon. site',h2_channel,n) = max( EPS ,
sum( h2_tech_recon ,
h2_recon_aux_sw(n,h2_tech_recon,h2_channel) * h2_recon_aux_ed(n,h2_tech_recon,h2_channel) * sum( h ,
max( EPS , H2_RECON_AUX_OUT.l(n,h2_channel,h2_tech_recon,h)/(1-H2_eta_recon_aux(n,h2_channel,h2_tech_recon)) )
)
)
) ;


h2_elec_dem_oa_by_channel('Overall elec. demand by channel',h2_channel,n) =
  h2_elec_dem('Production',h2_channel,n)
+ h2_elec_dem('Aux prod. site',h2_channel,n)
+ h2_elec_dem('HYD LIQ',h2_channel,n)
+ h2_elec_dem('Production site storage',h2_channel,n)
+ h2_elec_dem('Aux before trans.',h2_channel,n)
+ h2_elec_dem('Aux before LP storage .',h2_channel,n)
+ h2_elec_dem('LP storage',h2_channel,n)
+ h2_elec_dem('DEHYD EVAP',h2_channel,n)
+ h2_elec_dem('Aux bfmp storage.',h2_channel,n)
+ h2_elec_dem('MP storage',h2_channel,n)
+ h2_elec_dem('Aux BFHP storage',h2_channel,n)
+ h2_elec_dem('HP storage',h2_channel,n)
+ h2_elec_dem('Aux bffuel',h2_channel,n)
+ h2_elec_dem('Aux recon. site',h2_channel,n)
 ;









h2_elec_dem_costs_electrolysis_aux_per_h('Production - by tech.',h2_tech,h2_channel,n,h) = max( EPS ,
0.001 * report_hours('price',h,n) *
max( EPS , H2_E_H2_IN.l(n,h2_tech,h2_channel,h) )
) ;

h2_elec_dem_costs_electrolysis_aux_per_h('Aux prod. site - by tech.',h2_tech,h2_channel,n,h) = max( EPS ,
h2_prod_aux_sw(n,h2_tech,h2_channel) * h2_prod_aux_ed(n,h2_tech,h2_channel) *
0.001 * report_hours('price',h,n) *
max( EPS , H2_PROD_AUX_IN.l(n,h2_tech,h2_channel,h) )
) ;

h2_elec_dem_costs_reconversion_per_h('Aux recon. site - by tech.',h2_tech_recon,h2_channel,n,h) = max( EPS ,
h2_recon_aux_sw(n,h2_channel,h2_tech_recon) * h2_recon_aux_ed(n,h2_channel,h2_tech_recon) *
0.001 * report_hours('price',h,n) *
max( EPS , H2_RECON_AUX_OUT.l(n,h2_channel,h2_tech_recon,h)/(1-H2_eta_recon_aux(n,h2_channel,h2_tech_recon)) )
) ;



h2_elec_dem_costs_per_h('Production',h2_channel,n,h) = max( EPS ,
sum( h2_tech ,
0.001 * report_hours('price',h,n) *
max( EPS , H2_E_H2_IN.l(n,h2_tech,h2_channel,h) )
)
) ;

h2_elec_dem_costs_per_h('Aux prod. site',h2_channel,n,h) = max( EPS ,
sum( h2_tech ,
h2_prod_aux_sw(n,h2_tech,h2_channel) * h2_prod_aux_ed(n,h2_tech,h2_channel) *
0.001 * report_hours('price',h,n) *
max( EPS , H2_PROD_AUX_IN.l(n,h2_tech,h2_channel,h) )
)
) ;

h2_elec_dem_costs_per_h('HYD LIQ',h2_channel,n,h) = max( EPS ,
h2_hyd_liq_sw(n,h2_channel) * h2_hyd_liq_ed(n,h2_channel) *
0.001 * report_hours('price',h,n) *
max( EPS , H2_PROD_AUX_OUT.l(n,h2_channel,h) )
) ;

h2_elec_dem_costs_per_h('Production site storage',h2_channel,n,h) = max( EPS ,
h2_sto_p_sw(n,h2_channel) * h2_sto_p_ed(n,h2_channel) *
0.001 * report_hours('price',h,n) *
max( EPS , H2_STO_P_L.l(n,h2_channel,h) )
) ;

h2_elec_dem_costs_per_h('Aux before trans.',h2_channel,n,h) = max( EPS ,
h2_aux_pretrans_sw(n,h2_channel) * h2_aux_pretrans_ed(n,h2_channel) *
0.001 * report_hours('price',h,n) *
max( EPS , sum(h2_tech,H2_BYPASS_1.l(n,h2_tech,h2_channel,h)) + H2_STO_P_OUT.l(n,h2_channel,h) )
) ;

h2_elec_dem_costs_per_h('Aux before LP storage .',h2_channel,n,h) = max( EPS ,
h2_aux_bflp_sto_sw(n,h2_channel) * h2_aux_bflp_sto_ed(n,h2_channel) *
0.001 * report_hours('price',h,n) *
max( EPS , H2_AUX_BFLP_STO_IN.l(n,h2_channel,h) )
) ;

h2_elec_dem_costs_per_h('LP storage',h2_channel,n,h) = max( EPS ,
h2_lp_sto_sw(n,h2_channel) * h2_lp_sto_ed(n,h2_channel) *
0.001 * report_hours('price',h,n) *
max( EPS , H2_LP_STO_L.l(n,h2_channel,h) )
) ;

h2_elec_dem_costs_per_h('DEHYD EVAP',h2_channel,n,h) = max( EPS ,
h2_dehyd_evap_sw(n,h2_channel) * ( 1 - h2_dehyd_evap_gas_sw(n,h2_channel) ) * h2_dehyd_evap_ed(n,h2_channel) *
0.001 * report_hours('price',h,n) *
max( EPS , H2_LP_STO_OUT.l(n,h2_channel,h) )
) ;

h2_elec_dem_costs_per_h('Aux bfmp storage.',h2_channel,n,h) = max( EPS ,
h2_aux_bfmp_sto_sw(n,h2_channel) * h2_aux_bfmp_sto_ed(n,h2_channel) *
0.001 * report_hours('price',h,n) *
max( EPS , H2_DEHYD_EVAP_OUT.l(n,h2_channel,h) )
) ;

h2_elec_dem_costs_per_h('MP storage',h2_channel,n,h) = max( EPS ,
h2_mp_sto_sw(n,h2_channel) * h2_mp_sto_ed(n,h2_channel) *
0.001 * report_hours('price',h,n) *
max( EPS , H2_MP_STO_L.l(n,h2_channel,h) )
) ;

h2_elec_dem_costs_per_h('Aux BFHP storage',h2_channel,n,h) = max( EPS ,
h2_aux_bfhp_sto_sw(n,h2_channel) * h2_aux_bfhp_sto_ed(n,h2_channel) *
0.001 * report_hours('price',h,n) *
max( EPS , H2_AUX_BFHP_STO_IN.l(n,h2_channel,h) )
) ;

h2_elec_dem_costs_per_h('HP storage',h2_channel,n,h) = max( EPS ,
h2_hp_sto_sw(n,h2_channel) * h2_hp_sto_ed(n,h2_channel) *
0.001 * report_hours('price',h,n) *
max( EPS , H2_HP_STO_L.l(n,h2_channel,h) )
) ;

h2_elec_dem_costs_per_h('Aux bffuel',h2_channel,n,h) = max( EPS ,
h2_aux_bffuel_sw(n,h2_channel) * h2_aux_bffuel_ed(n,h2_channel) *
0.001 * report_hours('price',h,n) *
max( EPS , H2_HP_STO_OUT.l(n,h2_channel,h) )

) ;

h2_elec_dem_costs_per_h('Aux recon. site',h2_channel,n,h) = max( EPS ,
sum( h2_tech_recon ,
h2_recon_aux_sw(n,h2_tech_recon,h2_channel) * h2_recon_aux_ed(n,h2_tech_recon,h2_channel) *
0.001 * report_hours('price',h,n) *
max( EPS , H2_RECON_AUX_OUT.l(n,h2_channel,h2_tech_recon,h)/(1-H2_eta_recon_aux(n,h2_channel,h2_tech_recon)) )
)
) ;

h2_elec_dem_costs_oa_by_channel_per_h('Overall elec. dem. costs by channel',h2_channel,n,h) =
  h2_elec_dem_costs_per_h('Production',h2_channel,n,h)
+ h2_elec_dem_costs_per_h('Aux prod. site',h2_channel,n,h)
+ h2_elec_dem_costs_per_h('HYD LIQ',h2_channel,n,h)
+ h2_elec_dem_costs_per_h('Production site storage',h2_channel,n,h)
+ h2_elec_dem_costs_per_h('Aux before trans.',h2_channel,n,h)
+ h2_elec_dem_costs_per_h('Aux before LP storage .',h2_channel,n,h)
+ h2_elec_dem_costs_per_h('LP storage',h2_channel,n,h)
+ h2_elec_dem_costs_per_h('DEHYD EVAP',h2_channel,n,h)
+ h2_elec_dem_costs_per_h('Aux bfmp storage.',h2_channel,n,h)
+ h2_elec_dem_costs_per_h('MP storage',h2_channel,n,h)
+ h2_elec_dem_costs_per_h('Aux BFHP storage',h2_channel,n,h)
+ h2_elec_dem_costs_per_h('HP storage',h2_channel,n,h)
+ h2_elec_dem_costs_per_h('Aux bffuel',h2_channel,n,h)
+ h2_elec_dem_costs_per_h('Aux recon. site',h2_channel,n,h)
 ;


h2_elec_dem_costs_oa_by_channel('Overall elec. dem. costs by channel',h2_channel,n) = max( EPS ,
sum( h , h2_elec_dem_costs_oa_by_channel_per_h('Overall elec. dem. costs by channel',h2_channel,n,h) )
) ;



*******************



h2_add_costs_by_channel('Additional gas demand by channel (dehydration)',h2_channel,n) =
max( EPS ,
   h2_dehyd_evap_gas_sw(n,h2_channel) * h2_dehyd_evap_sw(n,h2_channel) * h2_dehyd_evap_gas(n,h2_channel) * sum( h , max( EPS, H2_LP_STO_OUT.l(n,h2_channel,h) ) )

 ) ;

h2_add_costs_by_channel('Additional gas costs by channel (dehydration)',h2_channel,n) =
max( EPS ,
   h2_dehyd_evap_gas_sw(n,h2_channel) * h2_dehyd_evap_sw(n,h2_channel) * h2_dehyd_evap_gas(n,h2_channel) * h2_c_gas(n) * sum( h , max( EPS, H2_LP_STO_OUT.l(n,h2_channel,h) ) )
) ;




********




h2_add_costs_by_channel('Additional CO2 emissions by channel (dehydration)',h2_channel,n) =
max( EPS ,
   h2_dehyd_evap_gas_sw(n,h2_channel) * h2_dehyd_evap_sw(n,h2_channel) * carbon_content(n,'CCGT') * h2_dehyd_evap_gas(n,h2_channel) * sum( h , max( EPS, H2_LP_STO_OUT.l(n,h2_channel,h) ) )

) ;

h2_add_costs_by_channel('Additional CO2 emission costs by channel (dehydration)',h2_channel,n) =
max( EPS ,
   h2_dehyd_evap_gas_sw(n,h2_channel) * h2_dehyd_evap_sw(n,h2_channel) * 0.001 * carbon_content(n,'CCGT') * CO2price(n,'CCGT') * h2_dehyd_evap_gas(n,h2_channel) * sum( h , max( EPS, H2_LP_STO_OUT.l(n,h2_channel,h) ) )
 ) ;




********




h2_add_costs_by_channel('Additional transportation costs by channel',h2_channel,n) =
max( EPS ,
   h2_trans_sw(n,h2_channel) * h2_trans_c_var(n,h2_channel) * h2_trans_dist(n,h2_channel) * sum( h , max( EPS, H2_AUX_PRETRANS_OUT.l(n,h2_channel,h) ) )
   ) ;

h2_add_costs_by_channel('Additional LOHC material costs by channel',h2_channel,n) =
max( EPS ,
   h2_hyd_liq_sw(n,h2_channel) * h2_hyd_liq_c_vom(n,h2_channel) * sum( h , max( EPS, H2_PROD_AUX_OUT.l(n,h2_channel,h) ) )
) ;




h2_add_costs_by_channel('Additional var costs for recon by channel',h2_channel,n) =
max( EPS ,
   sum( (h,h2_tech_recon) , max( EPS, h2_recon_c_vom(n,h2_tech_recon) * H2_RECON_AUX_OUT.l(n,h2_channel,h2_tech_recon,h) ) )
) ;





h2_add_costs_by_channel('Sum of additional costs by channel',h2_channel,n) =
max( EPS , h2_add_costs_by_channel('Additional gas costs by channel (dehydration)',h2_channel,n)
         + h2_add_costs_by_channel('Additional CO2 emission costs by channel (dehydration)',h2_channel,n)
         + h2_add_costs_by_channel('Additional transportation costs by channel',h2_channel,n)
         + h2_add_costs_by_channel('Additional LOHC material costs by channel',h2_channel,n)
         + h2_add_costs_by_channel('Additional var costs for recon by channel',h2_channel,n) ) ;


*****

h2_trans_on_road('capacity is on the road ',h2_channel,n,h)=EPS ;

$ontext
$offtext






%P2H2%$ontext
%Time_consuming_transportation%$ontext
* transportation reporting
h2_trans_on_road('capacity on the road by t',h2_channel,n,h)=max( EPS, H2_N_TRANS.l(n,h2_channel) - H2_N_AVAI_TRANS.l(n,h2_channel,h)) ;



$ontext
$offtext
* ---------------------------------------------------------------------------- *
* ---------------------------------------------------------------------------- *
* ---------------------------------------------------------------------------- *
